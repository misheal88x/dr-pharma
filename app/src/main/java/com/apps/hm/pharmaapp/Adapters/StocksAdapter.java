package com.apps.hm.pharmaapp.Adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.apps.hm.pharmaapp.Activities.OrdersActivities.OrderDetailsActivity;
import com.apps.hm.pharmaapp.Activities.ProductsActivities.CategoryProductsActivity;
import com.apps.hm.pharmaapp.Bases.BaseFunctions;
import com.apps.hm.pharmaapp.Bases.BaseRetrofit;
import com.apps.hm.pharmaapp.Models.UserObject;
import com.apps.hm.pharmaapp.R;
import com.facebook.drawee.view.SimpleDraweeView;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import de.hdodenhof.circleimageview.CircleImageView;

public class StocksAdapter extends RecyclerView.Adapter<StocksAdapter.ViewHolder>{
    private Context context;
    private List<UserObject> list;

    public StocksAdapter(Context context,List<UserObject> list) {
        this.context = context;
        this.list = list;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private SimpleDraweeView image;
        private TextView name,address;
        private RelativeLayout layout;

        public ViewHolder(View view) {
            super(view);
            image = view.findViewById(R.id.image);
            name = view.findViewById(R.id.name);
            address = view.findViewById(R.id.address);
            layout = view.findViewById(R.id.layout);
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_stock, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {

        final UserObject u = list.get(position);
        holder.name.setText(u.getName());
        holder.address.setText(u.getAddress());
        BaseFunctions.setFrescoImage(holder.image, BaseRetrofit.IMAGES_BASE+u.getAvatar());
        holder.layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, CategoryProductsActivity.class);
                intent.putExtra("id",u.getId());
                intent.putExtra("name",u.getName());
                context.startActivity(intent);
            }
        });
    }
}
