package com.apps.hm.pharmaapp.APIs;

import com.apps.hm.pharmaapp.Models.BaseResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface CategoriesAPIs {
    @GET("repositories")
    Call<BaseResponse> get_stocks(
            @Header("Accept") String accept
    );

    @GET("repository-medicine/{id}")
    Call<BaseResponse> get_category_products(
            @Header("Accept") String accept,
            @Path("id") String id,
            @Query("page") int page
    );

    @GET("medicines")
    Call<BaseResponse> get_medicines(
            @Header("Accept") String accept,
            @Query("page") int page
    );
 }
