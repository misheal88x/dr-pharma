package com.apps.hm.pharmaapp.APIsClasses;

import android.content.Context;

import com.apps.hm.pharmaapp.APIs.CategoriesAPIs;
import com.apps.hm.pharmaapp.APIs.SearchAPIs;
import com.apps.hm.pharmaapp.Bases.App;
import com.apps.hm.pharmaapp.Bases.BaseFunctions;
import com.apps.hm.pharmaapp.Bases.BaseRetrofit;
import com.apps.hm.pharmaapp.Interfaces.IFailure;
import com.apps.hm.pharmaapp.Interfaces.IResponse;
import com.apps.hm.pharmaapp.Models.BaseResponse;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class SearchAPIsClass extends BaseRetrofit {

    public static void search_stocks(final Context context,
                                           String name,
                                           IResponse onResponse1,
                                           final IFailure onFailure1){
        onResponse = onResponse1;
        onFailure = onFailure1;

        App.is_api_called = true;
        Retrofit retrofit = configureRetrofitWithBearer(context);
        SearchAPIs api = retrofit.create(SearchAPIs.class);
        Call<BaseResponse> call = api.search_stocks("application/json",name);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                App.is_api_called = false;
                BaseFunctions.processResponse(context,response,onResponse);
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                App.is_api_called = true;
                onFailure.onFailure();
            }
        });
    }
    public static void search_stock_products(final Context context,
                                     String stock_id,
                                     String name,
                                     int page,
                                     IResponse onResponse1,
                                     final IFailure onFailure1){
        onResponse = onResponse1;
        onFailure = onFailure1;

        App.is_api_called = true;
        Retrofit retrofit = configureRetrofitWithBearer(context);
        SearchAPIs api = retrofit.create(SearchAPIs.class);
        Call<BaseResponse> call = api.search_stock_products("application/json",stock_id,name,page);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                App.is_api_called = false;
                BaseFunctions.processResponse(context,response,onResponse);
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                App.is_api_called = false;
                onFailure.onFailure();
            }
        });
    }
}
