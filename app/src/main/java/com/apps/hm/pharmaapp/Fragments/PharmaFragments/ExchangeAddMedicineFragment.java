package com.apps.hm.pharmaapp.Fragments.PharmaFragments;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;

import com.apps.hm.pharmaapp.APIsClasses.MedicineAPIsClass;
import com.apps.hm.pharmaapp.Bases.BaseFragment;
import com.apps.hm.pharmaapp.Bases.BaseFunctions;
import com.apps.hm.pharmaapp.Interfaces.IFailure;
import com.apps.hm.pharmaapp.Interfaces.IResponse;
import com.apps.hm.pharmaapp.R;
import com.wang.avi.AVLoadingIndicatorView;

import org.w3c.dom.Text;

import java.util.Calendar;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class ExchangeAddMedicineFragment extends BaseFragment {

    private TextView expiry_date,final_price,add;
    private EditText name,price,qty;

    private String selected_date;

    private int final_price_value = 0;

    private AVLoadingIndicatorView loading;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_exchange_add_medicines,container,false);
    }

    @Override
    public void init_views() {
        name = base.findViewById(R.id.name);
        price = base.findViewById(R.id.price);
        qty = base.findViewById(R.id.qty);
        final_price = base.findViewById(R.id.final_price_value);
        expiry_date = base.findViewById(R.id.expiry_date);
        loading = base.findViewById(R.id.loading);
        add = base.findViewById(R.id.add);
    }

    @Override
    public void init_events() {
        expiry_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar newCalendar = Calendar.getInstance();
                DatePickerDialog cal = new DatePickerDialog(base, new DatePickerDialog.OnDateSetListener() {
                    public void onDateSet(DatePicker view, int year, int month, int day) {
                        Calendar newDate = Calendar.getInstance();
                        newDate.set(year, month, day);

                        String year_str = new String();
                        String month_str = new String();
                        String day_str = new String();
                        year_str = String.valueOf(year);
                        if (month + 1 < 10) {
                            month_str = "0" + String.valueOf(month + 1);
                        } else {
                            month_str = String.valueOf(month + 1);
                        }
                        if (day < 10) {
                            day_str = "0" + String.valueOf(day);
                        } else {
                            day_str = String.valueOf(day);
                        }
                        String desDate = year_str + "-" + month_str + "-" + day_str;
                        selected_date = desDate;
                        expiry_date.setText("تاريخ الانتهاء المختار : "+selected_date);
                        expiry_date.setTextColor(getResources().getColor(R.color.white));
                        expiry_date.setBackgroundResource(R.drawable.rounded_primary);
                    }

                }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
                cal.show();
            }
        });

        price.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length()>0&&qty.getText().toString().length()>0){
                    calculateFinalPrice();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        qty.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length()>0&&price.getText().toString().length()>0){
                    calculateFinalPrice();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (name.getText().toString().equals("")){
                    BaseFunctions.showWarningToast(base,"يجب عليك كتابة اسم الدواء");
                    return;
                }
                if (price.getText().toString().equals("")){
                    BaseFunctions.showWarningToast(base,"يجب عليك كتابة سعر الدواء");
                    return;
                }
                if (qty.getText().toString().equals("")){
                    BaseFunctions.showWarningToast(base,"يجب عليك كتابة الكمية");
                    return;
                }
                if (selected_date.equals("")){
                    BaseFunctions.showWarningToast(base,"يجب عليك اختيار تاريخ انتهاء الدواء");
                    return;
                }
                callAddMedicineAPI();
            }
        });
    }

    @Override
    public void init_fragment(Bundle savedInstanceState) {

    }

    private void calculateFinalPrice(){
        try {
            final_price_value = Integer.valueOf(qty.getText().toString())*Integer.valueOf(price.getText().toString());
            final_price.setText(final_price_value+" ل.س");
        }catch (Exception e){}
    }

    private void callAddMedicineAPI(){
        add.setVisibility(View.GONE);
        loading.setVisibility(View.VISIBLE);
        MedicineAPIsClass.add_exchange_medicine(
                base,
                name.getText().toString(),
                price.getText().toString(),
                qty.getText().toString(),
                selected_date,
                String.valueOf(final_price_value),
                new IResponse() {
                    @Override
                    public void onResponse() {
                        add.setVisibility(View.VISIBLE);
                        loading.setVisibility(View.GONE);
                    }

                    @Override
                    public void onResponse(Object json) {
                        add.setVisibility(View.VISIBLE);
                        loading.setVisibility(View.GONE);
                        BaseFunctions.showSuccessToast(base,"تم إضافة الدواء بنجاح");
                        name.setText("");
                        price.setText("");
                        qty.setText("");
                        final_price_value = 0;
                        final_price.setText("0.00 ل.س");
                        expiry_date.setText("اختر تاريخ انتهاء الدواء");
                        selected_date = "";
                        expiry_date.setBackgroundResource(R.drawable.rounded_primary_border);
                        expiry_date.setTextColor(getResources().getColor(R.color.colorPrimary));
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        add.setVisibility(View.VISIBLE);
                        loading.setVisibility(View.GONE);
                        BaseFunctions.showErrorToast(base,getResources().getString(R.string.no_internet));
                    }
                });
    }
}
