package com.apps.hm.pharmaapp.Models;

import com.google.gson.annotations.SerializedName;

public class NewsObject {
    @SerializedName("id") private int id = 0;
    @SerializedName("title") private String title = "";
    @SerializedName("body") private String body = "";
    @SerializedName("image") private String image = "";
    @SerializedName("visibility") private boolean visibility = false;
    @SerializedName("likes_count") private int likes_count = 0;
    @SerializedName("liked") private int liked = 0;
    @SerializedName("created_at") private String created_at = "";
    @SerializedName("updated_at") private String updated_at = "";

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public boolean isVisibility() {
        return visibility;
    }

    public void setVisibility(boolean visibility) {
        this.visibility = visibility;
    }

    public int getLikes_count() {
        return likes_count;
    }

    public void setLikes_count(int likes_count) {
        this.likes_count = likes_count;
    }

    public int getLiked() {
        return liked;
    }

    public void setLiked(int liked) {
        this.liked = liked;
    }
}
