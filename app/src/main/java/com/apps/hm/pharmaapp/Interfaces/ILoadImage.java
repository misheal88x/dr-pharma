package com.apps.hm.pharmaapp.Interfaces;

public interface ILoadImage {
    void onLoaded();
    void onFailed();
}
