package com.apps.hm.pharmaapp.Activities.AccountActivities;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.apps.hm.pharmaapp.Bases.BaseActivity;
import com.apps.hm.pharmaapp.R;

public class NotActivatedAccountActivity extends BaseActivity {


    @Override
    public void set_layout() {
        setContentView(R.layout.activity_not_activated_account);
    }

    @Override
    public void init_activity(Bundle savedInstanceState) {

    }

    @Override
    public void init_views() {

    }

    @Override
    public void init_events() {

    }

    @Override
    public void set_fragment_place() {

    }
}
