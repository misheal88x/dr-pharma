package com.apps.hm.pharmaapp.Activities.HomesActivities;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.apps.hm.pharmaapp.Activities.PharmaActivities.PharmaOffersActivity;
import com.apps.hm.pharmaapp.Bases.App;
import com.apps.hm.pharmaapp.Bases.BaseActivity;
import com.apps.hm.pharmaapp.Bases.BaseFunctions;
import com.apps.hm.pharmaapp.Bases.SharedPrefManager;
import com.apps.hm.pharmaapp.Dialogs.NotActivatedDialog;
import com.apps.hm.pharmaapp.Fragments.PharmaFragments.ExchangeMediciesFragment;
import com.apps.hm.pharmaapp.Fragments.PharmaFragments.MoreFragment;
import com.apps.hm.pharmaapp.Fragments.PharmaFragments.PricesFragment;
import com.apps.hm.pharmaapp.Fragments.PharmaFragments.ShopFragment;
import com.apps.hm.pharmaapp.R;

public class PharmaHomeActivity extends BaseActivity {

    private RelativeLayout toolbar;
    private TextView title;
    private ImageView back;

    private LinearLayout shop_layout,more_layout;
    private RelativeLayout prices_layout,exchange_layout,prices_trans_layout,exchange_trans_layout;
    private ImageView shop_icon,prices_icon, exchange_icon,more_icon;
    private TextView shop_txt,prices_txt,exchange_txt,more_txt;
    private boolean is_back_pressed = false, is_prices_activated = false, is_exchange_activated = false;

    @Override
    public void set_layout() {
        setContentView(R.layout.activity_pharma_home);
    }

    @Override
    public void init_activity(Bundle savedInstanceState) {
        if (SharedPrefManager.getInstance(PharmaHomeActivity.this).getUser().getAllow_med_price() == 1){
            prices_trans_layout.setVisibility(View.GONE);
        }
        if (SharedPrefManager.getInstance(PharmaHomeActivity.this).getUser().getAllow_med_exchange() == 1){
            exchange_trans_layout.setVisibility(View.GONE);
        }
        click_shop();
        back.setVisibility(View.GONE);
    }

    @Override
    public void init_views() {
        toolbar = findViewById(R.id.toolbar);
        title = toolbar.findViewById(R.id.title);
        back = toolbar.findViewById(R.id.back);

        shop_layout = findViewById(R.id.shop_layout);
        prices_layout = findViewById(R.id.prices_layout);
        exchange_layout = findViewById(R.id.exchange_layout);
        more_layout = findViewById(R.id.more_layout);

        shop_icon = findViewById(R.id.shop_icon);
        prices_icon = findViewById(R.id.prices_icon);
        exchange_icon = findViewById(R.id.exchange_icon);
        more_icon = findViewById(R.id.more_icon);
        prices_trans_layout = findViewById(R.id.prices_trans_layout);
        exchange_trans_layout = findViewById(R.id.exchange_trans_layout);

        shop_txt = findViewById(R.id.shop_txt);
        prices_txt = findViewById(R.id.prices_txt);
        exchange_txt = findViewById(R.id.exchange_txt);
        more_txt = findViewById(R.id.more_txt);

    }

    @Override
    public void init_events() {
        shop_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (App.is_api_called == false) {
                    click_shop();
                }
            }
        });

        prices_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!App.is_api_called) {
                    if (SharedPrefManager.getInstance(PharmaHomeActivity.this).getUser().getAllow_med_price() == 1) {
                        click_prices();
                    } else {
                        NotActivatedDialog dialog = new NotActivatedDialog(PharmaHomeActivity.this);
                        dialog.show();
                    }
                }
            }
        });

        exchange_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!App.is_api_called) {
                    if (SharedPrefManager.getInstance(PharmaHomeActivity.this).getUser().getAllow_med_exchange() == 1) {
                        click_exchange();
                    } else {
                        NotActivatedDialog dialog = new NotActivatedDialog(PharmaHomeActivity.this);
                        dialog.show();
                    }
                }
            }
        });
        more_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!App.is_api_called) {
                    click_more();
                }
            }
        });
    }

    @Override
    public void set_fragment_place() {
        fragment_place = findViewById(R.id.container);
    }

    private void click_shop(){
        Fragment f = fragmentManager.findFragmentById(R.id.container);
        if (!(f instanceof ShopFragment)) {
            shop_icon.setImageResource(R.drawable.pharma_shop_on);
            prices_icon.setImageResource(R.drawable.pharma_prices_off);
            exchange_icon.setImageResource(R.drawable.pharma_exchange_off);
            more_icon.setImageResource(R.drawable.pharma_more_off);
            title.setText("التسوق");

            shop_txt.setTextColor(getResources().getColor(R.color.colorPrimary));
            prices_txt.setTextColor(getResources().getColor(R.color.black));
            exchange_txt.setTextColor(getResources().getColor(R.color.black));
            more_txt.setTextColor(getResources().getColor(R.color.black));

            open_fragment(new ShopFragment());
        }
    }

    private void click_prices(){
        Fragment f = fragmentManager.findFragmentById(R.id.container);
        if (!(f instanceof PricesFragment)) {
            shop_icon.setImageResource(R.drawable.pharma_shop_off);
            prices_icon.setImageResource(R.drawable.pharma_prices_on);
            exchange_icon.setImageResource(R.drawable.pharma_exchange_off);
            more_icon.setImageResource(R.drawable.pharma_more_off);
            title.setText("أسعار الأدوية");

            shop_txt.setTextColor(getResources().getColor(R.color.black));
            prices_txt.setTextColor(getResources().getColor(R.color.colorPrimary));
            exchange_txt.setTextColor(getResources().getColor(R.color.black));
            more_txt.setTextColor(getResources().getColor(R.color.black));

            open_fragment(new PricesFragment());
        }
    }

    private void click_exchange(){
        Fragment f = fragmentManager.findFragmentById(R.id.container);
        if (!(f instanceof ExchangeMediciesFragment)) {
            shop_icon.setImageResource(R.drawable.pharma_shop_off);
            prices_icon.setImageResource(R.drawable.pharma_prices_off);
            exchange_icon.setImageResource(R.drawable.pharma_exchange_on);
            more_icon.setImageResource(R.drawable.pharma_more_off);
            title.setText("تبادل الأدوية");

            shop_txt.setTextColor(getResources().getColor(R.color.black));
            prices_txt.setTextColor(getResources().getColor(R.color.black));
            exchange_txt.setTextColor(getResources().getColor(R.color.colorPrimary));
            more_txt.setTextColor(getResources().getColor(R.color.black));

            open_fragment(new ExchangeMediciesFragment());
        }
    }

    private void click_more(){
        Fragment f = fragmentManager.findFragmentById(R.id.container);
        if (!(f instanceof MoreFragment)) {
            shop_icon.setImageResource(R.drawable.pharma_shop_off);
            prices_icon.setImageResource(R.drawable.pharma_prices_off);
            exchange_icon.setImageResource(R.drawable.pharma_exchange_off);
            more_icon.setImageResource(R.drawable.pharma_more_on);
            title.setText("المزيد");

            shop_txt.setTextColor(getResources().getColor(R.color.black));
            prices_txt.setTextColor(getResources().getColor(R.color.black));
            exchange_txt.setTextColor(getResources().getColor(R.color.black));
            more_txt.setTextColor(getResources().getColor(R.color.colorPrimary));

            open_fragment(new MoreFragment());
        }
    }

    @Override
    public void onBackPressed() {
        Fragment f = fragmentManager.findFragmentById(R.id.container);
        if (f != null){
            if (f instanceof ShopFragment){
                if (is_back_pressed){
                    Intent intent = new Intent(Intent.ACTION_MAIN);
                    intent.addCategory(Intent.CATEGORY_HOME);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    finish();
                    BaseFunctions.runBackSlideAnimation(this);
                }else {
                    is_back_pressed = true;
                    BaseFunctions.showInfoToast(PharmaHomeActivity.this,"اضغط زر العودة مرة اخرى للخروج من التطبيق");
                }
            }else if (f instanceof PricesFragment || f instanceof ExchangeMediciesFragment || f instanceof MoreFragment){
                click_shop();
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }
}
