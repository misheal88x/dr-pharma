package com.apps.hm.pharmaapp.Activities.AccountActivities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.apps.hm.pharmaapp.APIsClasses.UserAPIsClass;
import com.apps.hm.pharmaapp.Activities.HomesActivities.PharmaHomeActivity;
import com.apps.hm.pharmaapp.Activities.HomesActivities.StockOwnerHomeActivity;
import com.apps.hm.pharmaapp.Bases.BaseActivity;
import com.apps.hm.pharmaapp.Bases.BaseFunctions;
import com.apps.hm.pharmaapp.Bases.SharedPrefManager;
import com.apps.hm.pharmaapp.Interfaces.IFailure;
import com.apps.hm.pharmaapp.Interfaces.IResponse;
import com.apps.hm.pharmaapp.Models.UserObject;
import com.apps.hm.pharmaapp.R;
import com.google.gson.Gson;
import com.wang.avi.AVLoadingIndicatorView;

public class LoginActivity extends BaseActivity {
    private EditText username,password;
    private AVLoadingIndicatorView loading;
    private TextView login;

    @Override
    public void set_layout() {
        setContentView(R.layout.activity_login);
    }

    @Override
    public void init_activity(Bundle savedInstanceState) {

    }

    @Override
    public void init_views() {
        username = findViewById(R.id.username);
        password = findViewById(R.id.password);
        loading = findViewById(R.id.loading);
        login = findViewById(R.id.login);
    }

    @Override
    public void init_events() {
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (username.getText().toString().equals("")){
                    BaseFunctions.showWarningToast(LoginActivity.this,"يجب عليك كتابة اسم المستخدم");
                    return;
                }
                if (password.getText().toString().equals("")){
                    BaseFunctions.showWarningToast(LoginActivity.this,"يجب عليك كتابة كلمة المرور");
                    return;
                }
                callLoginAPI();
            }
        });
    }

    @Override
    public void set_fragment_place() {

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        BaseFunctions.runBackSlideAnimation(this);
    }

    private void callLoginAPI(){
        loading.smoothToShow();
        login.setVisibility(View.GONE);
        UserAPIsClass.login(
                LoginActivity.this,
                username.getText().toString(),
                password.getText().toString(),
                new IResponse() {
                    @Override
                    public void onResponse() {
                        loading.smoothToHide();
                        login.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onResponse(Object json) {
                        loading.smoothToHide();
                        login.setVisibility(View.VISIBLE);
                        String j = new Gson().toJson(json);
                        UserObject success = new Gson().fromJson(j,UserObject.class);
                        if (success.getActive() == 0&&success.getRole_id()==3){
                            startActivity(new Intent(LoginActivity.this,NotActivatedAccountActivity.class));
                        }else {
                            success.setPassword(password.getText().toString());
                            SharedPrefManager.getInstance(LoginActivity.this).setUser(success);
                            if (success.getRole_id()==2){
                                startActivity(new Intent(LoginActivity.this,PharmaHomeActivity.class));
                                finish();
                            }else {
                                startActivity(new Intent(LoginActivity.this,StockOwnerHomeActivity.class));
                                finish();
                            }
                        }
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        loading.smoothToHide();
                        login.setVisibility(View.VISIBLE);
                        BaseFunctions.showErrorToast(LoginActivity.this,getResources().getString(R.string.no_internet));
                    }
                }
        );
    }
}
