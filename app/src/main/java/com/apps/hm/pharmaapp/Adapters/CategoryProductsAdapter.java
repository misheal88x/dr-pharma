package com.apps.hm.pharmaapp.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.apps.hm.pharmaapp.Bases.BaseFragment;
import com.apps.hm.pharmaapp.Bases.BaseFunctions;
import com.apps.hm.pharmaapp.Bases.BaseRetrofit;
import com.apps.hm.pharmaapp.Bases.SharedPrefManager;
import com.apps.hm.pharmaapp.Interfaces.IMove;
import com.apps.hm.pharmaapp.Interfaces.IProduct;
import com.apps.hm.pharmaapp.Models.MedicineObject;
import com.apps.hm.pharmaapp.R;
import com.facebook.drawee.view.SimpleDraweeView;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.PopupMenu;
import androidx.recyclerview.widget.RecyclerView;

public class CategoryProductsAdapter extends RecyclerView.Adapter<CategoryProductsAdapter.ViewHolder>{
    private Context context;
    private List<MedicineObject> list;
    private IProduct iProduct;
    private int type;
    private int repo_id;

    public CategoryProductsAdapter(Context context,List<MedicineObject> list,int repo_id,int type,IProduct iProduct) {
        this.context = context;
        this.list = list;
        this.type = type;
        this.iProduct = iProduct;
        this.repo_id = repo_id;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView name,company,plus,qty,minus,add_to_cart,remove_from_cart,select,remove,shape;
        private SimpleDraweeView image;

        public ViewHolder(View view) {
            super(view);
            name = view.findViewById(R.id.name);
            company = view.findViewById(R.id.company);
            plus = view.findViewById(R.id.plus);
            minus = view.findViewById(R.id.minus);
            qty = view.findViewById(R.id.qty);
            add_to_cart = view.findViewById(R.id.add_to_cart);
            remove_from_cart = view.findViewById(R.id.remove_from_cart);
            select = view.findViewById(R.id.select);
            remove = view.findViewById(R.id.remove);
            image = view.findViewById(R.id.image);
            shape = view.findViewById(R.id.shape);
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_category_product, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        final MedicineObject o = list.get(position);

        if(type == 0 || type == 1){
            holder.name.setText(o.getMedicine().getName());
            holder.company.setText(o.getMedicine().getCo_name());
            holder.qty.setText(String.valueOf(o.getOrdered_count()));
            BaseFunctions.setFrescoImage(holder.image, BaseRetrofit.IMAGES_BASE+o.getMedicine().getImage());
            if (o.getMedicine().getShape()!=null&&!o.getMedicine().getShape().equals("")){
                holder.shape.setVisibility(View.VISIBLE);
                holder.shape.setText(o.getMedicine().getShape());
            }
        }else {
            holder.name.setText(o.getName());
            holder.company.setText(type==2?o.getCo_name():o.getCompany());
            holder.qty.setText(String.valueOf(o.getOrdered_count()));
            BaseFunctions.setFrescoImage(holder.image, BaseRetrofit.IMAGES_BASE+o.getImage());
        }
        holder.plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int i = o.getOrdered_count();
                i++;
                holder.qty.setText(String.valueOf(i));
                o.setOrdered_count(i);
                notifyItemChanged(position);
            }
        });
        holder.minus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int i = o.getOrdered_count();
                if (i>0) {
                    i--;
                    holder.qty.setText(String.valueOf(i));
                    o.setOrdered_count(i);
                    notifyItemChanged(position);
                }
            }
        });
        if (type == 0){
            holder.add_to_cart.setVisibility(View.VISIBLE);
            holder.remove_from_cart.setVisibility(View.GONE);
            holder.select.setVisibility(View.GONE);
            if (o.isAdded_to_cart()){
                holder.add_to_cart.setVisibility(View.GONE);
                holder.remove_from_cart.setVisibility(View.VISIBLE);
                holder.plus.setEnabled(false);
                holder.minus.setEnabled(false);
            }else {
                holder.add_to_cart.setVisibility(View.VISIBLE);
                holder.remove_from_cart.setVisibility(View.GONE);
                holder.plus.setEnabled(true);
                holder.minus.setEnabled(true);
            }
            if (SharedPrefManager.getInstance(context).getCart().size()>0){
                for (MedicineObject m : SharedPrefManager.getInstance(context).getCart()){
                    if (m.getId() == o.getId()){
                        holder.add_to_cart.setVisibility(View.GONE);
                        holder.remove_from_cart.setVisibility(View.VISIBLE);
                        break;
                    }
                }
            }
        }else if (type == 1){
            holder.add_to_cart.setVisibility(View.GONE);
            holder.remove_from_cart.setVisibility(View.VISIBLE);
            holder.select.setVisibility(View.GONE);
            holder.plus.setVisibility(View.GONE);
            holder.minus.setVisibility(View.GONE);
        }else if (type == 2){
            holder.add_to_cart.setVisibility(View.GONE);
            holder.remove_from_cart.setVisibility(View.GONE);
            holder.select.setVisibility(View.VISIBLE);
            holder.plus.setVisibility(View.GONE);
            holder.minus.setVisibility(View.GONE);
            holder.qty.setVisibility(View.GONE);
        }else {
            holder.add_to_cart.setVisibility(View.GONE);
            holder.remove_from_cart.setVisibility(View.GONE);
            holder.select.setVisibility(View.GONE);
            holder.remove.setVisibility(View.VISIBLE);
            holder.plus.setVisibility(View.GONE);
            holder.minus.setVisibility(View.GONE);
            holder.qty.setVisibility(View.GONE);
        }
        holder.add_to_cart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (o.getOrdered_count() == 0){
                    BaseFunctions.showErrorToast(context,"قم باختيار عدد القطع أولا");
                }else {
                    BaseFunctions.showSuccessToast(context,"تم إضافة الدواء للسلة");
                    o.setAdded_to_cart(true);
                    list.set(position,o);
                    notifyItemChanged(position);
                    holder.qty.setText("0");
                    holder.plus.setEnabled(false);
                    holder.minus.setEnabled(false);
                    holder.add_to_cart.setVisibility(View.GONE);
                    holder.remove_from_cart.setVisibility(View.VISIBLE);
                    SharedPrefManager.getInstance(context).addToCart(o);
                    SharedPrefManager.getInstance(context).setRepoId(repo_id);
                    o.setOrdered_count(0);
                    list.set(position,o);
                    notifyItemChanged(position);
                }
            }
        });

        holder.remove_from_cart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (type == 0){
                    o.setAdded_to_cart(false);
                    list.set(position,o);
                    notifyItemChanged(position);
                    holder.plus.setEnabled(true);
                    holder.minus.setEnabled(true);
                    holder.add_to_cart.setVisibility(View.VISIBLE);
                    holder.remove_from_cart.setVisibility(View.GONE);
                    SharedPrefManager.getInstance(context).removeFromCart(o.getId());
                }else if (type == 1){
                    list.remove(position);
                    notifyDataSetChanged();
                }
            }
        });

        holder.select.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                iProduct.onSelect(position);
            }
        });
    }
}
