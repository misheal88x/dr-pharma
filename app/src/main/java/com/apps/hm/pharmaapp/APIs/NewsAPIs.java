package com.apps.hm.pharmaapp.APIs;

import com.apps.hm.pharmaapp.Models.BaseResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface NewsAPIs {
    @GET("news")
    Call<BaseResponse> get_news(
            @Header("Accept") String accept,
            @Query("page") int page
    );

    @GET("like/{id}")
    Call<BaseResponse> like(
            @Header("Accept") String accept,
            @Path("id") String id
    );
}
