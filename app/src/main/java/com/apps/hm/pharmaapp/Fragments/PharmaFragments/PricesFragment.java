package com.apps.hm.pharmaapp.Fragments.PharmaFragments;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.apps.hm.pharmaapp.APIsClasses.CategoriesAPIsClass;
import com.apps.hm.pharmaapp.APIsClasses.MedicineAPIsClass;
import com.apps.hm.pharmaapp.Activities.ProductsActivities.CategoryProductsActivity;
import com.apps.hm.pharmaapp.Activities.ProductsActivities.SelelctMedicineActivity;
import com.apps.hm.pharmaapp.Adapters.CategoryProductsAdapter;
import com.apps.hm.pharmaapp.Adapters.PricesAdapter;
import com.apps.hm.pharmaapp.Bases.BaseFragment;
import com.apps.hm.pharmaapp.Bases.BaseFunctions;
import com.apps.hm.pharmaapp.Interfaces.IFailure;
import com.apps.hm.pharmaapp.Interfaces.IResponse;
import com.apps.hm.pharmaapp.Models.MedicineObject;
import com.apps.hm.pharmaapp.Models.MedicinesResponse;
import com.apps.hm.pharmaapp.R;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.android.material.snackbar.Snackbar;
import com.google.gson.Gson;
import com.wang.avi.AVLoadingIndicatorView;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.SimpleItemAnimator;

public class PricesFragment extends BaseFragment {
    private RecyclerView recycler;
    private List<MedicineObject> list;
    private PricesAdapter adapter;
    private ImageView go_to_top;
    private LinearLayoutManager layoutManager;
    private NestedScrollView scrollView;
    private AVLoadingIndicatorView loading_more;
    private EditText search;
    private LinearLayout no_data,no_internet,error_layout;
    private ShimmerFrameLayout shimmer;
    private int current_page = 1;
    private boolean continue_paginate = true;
    private int per_page = 10;
    private RelativeLayout root;
    private int api_type = 0;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_prices,container,false);
    }

    @Override
    public void init_views() {
        recycler = base.findViewById(R.id.recycler);
        search = base.findViewById(R.id.search);
        scrollView = base.findViewById(R.id.scroll_view);
        loading_more = base.findViewById(R.id.loading_more);
        no_data = base.findViewById(R.id.no_data_layout);
        no_internet = base.findViewById(R.id.no_internet_layout);
        error_layout = base.findViewById(R.id.error_layout);
        shimmer = base.findViewById(R.id.shimmer);
        root = base.findViewById(R.id.layout);
        go_to_top = base.findViewById(R.id.go_to_top);
    }

    @Override
    public void init_events() {

        error_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (api_type == 1) {
                    callGetMedicines(current_page, 0);
                }else if (api_type == 2){
                    //callSearchMedicinesAPI(current_page,0,search.getText().toString());
                }
            }
        });

        no_internet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (api_type == 1) {
                    callGetMedicines(current_page, 0);
                }else if (api_type == 2){
                    //callSearchMedicinesAPI(current_page,0,search.getText().toString());
                }
            }
        });

        go_to_top.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                scrollToTop();
            }
        });


        scrollView.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
            @Override
            public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                if (scrollY > 400) go_to_top.setVisibility(View.VISIBLE);
                else
                    go_to_top.setVisibility(View.GONE);
                if(v.getChildAt(v.getChildCount() - 1) != null) {
                    if ((scrollY >= (v.getChildAt(v.getChildCount() - 1).getMeasuredHeight() - v.getMeasuredHeight())) &&
                            scrollY > oldScrollY) {
                        if (list.size()>=per_page){
                            if (continue_paginate){
                                current_page++;
                                if (api_type == 1){
                                    callGetMedicines(current_page,1);
                                }else if (api_type == 2){
                                    callSearchAPI(current_page,1,search.getText().toString());
                                }
                            }
                        }
                    }
                }
            }
        });

        search.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    if (search.getText().toString().equals("")){
                        BaseFunctions.showErrorToast(base,"يجب عليك كتابة اسم الدواء");
                        return false;
                    }else {
                        BaseFunctions.showWarningToast(base,"جاري البحث..");
                        current_page = 1;
                        continue_paginate = true;
                        callSearchAPI(current_page,0,search.getText().toString());
                    }
                    return true;
                }
                return false;
            }
        });

        search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() == 0){
                    current_page = 1;
                    continue_paginate = true;
                    callGetMedicines(current_page,0);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    @Override
    public void init_fragment(Bundle savedInstanceState) {
        init_recycler();
        callGetMedicines(current_page, 0);
    }

    private void init_recycler(){
        list = new ArrayList<>();
        adapter = new PricesAdapter(base,list);
        layoutManager = new LinearLayoutManager(base);
        recycler.setLayoutManager(layoutManager);
        recycler.setAdapter(adapter);
        recycler.getItemAnimator().setChangeDuration(0);

    }

    private void callGetMedicines(final int page,final int type){
        api_type = 1;
        no_internet.setVisibility(View.GONE);
        no_data.setVisibility(View.GONE);
        error_layout.setVisibility(View.GONE);
        search.setVisibility(View.GONE);
        if (type == 1){
            loading_more.smoothToShow();
        }else {
            shimmer.startShimmer();
            shimmer.setVisibility(View.VISIBLE);
        }
        CategoriesAPIsClass.get_medicines(
                base,
                page,
                new IResponse() {
                    @Override
                    public void onResponse() {
                        error_happend(type);
                    }

                    @Override
                    public void onResponse(Object json) {
                        if (json!=null) {
                            String j = new Gson().toJson(json);
                            try {
                                MedicinesResponse success = new Gson().fromJson(j, MedicinesResponse.class);
                                if (success.getData() != null) {
                                    if (success.getData().size() > 0) {
                                        process_data(type);
                                        search.setVisibility(View.VISIBLE);
                                        for (MedicineObject o : success.getData()) {
                                            boolean found = false;
                                            for (MedicineObject oo : list){
                                                if (o.getId() == oo.getId()){
                                                    found = true;
                                                }
                                            }
                                            if (!found) {
                                                list.add(o);
                                                adapter.notifyDataSetChanged();
                                            }
                                        }
                                    } else {
                                        no_data(type);
                                    }
                                } else {
                                    error_happend(type);
                                }
                            }catch (Exception e){
                                callGetMedicines(page,type);
                            }
                        }else {
                            error_happend(type);
                        }
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        error_happend(type);
                        Snackbar.make(root, getResources().getString(R.string.no_internet), Snackbar.LENGTH_INDEFINITE)
                                .setAction("إعادة المحاولة", new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        callGetMedicines(page,type);
                                    }
                                }).setActionTextColor(getResources().getColor(R.color.white)).show();
                    }
                }
        );
    }

    private void callSearchAPI(final int page,final int type, String text){
        api_type = 2;
        no_internet.setVisibility(View.GONE);
        no_data.setVisibility(View.GONE);
        error_layout.setVisibility(View.GONE);
        search.setVisibility(View.GONE);
        if (type == 1){
            loading_more.smoothToShow();
        }else {
            shimmer.startShimmer();
            shimmer.setVisibility(View.VISIBLE);
        }
        MedicineAPIsClass.search_in_all_medicines(
                base,
                text,
                page,
                new IResponse() {
                    @Override
                    public void onResponse() {
                        error_happend(type);
                    }

                    @Override
                    public void onResponse(Object json) {
                        if (json!=null) {
                            String j = new Gson().toJson(json);
                            try {
                                MedicinesResponse success = new Gson().fromJson(j, MedicinesResponse.class);
                                if (success.getData() != null) {
                                    if (success.getData().size() > 0) {
                                        process_data(type);
                                        search.setVisibility(View.VISIBLE);
                                        for (MedicineObject o : success.getData()) {
                                            boolean found = false;
                                            for (MedicineObject oo : list){
                                                if (o.getId() == oo.getId()){
                                                    found = true;
                                                }
                                            }
                                            if (!found) {
                                                list.add(o);
                                                adapter.notifyDataSetChanged();
                                            }
                                        }
                                    } else {
                                        no_data(type);
                                    }
                                } else {
                                    error_happend(type);
                                }
                            }catch (Exception e){
                                callGetMedicines(page,type);
                            }
                        }else {
                            error_happend(type);
                        }
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        error_happend(type);
                        Snackbar.make(root, getResources().getString(R.string.no_internet), Snackbar.LENGTH_INDEFINITE)
                                .setAction("إعادة المحاولة", new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        callGetMedicines(page,type);
                                    }
                                }).setActionTextColor(getResources().getColor(R.color.white)).show();
                    }
                }
        );
    }

    private void error_happend(int type){
        if (type == 1){
            loading_more.smoothToHide();
        }else {
            shimmer.stopShimmer();
            shimmer.setVisibility(View.GONE);
            no_data.setVisibility(View.VISIBLE);
        }
    }
    private void process_data(int type){
        if (type == 0){
            no_data.setVisibility(View.GONE);
            shimmer.stopShimmer();
            shimmer.setVisibility(View.GONE);
            list = new ArrayList<>();
            adapter = new PricesAdapter(base,list);
            recycler.setAdapter(adapter);
        }else {
            loading_more.smoothToHide();
        }
    }
    private void no_data(int type){
        if (type == 0){
            no_data.setVisibility(View.VISIBLE);
            shimmer.stopShimmer();
            shimmer.setVisibility(View.GONE);
            list = new ArrayList<>();
            adapter = new PricesAdapter(base,list);
            recycler.setAdapter(adapter);
        }else {
            loading_more.smoothToHide();
            Snackbar.make(root,"لا يوجد المزيد",Snackbar.LENGTH_SHORT).show();
            continue_paginate = false;
        }
    }

    public void scrollToTop() {
        int x = 0;
        int y = 0;

        ObjectAnimator xTranslate = ObjectAnimator.ofInt(scrollView, "scrollX", x);
        ObjectAnimator yTranslate = ObjectAnimator.ofInt(scrollView, "scrollY", y);

        AnimatorSet animators = new AnimatorSet();
        animators.setDuration(1000L);
        animators.playTogether(xTranslate, yTranslate);

        animators.addListener(new Animator.AnimatorListener() {

            @Override
            public void onAnimationStart(Animator arg0) {

            }

            @Override
            public void onAnimationRepeat(Animator arg0) {


            }

            @Override
            public void onAnimationEnd(Animator arg0) {


            }

            @Override
            public void onAnimationCancel(Animator arg0) {


            }
        });
        animators.start();
    }
}
