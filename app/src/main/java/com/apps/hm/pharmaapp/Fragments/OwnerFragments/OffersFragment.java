package com.apps.hm.pharmaapp.Fragments.OwnerFragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.apps.hm.pharmaapp.APIsClasses.CategoriesAPIsClass;
import com.apps.hm.pharmaapp.APIsClasses.OffersAPIsClass;
import com.apps.hm.pharmaapp.Adapters.OwnerOffersAdapter;
import com.apps.hm.pharmaapp.Adapters.OwnerOrdersAdapter;
import com.apps.hm.pharmaapp.Bases.BaseFragment;
import com.apps.hm.pharmaapp.Interfaces.IFailure;
import com.apps.hm.pharmaapp.Interfaces.IResponse;
import com.apps.hm.pharmaapp.Models.OwnerOfferObject;
import com.apps.hm.pharmaapp.Models.UserObject;
import com.apps.hm.pharmaapp.R;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class OffersFragment extends BaseFragment {

    private RecyclerView recycler;
    private List<OwnerOfferObject> list;
    private OwnerOffersAdapter adapter;
    private LinearLayoutManager layoutManager;

    private LinearLayout no_data,no_internet,error_layout;
    private ShimmerFrameLayout shimmer;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_offers,container,false);
    }

    @Override
    public void init_views() {
        recycler = base.findViewById(R.id.recycler);
        no_data = base.findViewById(R.id.no_data_layout);
        no_internet = base.findViewById(R.id.no_internet_layout);
        error_layout = base.findViewById(R.id.error_layout);
        shimmer = base.findViewById(R.id.shimmer);
    }

    @Override
    public void init_events() {
        error_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callOffersAPI();
            }
        });

        no_internet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callOffersAPI();
            }
        });
    }

    @Override
    public void init_fragment(Bundle savedInstanceState) {
        init_recycler();
        callOffersAPI();
    }

    private void init_recycler(){
        list = new ArrayList<>();
        adapter = new OwnerOffersAdapter(base,list);
        layoutManager = new LinearLayoutManager(base);
        recycler.setLayoutManager(layoutManager);
        recycler.setAdapter(adapter);
    }

    public void callOffersAPI(){
        list = new ArrayList<>();
        adapter = new OwnerOffersAdapter(base,list);
        recycler.setAdapter(adapter);
        no_internet.setVisibility(View.GONE);
        no_data.setVisibility(View.GONE);
        error_layout.setVisibility(View.GONE);
        shimmer.startShimmer();
        shimmer.setVisibility(View.VISIBLE);
        OffersAPIsClass.get_my_offer(
                base,
                new IResponse() {
                    @Override
                    public void onResponse() {
                        error_layout.setVisibility(View.VISIBLE);
                        shimmer.stopShimmer();
                        shimmer.setVisibility(View.GONE);
                    }

                    @Override
                    public void onResponse(Object json) {
                        shimmer.stopShimmer();
                        shimmer.setVisibility(View.GONE);
                        String j = new Gson().toJson(json);
                        try {
                            OwnerOfferObject[] success = new Gson().fromJson(j, OwnerOfferObject[].class);
                            if (success.length > 0) {
                                for (OwnerOfferObject o : success) {
                                    list.add(o);
                                    adapter.notifyDataSetChanged();
                                }
                            } else {
                                no_data.setVisibility(View.VISIBLE);
                            }
                        }catch (Exception e){
                            callOffersAPI();
                        }
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        shimmer.stopShimmer();
                        shimmer.setVisibility(View.GONE);
                        no_internet.setVisibility(View.VISIBLE);
                    }
                }
        );
    }
}
