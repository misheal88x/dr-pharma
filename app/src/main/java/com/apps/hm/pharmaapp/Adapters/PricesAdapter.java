package com.apps.hm.pharmaapp.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.apps.hm.pharmaapp.Models.MedicineObject;
import com.apps.hm.pharmaapp.R;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class PricesAdapter  extends RecyclerView.Adapter<PricesAdapter.ViewHolder>{
    private Context context;
    private List<MedicineObject> list;

    public PricesAdapter(Context context,List<MedicineObject> list) {
        this.context = context;
        this.list = list;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView name,net_price,sale_price;

        public ViewHolder(View view) {
            super(view);
            name = view.findViewById(R.id.name);
            net_price = view.findViewById(R.id.net_price);
            sale_price = view.findViewById(R.id.sale_price);
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_price, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        MedicineObject m = list.get(position);
        holder.name.setText(m.getName());
        holder.net_price.setText("سعر النت : "+m.getPrice()+" ل.س");
        holder.sale_price.setText("سعر المبيع : "+m.getSale_price()+" ل.س");
    }
}
