package com.apps.hm.pharmaapp.Adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.apps.hm.pharmaapp.APIsClasses.NewsAPIsClass;
import com.apps.hm.pharmaapp.Activities.ProductsActivities.CategoryProductsActivity;
import com.apps.hm.pharmaapp.Bases.BaseFunctions;
import com.apps.hm.pharmaapp.Bases.BaseRetrofit;
import com.apps.hm.pharmaapp.Interfaces.IFailure;
import com.apps.hm.pharmaapp.Interfaces.IResponse;
import com.apps.hm.pharmaapp.Models.NewsObject;
import com.apps.hm.pharmaapp.R;
import com.apps.hm.pharmaapp.Utils.AnimationUtils;
import com.facebook.drawee.view.SimpleDraweeView;
import com.like.LikeButton;
import com.like.OnLikeListener;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Retrofit;

public class SocietyAdapter  extends RecyclerView.Adapter<SocietyAdapter.ViewHolder>{
    private Context context;
    private List<NewsObject> list;

    public SocietyAdapter(Context context,List<NewsObject> list) {
        this.context = context;
        this.list = list;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView title,text,date;
        private SimpleDraweeView image;
        private RelativeLayout layout;
        private LinearLayout animation_layout;
        private LikeButton like;
        private TextView like_count;

        public ViewHolder(View view) {
            super(view);
            title = view.findViewById(R.id.title);
            image = view.findViewById(R.id.image);
            text = view.findViewById(R.id.text);
            date = view.findViewById(R.id.date);
            layout = view.findViewById(R.id.layout);
            animation_layout = view.findViewById(R.id.animation_layout);
            like = view.findViewById(R.id.like);
            like_count = view.findViewById(R.id.like_count);
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_society, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        final NewsObject o = list.get(position);

        holder.title.setText(o.getTitle());
        holder.date.setText(BaseFunctions.dateExtractor(o.getCreated_at()));
        holder.text.setText(o.getBody());
        BaseFunctions.setFrescoImage(holder.image, BaseRetrofit.IMAGES_BASE+o.getImage());
        if (o.isVisibility()){
            holder.animation_layout.setVisibility(View.VISIBLE);
        }else {
            holder.animation_layout.setVisibility(View.GONE);
        }
        holder.layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (o.isVisibility()){
                    o.setVisibility(false);
                    list.set(position,o);
                    //notifyItemChanged(position);
                    AnimationUtils.collapse(holder.animation_layout);
                }else {
                    o.setVisibility(true);
                    list.set(position,o);
                    //notifyItemChanged(position);
                    holder.animation_layout.setVisibility(View.VISIBLE);
                    AnimationUtils.expand(holder.animation_layout);
                }
            }
        });

        holder.like_count.setText(o.getLikes_count()+"");
        if (o.getLiked() == 0){
            holder.like.setLiked(false);
        }else {
            holder.like.setLiked(true);
        }
        holder.like.setOnLikeListener(new OnLikeListener() {
            @Override
            public void liked(LikeButton likeButton) {
                callLikeAPI(holder,position,String.valueOf(o.getId()),1);
            }

            @Override
            public void unLiked(LikeButton likeButton) {
                callLikeAPI(holder,position,String.valueOf(o.getId()),0);
            }
        });
    }

    private void callLikeAPI(final ViewHolder holder, final int position, String id, final int new_state){
        NewsAPIsClass.like(
                context,
                id,
                new IResponse() {
                    @Override
                    public void onResponse() {
                        if (new_state == 0){
                            holder.like.setLiked(true);
                        }
                    }

                    @Override
                    public void onResponse(Object json) {
                        NewsObject o = list.get(position);
                        BaseFunctions.playMusic(context,R.raw.facebook_pop);
                        if (new_state == 0){
                            o.setLikes_count(o.getLikes_count()-1);
                            o.setLiked(0);
                            holder.like_count.setText(o.getLikes_count()+"");
                            list.set(position,o);
                            notifyItemChanged(position);
                        }else if (new_state == 1){
                            o.setLikes_count(o.getLikes_count()+1);
                            o.setLiked(1);
                            holder.like_count.setText(o.getLikes_count()+"");
                            list.set(position,o);
                            notifyItemChanged(position);
                        }
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        BaseFunctions.showErrorToast(context,context.getResources().getString(R.string.no_internet));
                        if (new_state == 0){
                            holder.like.setLiked(true);
                        }
                    }
                });
    }
}
