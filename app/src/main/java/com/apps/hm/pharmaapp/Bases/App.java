package com.apps.hm.pharmaapp.Bases;

import android.app.Application;
import android.content.Context;

import com.facebook.drawee.backends.pipeline.Fresco;

import androidx.multidex.MultiDex;

public class App extends Application {

    public static boolean is_token_updated = false;
    public static boolean is_api_called = false;
    @Override
    public void onCreate() {
        super.onCreate();
        Fresco.initialize(this);
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(base);
    }
}
