package com.apps.hm.pharmaapp.Activities.OrdersActivities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import de.hdodenhof.circleimageview.CircleImageView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.apps.hm.pharmaapp.APIsClasses.OrdersAPIsClass;
import com.apps.hm.pharmaapp.Activities.LocationOnMapActivity;
import com.apps.hm.pharmaapp.Adapters.OrderProductsAdapter;
import com.apps.hm.pharmaapp.Adapters.OwnerOrdersAdapter;
import com.apps.hm.pharmaapp.Bases.BaseActivity;
import com.apps.hm.pharmaapp.Bases.BaseFunctions;
import com.apps.hm.pharmaapp.Interfaces.IFailure;
import com.apps.hm.pharmaapp.Interfaces.IResponse;
import com.apps.hm.pharmaapp.Models.OrderModels.OrderObject;
import com.apps.hm.pharmaapp.Models.OrderModels.OrderProductObject;
import com.apps.hm.pharmaapp.R;
import com.google.gson.Gson;
import com.wang.avi.AVLoadingIndicatorView;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

public class OrderDetailsActivity extends BaseActivity {

    private RelativeLayout toolbar;
    private TextView title;
    private ImageView back;

    private RecyclerView recycler;
    private List<OrderProductObject> list;
    private OrderProductsAdapter adapter;
    private LinearLayoutManager layoutManager;

    private CircleImageView pharma_image;
    private RelativeLayout image_layout;
    private TextView pharma_name,pharma_address;
    private ImageView show_on_map;
    private TextView total_price;
    private TextView confirm_order,confirm_deliver;
    private RelativeLayout on_map_layout;
    private AVLoadingIndicatorView loading;

    @Override
    public void set_layout() {
        setContentView(R.layout.activity_order_details);
    }

    @Override
    public void init_activity(Bundle savedInstanceState) {
        init_recycler();
        get_data();
    }

    @Override
    public void init_views() {
        toolbar = findViewById(R.id.toolbar);
        title = toolbar.findViewById(R.id.title);
        back = toolbar.findViewById(R.id.back);
        recycler = findViewById(R.id.items_recycler);

        pharma_image = findViewById(R.id.image);
        pharma_name = findViewById(R.id.name);
        pharma_address = findViewById(R.id.address);
        show_on_map = findViewById(R.id.show_on_map);
        total_price = findViewById(R.id.total_price);
        confirm_order = findViewById(R.id.confirm_order);
        confirm_deliver = findViewById(R.id.confirm_deliver);
        on_map_layout = findViewById(R.id.on_map_layout);
        loading = findViewById(R.id.loading);

    }

    @Override
    public void init_events() {
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                BaseFunctions.runBackSlideAnimation(OrderDetailsActivity.this);
            }
        });
    }

    @Override
    public void set_fragment_place() {

    }

    @Override
    public void onBackPressed() {
        finish();
        BaseFunctions.runBackSlideAnimation(OrderDetailsActivity.this);
    }

    private void init_recycler(){
        list = new ArrayList<>();
        adapter = new OrderProductsAdapter(OrderDetailsActivity.this,list);
        layoutManager = new LinearLayoutManager(OrderDetailsActivity.this);
        recycler.setLayoutManager(layoutManager);
        recycler.setAdapter(adapter);
    }

    private void get_data(){
        String j = getIntent().getStringExtra("order");
        final OrderObject o = new Gson().fromJson(j,OrderObject.class);

        //Order id
        title.setText("الطلب رقم : "+o.getId());
        if (o.getSender()!=null) {
            //Pharma image
            if (o.getSender().getAvatar() != null) {
                BaseFunctions.setGlideImage(OrderDetailsActivity.this, pharma_image, o.getSender().getAvatar());
            }
            //Pharma name
            pharma_name.setText(o.getSender().getName()!=null?o.getSender().getName():"");
            //Pharma address
            pharma_address.setText(o.getSender().getAddress()!=null?o.getSender().getAddress():"");
            //Pharma on map
            if (o.getSender().getLocation()!=null){
                final String [] coord = o.getSender().getLocation().split("\\s*,\\s*");
                show_on_map.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(OrderDetailsActivity.this, LocationOnMapActivity.class);
                        intent.putExtra("lat",Float.valueOf(coord[0]));
                        intent.putExtra("lng",Float.valueOf(coord[1]));
                        startActivity(intent);
                    }
                });
            }else {
                on_map_layout.setVisibility(View.GONE);
            }
        }else {
            image_layout.setVisibility(View.GONE);
            pharma_name.setVisibility(View.GONE);
            pharma_address.setVisibility(View.GONE);
            on_map_layout.setVisibility(View.GONE);
        }
        //Products
        if (o.getProducts().size()>0){
            for (OrderProductObject opo : o.getProducts()){
                list.add(opo);
                adapter.notifyDataSetChanged();
            }
        }
        //Total price
        total_price.setText("تكلفة الطلبية : "+o.getTotal_price()+" ل.س");
        //Order status

        //New
        if (o.getStatus() == 1){
            confirm_order.setVisibility(View.VISIBLE);
            confirm_deliver.setVisibility(View.GONE);
            confirm_order.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    callUpdateStatusAPI(o.getId(),2);
                }
            });
        }
        //Processing
        else if (o.getStatus() == 2){
           confirm_deliver.setVisibility(View.VISIBLE);
           confirm_order.setVisibility(View.GONE);
           confirm_deliver.setOnClickListener(new View.OnClickListener() {
               @Override
               public void onClick(View v) {
                   callUpdateStatusAPI(o.getId(),3);
               }
           });
        }else if (o.getStatus() == 3){
            confirm_order.setVisibility(View.GONE);
            confirm_deliver.setVisibility(View.GONE);
        }
    }

    private void callUpdateStatusAPI(int order_id,final int new_status){
        loading.smoothToShow();
        if (new_status == 2){
            confirm_order.setVisibility(View.GONE);
        }else if (new_status == 3){
            confirm_deliver.setVisibility(View.GONE);
        }
        OrdersAPIsClass.update_order_status(OrderDetailsActivity.this,
                order_id, new_status, new IResponse() {
                    @Override
                    public void onResponse() {
                        loading.smoothToHide();
                        if (new_status == 2){
                            confirm_order.setVisibility(View.VISIBLE);
                        }else if (new_status == 3){
                            confirm_deliver.setVisibility(View.VISIBLE);
                        }
                    }

                    @Override
                    public void onResponse(Object json) {
                        loading.smoothToHide();
                        if (new_status == 2){
                            confirm_order.setVisibility(View.VISIBLE);
                        }else if (new_status == 3){
                            confirm_deliver.setVisibility(View.VISIBLE);
                        }
                        BaseFunctions.showSuccessToast(OrderDetailsActivity.this,"تم التأكيد بنجاح");
                        Intent intent = new Intent();
                        intent.putExtra("id",0);
                        setResult(RESULT_OK,intent);
                        finish();
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        loading.smoothToHide();
                        if (new_status == 2){
                            confirm_order.setVisibility(View.VISIBLE);
                        }else if (new_status == 3){
                            confirm_deliver.setVisibility(View.VISIBLE);
                        }
                        BaseFunctions.showErrorToast(OrderDetailsActivity.this,getResources().getString(R.string.no_internet));
                    }
                });
    }
}
