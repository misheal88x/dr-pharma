package com.apps.hm.pharmaapp.APIsClasses;

import android.content.Context;

import com.apps.hm.pharmaapp.APIs.MedicinesAPIs;
import com.apps.hm.pharmaapp.APIs.OffersAPIs;
import com.apps.hm.pharmaapp.Bases.App;
import com.apps.hm.pharmaapp.Bases.BaseFunctions;
import com.apps.hm.pharmaapp.Bases.BaseRetrofit;
import com.apps.hm.pharmaapp.Interfaces.IFailure;
import com.apps.hm.pharmaapp.Interfaces.IResponse;
import com.apps.hm.pharmaapp.Models.BaseResponse;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.http.Field;

public class MedicineAPIsClass extends BaseRetrofit {

    public static void search_medicines(final Context context,
                                             String text,
                                             IResponse onResponse1,
                                             final IFailure onFailure1){
        onResponse = onResponse1;
        onFailure = onFailure1;

        App.is_api_called = true;
        Retrofit retrofit = configureRetrofitWithBearer(context);
        MedicinesAPIs api = retrofit.create(MedicinesAPIs.class);
        Call<BaseResponse> call = api.search_products("application/json",
                text);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                App.is_api_called = false;
                BaseFunctions.processResponse(context,response,onResponse);
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                App.is_api_called = false;
                onFailure.onFailure();
            }
        });
    }

    public static void add_exchange_medicine(final Context context,
                                             String name,
                                             String price,
                                             String count,
                                             String expire_date,
                                             String total,
                                 IResponse onResponse1,
                                 final IFailure onFailure1){
        onResponse = onResponse1;
        onFailure = onFailure1;

        App.is_api_called = true;
        Retrofit retrofit = configureRetrofitWithBearer(context);
        MedicinesAPIs api = retrofit.create(MedicinesAPIs.class);
        Call<BaseResponse> call = api.add_exchange_medicine("application/json",
                name,price,count,expire_date,total);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                App.is_api_called = false;
                BaseFunctions.processResponse(context,response,onResponse);
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                App.is_api_called = false;
                onFailure.onFailure();
            }
        });
    }

    public static void delete_exchange_medicine(final Context context,
                                                 String id,
                                                 IResponse onResponse1,
                                                 final IFailure onFailure1){
        onResponse = onResponse1;
        onFailure = onFailure1;

        App.is_api_called = true;
        Retrofit retrofit = configureRetrofitWithBearer(context);
        MedicinesAPIs api = retrofit.create(MedicinesAPIs.class);
        Call<BaseResponse> call = api.delete_exchange_medicine("application/json",
                id);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                App.is_api_called = false;
                BaseFunctions.processResponse(context,response,onResponse);
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                App.is_api_called = false;
                onFailure.onFailure();
            }
        });
    }

    public static void get_exchange_medicines(final Context context,
                                                int page,
                                                IResponse onResponse1,
                                                final IFailure onFailure1){
        onResponse = onResponse1;
        onFailure = onFailure1;

        App.is_api_called = true;
        Retrofit retrofit = configureRetrofitWithBearer(context);
        MedicinesAPIs api = retrofit.create(MedicinesAPIs.class);
        Call<BaseResponse> call = api.get_exchange_medicines("application/json",page);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                App.is_api_called = false;
                BaseFunctions.processResponse(context,response,onResponse);
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                App.is_api_called = false;
                onFailure.onFailure();
            }
        });
    }

    public static void get_my_medicines(final Context context,
                                              int page,
                                              IResponse onResponse1,
                                              final IFailure onFailure1){
        onResponse = onResponse1;
        onFailure = onFailure1;

        App.is_api_called = true;
        Retrofit retrofit = configureRetrofitWithBearer(context);
        MedicinesAPIs api = retrofit.create(MedicinesAPIs.class);
        Call<BaseResponse> call = api.get_my_medicines("application/json",page);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                App.is_api_called = false;
                BaseFunctions.processResponse(context,response,onResponse);
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                App.is_api_called = false;
                onFailure.onFailure();
            }
        });
    }

    public static void delete_my_medicine(final Context context,
                                        String id,
                                        IResponse onResponse1,
                                        final IFailure onFailure1){
        onResponse = onResponse1;
        onFailure = onFailure1;

        App.is_api_called = true;
        Retrofit retrofit = configureRetrofitWithBearer(context);
        MedicinesAPIs api = retrofit.create(MedicinesAPIs.class);
        Call<BaseResponse> call = api.delete_my_medicine("application/json",id);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                App.is_api_called = false;
                BaseFunctions.processResponse(context,response,onResponse);
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                App.is_api_called = false;
                onFailure.onFailure();
            }
        });
    }

    public static void add_my_medicine(final Context context,
                                          int med_id,
                                          int count,
                                          IResponse onResponse1,
                                          final IFailure onFailure1){
        onResponse = onResponse1;
        onFailure = onFailure1;

        App.is_api_called = true;
        Retrofit retrofit = configureRetrofitWithBearer(context);
        MedicinesAPIs api = retrofit.create(MedicinesAPIs.class);
        Call<BaseResponse> call = api.add_my_medicine("application/json",med_id,count);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                App.is_api_called = false;
                BaseFunctions.processResponse(context,response,onResponse);
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                App.is_api_called = false;
                onFailure.onFailure();
            }
        });
    }

    public static void search_in_all_medicines(final Context context,
                                       String text,
                                       int page,
                                       IResponse onResponse1,
                                       final IFailure onFailure1){
        onResponse = onResponse1;
        onFailure = onFailure1;

        App.is_api_called = true;
        Retrofit retrofit = configureRetrofitWithBearer(context);
        MedicinesAPIs api = retrofit.create(MedicinesAPIs.class);
        Call<BaseResponse> call = api.search_in_all_medicines("application/json",text,page);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                App.is_api_called = false;
                BaseFunctions.processResponse(context,response,onResponse);
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                App.is_api_called = false;
                onFailure.onFailure();
            }
        });
    }
}
