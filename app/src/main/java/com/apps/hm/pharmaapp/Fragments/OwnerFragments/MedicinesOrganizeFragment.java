package com.apps.hm.pharmaapp.Fragments.OwnerFragments;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.apps.hm.pharmaapp.APIsClasses.CategoriesAPIsClass;
import com.apps.hm.pharmaapp.APIsClasses.MedicineAPIsClass;
import com.apps.hm.pharmaapp.APIsClasses.SearchAPIsClass;
import com.apps.hm.pharmaapp.Adapters.CategoryProductsAdapter;
import com.apps.hm.pharmaapp.Adapters.OwnerMedicineAdapter;
import com.apps.hm.pharmaapp.Adapters.PricesAdapter;
import com.apps.hm.pharmaapp.Adapters.StocksAdapter;
import com.apps.hm.pharmaapp.Bases.BaseFragment;
import com.apps.hm.pharmaapp.Bases.BaseFunctions;
import com.apps.hm.pharmaapp.Bases.SharedPrefManager;
import com.apps.hm.pharmaapp.Interfaces.IFailure;
import com.apps.hm.pharmaapp.Interfaces.IResponse;
import com.apps.hm.pharmaapp.Models.MedicineObject;
import com.apps.hm.pharmaapp.Models.OwnerMedicineObject;
import com.apps.hm.pharmaapp.Models.OwnerMedicinesResponse;
import com.apps.hm.pharmaapp.Models.UserObject;
import com.apps.hm.pharmaapp.R;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.android.material.snackbar.Snackbar;
import com.google.gson.Gson;
import com.wang.avi.AVLoadingIndicatorView;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class MedicinesOrganizeFragment extends BaseFragment {

    private EditText search;
    private RecyclerView recycler;
    private List<OwnerMedicineObject> list;
    private OwnerMedicineAdapter adapter;
    private GridLayoutManager layoutManager;
    private NestedScrollView scrollView;
    private AVLoadingIndicatorView loading_more;

    private LinearLayout no_data,no_internet,error_layout;
    private ShimmerFrameLayout shimmer;
    private int current_page = 1;
    private boolean continue_paginate = true;
    private int per_page = 10;
    private RelativeLayout root;
    private int api_type = 0;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_medicines_organize,container,false);
    }

    @Override
    public void init_views() {
        search = base.findViewById(R.id.search);
        recycler = base.findViewById(R.id.recycler);
        no_data = base.findViewById(R.id.no_data_layout);
        no_internet = base.findViewById(R.id.no_internet_layout);
        error_layout = base.findViewById(R.id.error_layout);
        shimmer = base.findViewById(R.id.shimmer);
        scrollView = base.findViewById(R.id.scrollView);
        loading_more = base.findViewById(R.id.loading_more);
        root = base.findViewById(R.id.layout);
    }

    @Override
    public void init_events() {
        search.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    if (search.getText().toString().equals("")){
                        BaseFunctions.showErrorToast(getContext(),"يجب عليك كتابة اسم الدواء");
                        return false;
                    }else {
                        BaseFunctions.showWarningToast(getContext(),"جاري البحث..");
                        current_page = 1;
                        continue_paginate = true;
                        callSearchAPI(current_page,0,search.getText().toString());
                    }
                    return true;
                }
                return false;
            }
        });

        search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() == 0){
                    current_page = 1;
                    continue_paginate = true;
                    callMyMedicinesAPI(current_page,0);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        error_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (api_type == 1) {
                    callMyMedicinesAPI(current_page,0);
                }else if (api_type == 2){
                    callSearchAPI(current_page,0,search.getText().toString());
                }
            }
        });

        no_internet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (api_type == 1) {
                    callMyMedicinesAPI(current_page,0);
                }else if (api_type == 2){
                    callSearchAPI(current_page,0,search.getText().toString());
                }
            }
        });

        scrollView.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
            @Override
            public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                if(v.getChildAt(v.getChildCount() - 1) != null) {
                    if ((scrollY >= (v.getChildAt(v.getChildCount() - 1).getMeasuredHeight() - v.getMeasuredHeight())) &&
                            scrollY > oldScrollY) {
                        if (list.size()>=per_page){
                            if (continue_paginate){
                                current_page++;
                                if (api_type == 1){
                                    callMyMedicinesAPI(current_page,1);
                                }else if (api_type == 2){
                                    callSearchAPI(current_page,1,search.getText().toString());
                                }
                            }
                        }
                    }
                }
            }
        });
    }

    @Override
    public void init_fragment(Bundle savedInstanceState) {
        init_recycler();
        callMyMedicinesAPI(current_page,0);
    }

    private void init_recycler(){
        list = new ArrayList<>();
        adapter = new OwnerMedicineAdapter(base,list);
        layoutManager = new GridLayoutManager(base,2);
        recycler.setLayoutManager(layoutManager);
        recycler.setAdapter(adapter);
    }

    private void callMyMedicinesAPI(final int page,final int type){
        api_type = 1;
        no_internet.setVisibility(View.GONE);
        no_data.setVisibility(View.GONE);
        error_layout.setVisibility(View.GONE);
        search.setVisibility(View.GONE);
        if (type == 1){
            loading_more.smoothToShow();
        }else {
            shimmer.startShimmer();
            shimmer.setVisibility(View.VISIBLE);
        }
        MedicineAPIsClass.get_my_medicines(
                base,
                page,
                new IResponse() {
                    @Override
                    public void onResponse() {
                        error_happend(type);
                    }

                    @Override
                    public void onResponse(Object json) {

                        String j = new Gson().toJson(json);
                        try {
                            OwnerMedicinesResponse success = new Gson().fromJson(j,OwnerMedicinesResponse.class);
                            if (success.getData()!=null){
                                if (success.getData().size()>0){
                                    process_data(type);
                                    search.setVisibility(View.VISIBLE);
                                    for (OwnerMedicineObject o : success.getData()){
                                        list.add(o);
                                        adapter.notifyDataSetChanged();
                                    }
                                }else {
                                    no_data(type);
                                }
                            }else {
                                error_happend(type);
                            }
                        }catch (Exception e){
                            callMyMedicinesAPI(page,type);
                        }
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        error_happend(type);
                        Snackbar.make(root, getResources().getString(R.string.no_internet), Snackbar.LENGTH_INDEFINITE)
                                .setAction("إعادة المحاولة", new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        callMyMedicinesAPI(page,type);
                                    }
                                }).setActionTextColor(getResources().getColor(R.color.white)).show();
                    }
                }
        );
    }

    private void callSearchAPI(final int page,final int type,String text){
        api_type = 2;
        no_internet.setVisibility(View.GONE);
        no_data.setVisibility(View.GONE);
        error_layout.setVisibility(View.GONE);
        search.setVisibility(View.GONE);
        if (type == 1){
            loading_more.smoothToShow();
        }else {
            shimmer.startShimmer();
            shimmer.setVisibility(View.VISIBLE);
        }
        SearchAPIsClass.search_stock_products(
                base,
                String.valueOf(SharedPrefManager.getInstance(base).getUser().getId()),
                text,
                page,
                new IResponse() {
                    @Override
                    public void onResponse() {
                        error_happend(type);
                    }

                    @Override
                    public void onResponse(Object json) {
                        String j = new Gson().toJson(json);
                        try {
                            OwnerMedicinesResponse success = new Gson().fromJson(j, OwnerMedicinesResponse.class);
                            if (success.getData() != null) {
                                if (success.getData().size() > 0) {
                                    process_data(type);
                                    search.setVisibility(View.VISIBLE);
                                    for (OwnerMedicineObject o : success.getData()) {
                                        list.add(o);
                                        adapter.notifyDataSetChanged();
                                    }
                                } else {
                                    no_data(type);
                                }
                            } else {
                                error_happend(type);
                            }
                        }catch (Exception e){
                            callMyMedicinesAPI(page,type);
                        }
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        error_happend(type);
                        Snackbar.make(root, getResources().getString(R.string.no_internet), Snackbar.LENGTH_INDEFINITE)
                                .setAction("إعادة المحاولة", new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        callMyMedicinesAPI(page,type);
                                    }
                                }).setActionTextColor(getResources().getColor(R.color.white)).show();
                    }
                });
    }

    private void error_happend(int type){
        if (type == 1){
            loading_more.smoothToHide();
        }else {
            shimmer.stopShimmer();
            shimmer.setVisibility(View.GONE);
            no_data.setVisibility(View.VISIBLE);
        }
    }
    private void process_data(int type){
        if (type == 0){
            no_data.setVisibility(View.GONE);
            shimmer.stopShimmer();
            shimmer.setVisibility(View.GONE);
            list.clear();
            adapter = new OwnerMedicineAdapter(base,list);
            recycler.setAdapter(adapter);
        }else {
            loading_more.smoothToHide();
        }
    }
    private void no_data(int type){
        if (type == 0){
            no_data.setVisibility(View.VISIBLE);
            shimmer.stopShimmer();
            shimmer.setVisibility(View.GONE);
            list.clear();
            adapter = new OwnerMedicineAdapter(base,list);
            recycler.setAdapter(adapter);
        }else {
            loading_more.smoothToHide();
            Snackbar.make(root,"لا يوجد المزيد",Snackbar.LENGTH_SHORT).show();
            continue_paginate = false;
        }
    }
}
