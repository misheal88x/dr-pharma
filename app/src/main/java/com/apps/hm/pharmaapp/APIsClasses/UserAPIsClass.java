package com.apps.hm.pharmaapp.APIsClasses;

import android.content.Context;

import com.apps.hm.pharmaapp.APIs.UserAPIs;
import com.apps.hm.pharmaapp.Bases.App;
import com.apps.hm.pharmaapp.Bases.BaseFunctions;
import com.apps.hm.pharmaapp.Bases.BaseRetrofit;
import com.apps.hm.pharmaapp.Interfaces.IFailure;
import com.apps.hm.pharmaapp.Interfaces.IResponse;
import com.apps.hm.pharmaapp.Models.BaseResponse;
import com.apps.hm.pharmaapp.Models.UserObject;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.http.Part;

public class UserAPIsClass extends BaseRetrofit {

    public static void register(final Context context,
                                    String name,
                                    String phone,
                                    String password,
                                    String role_id,
                                    String user_name,
                                    String address,
                                    String location,
                                    String image,
                                    IResponse onResponse1,
                                    final IFailure onFailure1){
        onResponse = onResponse1;
        onFailure = onFailure1;

        App.is_api_called = true;
        //Name
        RequestBody name_request = BaseFunctions.uploadFileStringConverter(name);
        //Phone
        RequestBody phone_request = BaseFunctions.uploadFileStringConverter(phone);
        //Password
        RequestBody password_request = BaseFunctions.uploadFileStringConverter(password);
        //Role
        RequestBody role_request = BaseFunctions.uploadFileStringConverter(role_id);
        //User_name
        RequestBody username_request = BaseFunctions.uploadFileStringConverter(user_name);
        //Address
        RequestBody address_request = BaseFunctions.uploadFileStringConverter(address);
        //Location
        RequestBody location_request = BaseFunctions.uploadFileStringConverter(location);
        //Image
        MultipartBody.Part body = BaseFunctions.uploadFileImageConverter("image", image);

        Retrofit retrofit = configureRetrofitWithoutBearer();
        UserAPIs api = retrofit.create(UserAPIs.class);
        Call<BaseResponse> call = api.register("application/json",
                name_request,
                phone_request,
                password_request,
                role_request,
                username_request,
                address_request,
                location_request,
                body);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                BaseFunctions.processResponse(context,response,onResponse);
                App.is_api_called = false;
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                App.is_api_called = false;
                onFailure.onFailure();
            }
        });
    }

    public static void login(final Context context,
                                String user_name,
                                String password,
                                IResponse onResponse1,
                                final IFailure onFailure1){
        onResponse = onResponse1;
        onFailure = onFailure1;

        App.is_api_called = true;
        Retrofit retrofit = configureRetrofitWithoutBearer();
        UserAPIs api = retrofit.create(UserAPIs.class);
        Call<BaseResponse> call = api.login("application/json",
                user_name,
                password);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                App.is_api_called = false;
                BaseFunctions.processResponse(context,response,onResponse);
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                App.is_api_called = false;
                onFailure.onFailure();
            }
        });
    }

    public static void update_fcm(final Context context,
                             String token,
                             IResponse onResponse1,
                             final IFailure onFailure1){
        onResponse = onResponse1;
        onFailure = onFailure1;

        App.is_api_called = true;
        Retrofit retrofit = configureRetrofitWithBearer(context);
        UserAPIs api = retrofit.create(UserAPIs.class);
        Call<BaseResponse> call = api.update_fcm("application/json",
                token);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                App.is_api_called = false;
                BaseFunctions.processResponse(context,response,onResponse);
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                App.is_api_called = false;
                onFailure.onFailure();
            }
        });
    }
}
