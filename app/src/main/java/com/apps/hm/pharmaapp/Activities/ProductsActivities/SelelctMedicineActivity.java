package com.apps.hm.pharmaapp.Activities.ProductsActivities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.apps.hm.pharmaapp.APIsClasses.CategoriesAPIsClass;
import com.apps.hm.pharmaapp.APIsClasses.MedicineAPIsClass;
import com.apps.hm.pharmaapp.Adapters.CategoryProductsAdapter;
import com.apps.hm.pharmaapp.Bases.BaseActivity;
import com.apps.hm.pharmaapp.Bases.BaseFunctions;
import com.apps.hm.pharmaapp.Interfaces.IFailure;
import com.apps.hm.pharmaapp.Interfaces.IProduct;
import com.apps.hm.pharmaapp.Interfaces.IResponse;
import com.apps.hm.pharmaapp.Models.MedicineObject;
import com.apps.hm.pharmaapp.Models.MedicinesResponse;
import com.apps.hm.pharmaapp.R;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.android.material.snackbar.Snackbar;
import com.google.gson.Gson;
import com.wang.avi.AVLoadingIndicatorView;

import java.util.ArrayList;
import java.util.List;

public class SelelctMedicineActivity extends BaseActivity implements IProduct {

    private RelativeLayout toolbar;
    private TextView title;
    private ImageView back,extra;
    private ImageView go_to_top;
    private EditText search;
    private RecyclerView recycler;
    private List<MedicineObject> list;
    private CategoryProductsAdapter adapter;
    private GridLayoutManager layoutManager;
    private AVLoadingIndicatorView loading_more;
    private LinearLayout no_data,no_internet,error_layout;
    private ShimmerFrameLayout shimmer;
    private NestedScrollView scrollView;
    private int current_page = 1;
    private boolean continue_paginate = true;
    private int per_page = 10;
    private RelativeLayout root;
    private int api_type = 0;

    @Override
    public void set_layout() {
        setContentView(R.layout.activity_selelct_medicine);
    }

    @Override
    public void init_activity(Bundle savedInstanceState) {
        title.setText("اختر دواء");
        init_recycler();
        callGetMedicines(current_page,0);
    }

    @Override
    public void init_views() {
        toolbar = findViewById(R.id.toolbar);
        title = toolbar.findViewById(R.id.title);
        back = toolbar.findViewById(R.id.back);
        extra = toolbar.findViewById(R.id.extra);

        search = findViewById(R.id.search);
        recycler = findViewById(R.id.recycler);

        scrollView = findViewById(R.id.scrollView);
        loading_more = findViewById(R.id.loading_more);
        no_data = findViewById(R.id.no_data_layout);
        no_internet = findViewById(R.id.no_internet_layout);
        error_layout = findViewById(R.id.error_layout);
        shimmer = findViewById(R.id.shimmer);
        root = findViewById(R.id.layout);

        go_to_top = findViewById(R.id.go_to_top);

    }

    @Override
    public void init_events() {
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                BaseFunctions.runBackSlideAnimation(SelelctMedicineActivity.this);
            }
        });

        error_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (api_type == 1) {
                    callGetMedicines(current_page, 0);
                }else if (api_type == 2){
                    callSearchAPI(current_page,0,search.getText().toString());
                }
            }
        });

        no_internet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (api_type == 1) {
                    callGetMedicines(current_page, 0);
                }else if (api_type == 2){
                    callSearchAPI(current_page,0,search.getText().toString());
                }
            }
        });
        go_to_top.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                scrollToTop();
            }
        });

        scrollView.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
            @Override
            public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                if (scrollY > 400) go_to_top.setVisibility(View.VISIBLE);
                else
                    go_to_top.setVisibility(View.GONE);
                if(v.getChildAt(v.getChildCount() - 1) != null) {
                    if ((scrollY >= (v.getChildAt(v.getChildCount() - 1).getMeasuredHeight() - v.getMeasuredHeight())) &&
                            scrollY > oldScrollY) {
                        if (list.size()>=per_page){
                            if (continue_paginate){
                                current_page++;
                                if (api_type == 1){
                                    callGetMedicines(current_page,1);
                                }else if (api_type == 2){
                                    callSearchAPI(current_page,1,search.getText().toString());
                                }
                            }
                        }
                    }
                }
            }
        });

        search.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    if (search.getText().toString().equals("")){
                        BaseFunctions.showErrorToast(SelelctMedicineActivity.this,"يجب عليك كتابة اسم الدواء");
                        return false;
                    }else {
                        BaseFunctions.showWarningToast(SelelctMedicineActivity.this,"جاري البحث..");
                        current_page = 1;
                        continue_paginate = true;
                        callSearchAPI(current_page,0,search.getText().toString());
                    }
                    return true;
                }
                return false;
            }
        });

        search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() == 0){
                    current_page = 1;
                    continue_paginate = true;
                    callGetMedicines(current_page,0);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    @Override
    public void set_fragment_place() {

    }

    private void init_recycler(){
        list = new ArrayList<>();
        adapter = new CategoryProductsAdapter(this,list,0,2,this);
        layoutManager = new GridLayoutManager(this,2);
        recycler.setLayoutManager(layoutManager);
        recycler.setAdapter(adapter);
    }

    @Override
    public void onBackPressed() {
        finish();
        BaseFunctions.runBackSlideAnimation(SelelctMedicineActivity.this);
    }

    private void callGetMedicines(final int page,final int type){
        api_type = 1;
        no_internet.setVisibility(View.GONE);
        no_data.setVisibility(View.GONE);
        error_layout.setVisibility(View.GONE);
        search.setVisibility(View.GONE);
        if (type == 1){
            loading_more.smoothToShow();
        }else {
            shimmer.startShimmer();
            shimmer.setVisibility(View.VISIBLE);
        }
        CategoriesAPIsClass.get_medicines(
                SelelctMedicineActivity.this,
                page,
                new IResponse() {
                    @Override
                    public void onResponse() {
                        error_happend(type);
                    }

                    @Override
                    public void onResponse(Object json) {
                        if (json!=null) {
                            String j = new Gson().toJson(json);try {
                                MedicinesResponse success = new Gson().fromJson(j, MedicinesResponse.class);
                                if (success.getData() != null) {
                                    if (success.getData().size() > 0) {
                                        process_data(type);
                                        search.setVisibility(View.VISIBLE);
                                        for (MedicineObject o : success.getData()) {
                                            list.add(o);
                                            adapter.notifyDataSetChanged();
                                        }
                                    } else {
                                        no_data(type);
                                    }

                                } else {
                                    error_happend(type);
                                }
                            }catch (Exception e){
                                callGetMedicines(page,type);
                            }
                        }else {
                            error_happend(type);
                        }
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        error_happend(type);
                        Snackbar.make(root, getResources().getString(R.string.no_internet), Snackbar.LENGTH_INDEFINITE)
                                .setAction("إعادة المحاولة", new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        callGetMedicines(page,type);
                                    }
                                }).setActionTextColor(getResources().getColor(R.color.white)).show();
                    }
                }
        );
    }

    private void callSearchAPI(final int page,final int type, String text){
        api_type = 2;
        no_internet.setVisibility(View.GONE);
        no_data.setVisibility(View.GONE);
        error_layout.setVisibility(View.GONE);
        search.setVisibility(View.GONE);
        if (type == 1){
            loading_more.smoothToShow();
        }else {
            shimmer.startShimmer();
            shimmer.setVisibility(View.VISIBLE);
        }
        MedicineAPIsClass.search_in_all_medicines(
                SelelctMedicineActivity.this,
                text,
                page,
                new IResponse() {
                    @Override
                    public void onResponse() {
                        error_happend(type);
                    }

                    @Override
                    public void onResponse(Object json) {
                        if (json!=null) {
                            String j = new Gson().toJson(json);
                            try {
                                MedicinesResponse success = new Gson().fromJson(j, MedicinesResponse.class);
                                if (success.getData() != null) {
                                    if (success.getData().size() > 0) {
                                        process_data(type);
                                        search.setVisibility(View.VISIBLE);
                                        for (MedicineObject o : success.getData()) {
                                            list.add(o);
                                            adapter.notifyDataSetChanged();
                                        }
                                    } else {
                                        no_data(type);
                                    }
                                } else {
                                    error_happend(type);
                                }
                            }catch (Exception e){
                                callGetMedicines(page,type);
                            }
                        }else {
                            error_happend(type);
                        }
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        error_happend(type);
                        Snackbar.make(root, getResources().getString(R.string.no_internet), Snackbar.LENGTH_INDEFINITE)
                                .setAction("إعادة المحاولة", new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        callGetMedicines(page,type);
                                    }
                                }).setActionTextColor(getResources().getColor(R.color.white)).show();
                    }
                }
        );
    }

    private void error_happend(int type){
        if (type == 1){
            loading_more.smoothToHide();
        }else {
            shimmer.stopShimmer();
            shimmer.setVisibility(View.GONE);
            no_data.setVisibility(View.VISIBLE);
        }
    }
    private void process_data(int type){
        if (type == 0){
            no_data.setVisibility(View.GONE);
            shimmer.stopShimmer();
            shimmer.setVisibility(View.GONE);
            list.clear();
            adapter = new CategoryProductsAdapter(this,list,0,2,this);
            recycler.setAdapter(adapter);
        }else {
            loading_more.smoothToHide();
        }
    }
    private void no_data(int type){
        if (type == 0){
            no_data.setVisibility(View.VISIBLE);
            shimmer.stopShimmer();
            shimmer.setVisibility(View.GONE);
            list.clear();
            adapter = new CategoryProductsAdapter(this,list,0,2,this);
            recycler.setAdapter(adapter);
        }else {
            loading_more.smoothToHide();
            Snackbar.make(root,"لا يوجد المزيد",Snackbar.LENGTH_SHORT).show();
            continue_paginate = false;
        }
    }

    @Override
    public void onAddToCart(int position) {

    }

    @Override
    public void onRemoveFromCart(int position) {

    }

    @Override
    public void onRemove(int position) {

    }

    @Override
    public void onSelect(int position) {
        int id = list.get(position).getId();
        Intent intent = new Intent();
        intent.putExtra("id",id);
        setResult(RESULT_OK,intent);
        finish();
    }

    public void scrollToTop() {
        int x = 0;
        int y = 0;

        ObjectAnimator xTranslate = ObjectAnimator.ofInt(scrollView, "scrollX", x);
        ObjectAnimator yTranslate = ObjectAnimator.ofInt(scrollView, "scrollY", y);

        AnimatorSet animators = new AnimatorSet();
        animators.setDuration(1000L);
        animators.playTogether(xTranslate, yTranslate);

        animators.addListener(new Animator.AnimatorListener() {

            @Override
            public void onAnimationStart(Animator arg0) {

            }

            @Override
            public void onAnimationRepeat(Animator arg0) {


            }

            @Override
            public void onAnimationEnd(Animator arg0) {


            }

            @Override
            public void onAnimationCancel(Animator arg0) {


            }
        });
        animators.start();
    }
}
