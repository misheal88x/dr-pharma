package com.apps.hm.pharmaapp.Activities.PharmaActivities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.apps.hm.pharmaapp.APIsClasses.OffersAPIsClass;
import com.apps.hm.pharmaapp.APIsClasses.SearchAPIsClass;
import com.apps.hm.pharmaapp.Activities.HomesActivities.PharmaHomeActivity;
import com.apps.hm.pharmaapp.Activities.ProductsActivities.CategoryProductsActivity;
import com.apps.hm.pharmaapp.Adapters.PharmaOffersAdapter;
import com.apps.hm.pharmaapp.Bases.BaseActivity;
import com.apps.hm.pharmaapp.Bases.BaseFunctions;
import com.apps.hm.pharmaapp.Interfaces.IFailure;
import com.apps.hm.pharmaapp.Interfaces.IResponse;
import com.apps.hm.pharmaapp.Models.OfferObject;
import com.apps.hm.pharmaapp.Models.OffersResponse;
import com.apps.hm.pharmaapp.Models.UserObject;
import com.apps.hm.pharmaapp.R;
import com.apps.hm.pharmaapp.Utils.EndlessRecyclerViewScrollListener;
import com.facebook.shimmer.Shimmer;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.android.material.snackbar.Snackbar;
import com.google.gson.Gson;
import com.wang.avi.AVLoadingIndicatorView;

import java.util.ArrayList;
import java.util.List;

public class PharmaOffersActivity extends BaseActivity {

    private RelativeLayout toolbar;
    private TextView title;
    private ImageView back,extra;
    private RecyclerView recycler;
    private List<OfferObject> list;
    private ShimmerFrameLayout shimmer;
    private PharmaOffersAdapter adapter;
    private LinearLayout no_data,no_internet,error_layout;
    private LinearLayoutManager layoutManager;
    private RelativeLayout root;

    private int current_page = 1;
    private boolean continue_paginate = true;
    private int per_page = 10;

    private AVLoadingIndicatorView loading_more;
    @Override
    public void set_layout() {
        setContentView(R.layout.activity_pharma_offers);
    }

    @Override
    public void init_activity(Bundle savedInstanceState) {
        title.setText("العروض");
        init_recycler();
        callOffersAPI(current_page,0);
    }

    @Override
    public void init_views() {
        toolbar = findViewById(R.id.toolbar);
        title = toolbar.findViewById(R.id.title);
        back = toolbar.findViewById(R.id.back);
        extra = toolbar.findViewById(R.id.extra);
        recycler = findViewById(R.id.recycler);
        shimmer = findViewById(R.id.shimmer);
        no_data = findViewById(R.id.no_data_layout);
        no_internet = findViewById(R.id.no_internet_layout);
        error_layout = findViewById(R.id.error_layout);
        loading_more = findViewById(R.id.loading_more);
        root = findViewById(R.id.layout);
    }

    @Override
    public void init_events() {
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                BaseFunctions.runBackSlideAnimation(PharmaOffersActivity.this);
            }
        });

        error_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callOffersAPI(current_page,0);
            }
        });

        no_internet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callOffersAPI(current_page,0);
            }
        });
    }

    @Override
    public void set_fragment_place() {

    }

    @Override
    public void onBackPressed() {
        finish();
        BaseFunctions.runBackSlideAnimation(PharmaOffersActivity.this);
    }

    private void init_recycler(){
        list = new ArrayList<>();
        adapter = new PharmaOffersAdapter(PharmaOffersActivity.this,list);
        layoutManager = new LinearLayoutManager(PharmaOffersActivity.this);
        recycler.setLayoutManager(layoutManager);
        recycler.setAdapter(adapter);
        recycler.addOnScrollListener(new EndlessRecyclerViewScrollListener(layoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                if (list.size()>=per_page){
                    if (continue_paginate){
                        current_page++;
                        callOffersAPI(current_page,1);
                    }
                }
            }
        });
    }

    private void callOffersAPI(final int page, final int type){
        if (type == 1){
            loading_more.smoothToShow();
        }else {
            shimmer.startShimmer();
            shimmer.setVisibility(View.VISIBLE);
        }
        OffersAPIsClass.get_offer(
                PharmaOffersActivity.this,
                page,
                new IResponse() {
                    @Override
                    public void onResponse() {
                        error_happend(type);
                    }

                    @Override
                    public void onResponse(Object json) {
                        String j = new Gson().toJson(json);
                        try {
                            OffersResponse success = new Gson().fromJson(j, OffersResponse.class);
                            if (success.getData() != null) {
                                if (success.getData().size() > 0) {
                                    process_data(type);
                                    per_page = success.getPer_page();
                                    for (OfferObject o : success.getData()) {
                                        if (!list.contains(o)){
                                            list.add(o);
                                            adapter.notifyDataSetChanged();
                                        }
                                    }
                                } else {
                                    no_data(type);
                                }
                            } else {
                                error_happend(type);
                            }
                        }catch (Exception e){
                            callOffersAPI(page,type);
                        }
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        shimmer.stopShimmer();
                        shimmer.setVisibility(View.GONE);
                        no_internet.setVisibility(View.VISIBLE);
                    }
                }
        );
    }

    private void error_happend(int type){
        if (type == 1){
            loading_more.smoothToHide();
        }else {
            shimmer.stopShimmer();
            shimmer.setVisibility(View.GONE);
            no_data.setVisibility(View.VISIBLE);
        }
    }
    private void process_data(int type){
        if (type == 0){
            no_data.setVisibility(View.GONE);
            shimmer.stopShimmer();
            shimmer.setVisibility(View.GONE);
        }else {
            loading_more.smoothToHide();
        }
    }
    private void no_data(int type){
        if (type == 0){
            no_data.setVisibility(View.VISIBLE);
            shimmer.stopShimmer();
            shimmer.setVisibility(View.GONE);
        }else {
            loading_more.smoothToHide();
            Snackbar.make(root,"لا يوجد المزيد",Snackbar.LENGTH_SHORT).show();
            continue_paginate = false;
        }
    }
}
