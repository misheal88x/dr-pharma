package com.apps.hm.pharmaapp.Interfaces;

public interface IProduct {
    void onAddToCart(int position);
    void onRemoveFromCart(int position);
    void onRemove(int position);
    void onSelect(int position);
}
