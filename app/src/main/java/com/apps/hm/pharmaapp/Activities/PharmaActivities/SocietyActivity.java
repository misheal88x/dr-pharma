package com.apps.hm.pharmaapp.Activities.PharmaActivities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.apps.hm.pharmaapp.APIsClasses.NewsAPIsClass;
import com.apps.hm.pharmaapp.Activities.ProductsActivities.CategoryProductsActivity;
import com.apps.hm.pharmaapp.Adapters.SocietyAdapter;
import com.apps.hm.pharmaapp.Bases.BaseActivity;
import com.apps.hm.pharmaapp.Bases.BaseFunctions;
import com.apps.hm.pharmaapp.Bases.SharedPrefManager;
import com.apps.hm.pharmaapp.Interfaces.IFailure;
import com.apps.hm.pharmaapp.Interfaces.IResponse;
import com.apps.hm.pharmaapp.Models.NewsObject;
import com.apps.hm.pharmaapp.Models.NewsResponse;
import com.apps.hm.pharmaapp.R;
import com.apps.hm.pharmaapp.Utils.EndlessRecyclerViewScrollListener;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.android.material.snackbar.Snackbar;
import com.google.gson.Gson;
import com.wang.avi.AVLoadingIndicatorView;

import java.util.ArrayList;
import java.util.List;

public class SocietyActivity extends BaseActivity {

    private RelativeLayout toolbar;
    private TextView title;
    private ImageView back,extra;

    private RecyclerView recycler;
    private List<NewsObject> list;
    private SocietyAdapter adapter;
    private LinearLayoutManager layoutManager;

    private LinearLayout no_data,no_internet,error_layout;
    private ShimmerFrameLayout shimmer;

    private int current_page = 1;
    private boolean continue_paginate = true;
    private int per_page = 10;

    private AVLoadingIndicatorView loading_more;
    private RelativeLayout root;
    @Override
    public void set_layout() {
        setContentView(R.layout.activity_society);
    }

    @Override
    public void init_activity(Bundle savedInstanceState) {
        title.setText("صيدلية مجتمع");
        init_recycler();
        callGetNewsAPI(current_page,0);
    }

    @Override
    public void init_views() {
        toolbar = findViewById(R.id.toolbar);
        title = toolbar.findViewById(R.id.title);
        back = toolbar.findViewById(R.id.back);
        extra = toolbar.findViewById(R.id.extra);
        recycler = findViewById(R.id.recycler);
        no_data = findViewById(R.id.no_data_layout);
        no_internet = findViewById(R.id.no_internet_layout);
        error_layout = findViewById(R.id.error_layout);
        shimmer = findViewById(R.id.shimmer);
        loading_more = findViewById(R.id.loading_more);
        root = findViewById(R.id.layout);
    }

    @Override
    public void init_events() {
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                BaseFunctions.runBackSlideAnimation(SocietyActivity.this);
            }
        });
        error_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callGetNewsAPI(current_page,0);
            }
        });

        no_internet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callGetNewsAPI(current_page,0);
            }
        });
    }

    @Override
    public void set_fragment_place() {

    }

    private void init_recycler(){
        list = new ArrayList<>();
        adapter = new SocietyAdapter(this,list);
        layoutManager = new LinearLayoutManager(this);
        recycler.setLayoutManager(layoutManager);
        recycler.setAdapter(adapter);
        recycler.getItemAnimator().setChangeDuration(0);
        recycler.addOnScrollListener(new EndlessRecyclerViewScrollListener(layoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                if (list.size()>=per_page){
                    if (continue_paginate){
                        current_page++;
                        callGetNewsAPI(current_page,1);
                    }
                }
            }
        });
    }

    @Override
    public void onBackPressed() {
        finish();
        BaseFunctions.runBackSlideAnimation(SocietyActivity.this);
    }

    private void callGetNewsAPI(final int page, final int type){
        SharedPrefManager.getInstance(SocietyActivity.this).setNewsDot(false);
        if (type == 1){
            loading_more.smoothToShow();
        }else {
            shimmer.startShimmer();
            shimmer.setVisibility(View.VISIBLE);
        }
        NewsAPIsClass.get_news(
                SocietyActivity.this,
                page,
                new IResponse() {
                    @Override
                    public void onResponse() {
                        error_happend(type);
                    }

                    @Override
                    public void onResponse(Object json) {
                        if (json!=null){
                            String j = new Gson().toJson(json);
                            try {
                                NewsResponse success = new Gson().fromJson(j, NewsResponse.class);
                                if (success.getData() != null) {
                                    if (success.getData().size() > 0) {
                                        process_data(type);
                                        per_page = success.getPer_page();
                                        for (NewsObject po : success.getData()) {
                                            list.add(po);
                                            adapter.notifyDataSetChanged();
                                        }
                                    } else {
                                        no_data(type);
                                    }
                                } else {
                                    error_happend(type);
                                }
                            }catch (Exception e){
                                callGetNewsAPI(page,type);
                            }
                        }else {
                            error_happend(type);
                        }
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        error_happend(type);
                        Snackbar.make(root, getResources().getString(R.string.no_internet), Snackbar.LENGTH_INDEFINITE)
                                .setAction("إعادة المحاولة", new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        callGetNewsAPI(page,type);
                                    }
                                }).setActionTextColor(getResources().getColor(R.color.white)).show();
                    }
                }
        );
    }

    private void error_happend(int type){
        if (type == 1){
            loading_more.smoothToHide();
        }else {
            shimmer.stopShimmer();
            shimmer.setVisibility(View.GONE);
            no_data.setVisibility(View.VISIBLE);
        }
    }
    private void process_data(int type){
        if (type == 0){
            no_data.setVisibility(View.GONE);
            shimmer.stopShimmer();
            shimmer.setVisibility(View.GONE);
        }else {
            loading_more.smoothToHide();
        }
    }
    private void no_data(int type){
        if (type == 0){
            no_data.setVisibility(View.VISIBLE);
            shimmer.stopShimmer();
            shimmer.setVisibility(View.GONE);
        }else {
            loading_more.smoothToHide();
            Snackbar.make(root,"لا يوجد المزيد",Snackbar.LENGTH_SHORT).show();
            continue_paginate = false;
        }
    }
}
