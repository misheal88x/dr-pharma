package com.apps.hm.pharmaapp.Bases;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;

import com.apps.hm.pharmaapp.R;
import com.apps.hm.pharmaapp.Utils.LocaleHelper;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

public abstract class BaseActivity extends AppCompatActivity {
    protected ViewGroup fragment_place;
    public BaseFragment current_fragment;
    public SharedPreferences sharedPreferences;
    public SharedPreferences.Editor editor;
    private App app;
    public FragmentManager fragmentManager;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(LocaleHelper.onAttach(newBase));
        //super.attachBaseContext(newBase);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LocaleHelper.setLocale(this, SharedPrefManager.getInstance(this).getViewLanguage());
        fragmentManager = getSupportFragmentManager();
        set_layout();
        init_views();
        set_fragment_place();
        init_events();
        init_activity(savedInstanceState);
        changeStatusBarColor();
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }

    public abstract void set_layout();

    public abstract void init_activity(Bundle savedInstanceState);

    public abstract void init_views();

    public abstract void init_events();

    public abstract void set_fragment_place();


   /* public void send_data(DataMessage message){
        message.get_receiver().onReceive(message.get_extra());
    }

    public void send_data(ObjectMessage message){
        message.get_receiver().onReceive(message.get_object());
    }*/

    //To open specific fragment
    public void open_fragment(final BaseFragment fragment) {
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                //if (current_fragment != fragment) {
                //try {
                FragmentTransaction transaction = fragmentManager.beginTransaction();
                transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
                transaction.replace(fragment_place.getId(), fragment);
                transaction.addToBackStack("Home");
                transaction.commit();
                //if (current_fragment!=null){
                //    fragment.set_previous(current_fragment);
                // }
                // current_fragment = fragment;
                //}catch (Exception e){
                //e.printStackTrace();
                // }
                //}
            }
        };
        runOnUiThread(runnable);
    }

   /* @Override
    public void onBackPressed() {
        if (current_fragment != null && current_fragment.get_previous_fragment() != null) {
            open_fragment(current_fragment.get_previous_fragment());
        }else {
            super.onBackPressed();
        }
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }

    */

    public void changeStatusBarColor(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(R.color.light_grey));
            View decor = getWindow().getDecorView();
            decor.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        }
    }
}
