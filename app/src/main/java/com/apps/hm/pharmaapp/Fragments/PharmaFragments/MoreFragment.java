package com.apps.hm.pharmaapp.Fragments.PharmaFragments;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.apps.hm.pharmaapp.Activities.AccountActivities.SelectAccountTypeActivity;
import com.apps.hm.pharmaapp.Activities.AccountActivities.SelectLoginTypeActivity;
import com.apps.hm.pharmaapp.Activities.PharmaActivities.PharmaOffersActivity;
import com.apps.hm.pharmaapp.Activities.PharmaActivities.SocietyActivity;
import com.apps.hm.pharmaapp.Bases.BaseFragment;
import com.apps.hm.pharmaapp.Bases.SharedPrefManager;
import com.apps.hm.pharmaapp.Models.UserObject;
import com.apps.hm.pharmaapp.R;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;

public class MoreFragment extends BaseFragment {

    private LinearLayout society_layout,offers_layout,logout_layout;
    private TextView social_dot;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_more,container,false);
    }
    @Override
    public void init_views() {
        society_layout = base.findViewById(R.id.society_layout);
        offers_layout = base.findViewById(R.id.offers_layout);
        logout_layout = base.findViewById(R.id.logout);
    }

    @Override
    public void init_events() {
        society_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(base, SocietyActivity.class));
            }
        });
        offers_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(base, PharmaOffersActivity.class));
            }
        });
        logout_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(base);
                builder.setMessage("هل أنت متأكد من تسجيل الخروج ؟");
                builder.setPositiveButton("نعم", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        SharedPrefManager.getInstance(base).setUser(new UserObject());
                        startActivity(new Intent(base, SelectAccountTypeActivity.class));
                        base.finish();
                    }
                });
                builder.setNegativeButton("لا", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                builder.show();
            }
        });
    }

    @Override
    public void init_fragment(Bundle savedInstanceState) {

    }

    @Override
    public void onResume() {
        super.onResume();
        social_dot = base.findViewById(R.id.social_dot);
        if (SharedPrefManager.getInstance(base).getNewsDot()){
            social_dot.setVisibility(View.VISIBLE);
        }else {
            social_dot.setVisibility(View.GONE);
        }
    }
}
