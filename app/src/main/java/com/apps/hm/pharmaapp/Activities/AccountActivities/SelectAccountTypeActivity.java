package com.apps.hm.pharmaapp.Activities.AccountActivities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.RelativeLayout;

import com.apps.hm.pharmaapp.Activities.HomesActivities.PharmaHomeActivity;
import com.apps.hm.pharmaapp.Activities.HomesActivities.StockOwnerHomeActivity;
import com.apps.hm.pharmaapp.Bases.BaseActivity;
import com.apps.hm.pharmaapp.Bases.SharedPrefManager;
import com.apps.hm.pharmaapp.R;

public class SelectAccountTypeActivity extends BaseActivity {

    private RelativeLayout pharmacist,stock_owner;
    @Override
    public void set_layout() {
        setContentView(R.layout.activity_select_account_type);
    }

    @Override
    public void init_activity(Bundle savedInstanceState) {
        if (SharedPrefManager.getInstance(SelectAccountTypeActivity.this).getUser().getId() == 0){

        }else {
            if (SharedPrefManager.getInstance(SelectAccountTypeActivity.this).getUser().getRole_id() == 2){
                startActivity(new Intent(SelectAccountTypeActivity.this, PharmaHomeActivity.class));
                finish();
            }else {
                startActivity(new Intent(SelectAccountTypeActivity.this, StockOwnerHomeActivity.class));
                finish();
            }
        }
    }

    @Override
    public void init_views() {
        pharmacist = findViewById(R.id.pharmacist);
        stock_owner = findViewById(R.id.stock_owner);
    }

    @Override
    public void init_events() {
        pharmacist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SelectAccountTypeActivity.this,SelectLoginTypeActivity.class);
                intent.putExtra("account_type",2);
                startActivity(intent);
            }
        });
        stock_owner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SelectAccountTypeActivity.this,SelectLoginTypeActivity.class);
                intent.putExtra("account_type",3);
                startActivity(intent);
            }
        });
    }

    @Override
    public void set_fragment_place() {

    }
}
