package com.apps.hm.pharmaapp.APIsClasses;

import android.content.Context;

import com.apps.hm.pharmaapp.APIs.NewsAPIs;
import com.apps.hm.pharmaapp.APIs.OffersAPIs;
import com.apps.hm.pharmaapp.Bases.App;
import com.apps.hm.pharmaapp.Bases.BaseFunctions;
import com.apps.hm.pharmaapp.Bases.BaseRetrofit;
import com.apps.hm.pharmaapp.Interfaces.IFailure;
import com.apps.hm.pharmaapp.Interfaces.IResponse;
import com.apps.hm.pharmaapp.Models.BaseResponse;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class OffersAPIsClass extends BaseRetrofit {

    public static void get_offer(final Context context,
                                int page,
                                IResponse onResponse1,
                                final IFailure onFailure1){
        onResponse = onResponse1;
        onFailure = onFailure1;

        App.is_api_called = true;
        Retrofit retrofit = configureRetrofitWithBearer(context);
        OffersAPIs api = retrofit.create(OffersAPIs.class);
        Call<BaseResponse> call = api.get_offers("application/json",page);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                App.is_api_called = false;
                BaseFunctions.processResponse(context,response,onResponse);
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                App.is_api_called = false;
                onFailure.onFailure();
            }
        });
    }

    public static void add_offer(final Context context,
                                 String text,
                                 IResponse onResponse1,
                                 final IFailure onFailure1){
        onResponse = onResponse1;
        onFailure = onFailure1;

        App.is_api_called = true;
        Retrofit retrofit = configureRetrofitWithBearer(context);
        OffersAPIs api = retrofit.create(OffersAPIs.class);
        Call<BaseResponse> call = api.add_offer("application/json",text);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                App.is_api_called = false;
                BaseFunctions.processResponse(context,response,onResponse);
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                App.is_api_called = false;
                onFailure.onFailure();
            }
        });
    }

    public static void get_my_offer(final Context context,
                                 IResponse onResponse1,
                                 final IFailure onFailure1){
        onResponse = onResponse1;
        onFailure = onFailure1;

        App.is_api_called = true;
        Retrofit retrofit = configureRetrofitWithBearer(context);
        OffersAPIs api = retrofit.create(OffersAPIs.class);
        Call<BaseResponse> call = api.get_my_offers("application/json");
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                App.is_api_called = false;
                BaseFunctions.processResponse(context,response,onResponse);
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                App.is_api_called = false;
                onFailure.onFailure();
            }
        });
    }

    public static void delete_offer(final Context context,
                                    String id,
                                    IResponse onResponse1,
                                    final IFailure onFailure1){
        onResponse = onResponse1;
        onFailure = onFailure1;

        App.is_api_called = true;
        Retrofit retrofit = configureRetrofitWithBearer(context);
        OffersAPIs api = retrofit.create(OffersAPIs.class);
        Call<BaseResponse> call = api.delete_offer("application/json",id);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                App.is_api_called = false;
                BaseFunctions.processResponse(context,response,onResponse);
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                App.is_api_called = false;
                onFailure.onFailure();
            }
        });
    }
}
