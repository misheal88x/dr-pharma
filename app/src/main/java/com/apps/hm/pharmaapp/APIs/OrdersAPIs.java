package com.apps.hm.pharmaapp.APIs;

import com.apps.hm.pharmaapp.Models.BaseResponse;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;

public interface OrdersAPIs {
    @GET("get-orders")
    Call<BaseResponse> get_stock_orders(
            @Header("Accept") String accept
    );

    @FormUrlEncoded
    @POST("change-status")
    Call<BaseResponse> update_order_status(
            @Header("Accept") String accept,
            @Field("order_id") int order_id,
            @Field("status") int status
    );

    @GET("get-pharam-orders")
    Call<BaseResponse> get_pharma_orders(
            @Header("Accept") String accept
    );

    @FormUrlEncoded
    @POST("add-order")
    Call<BaseResponse> add_order(
            @Header("Accept") String accept,
            @Field("repo_id") int repo_id,
            @Field("order_count") int order_count,
            @Field("total_price") String total_price,
            @Field("products") String products);
}
