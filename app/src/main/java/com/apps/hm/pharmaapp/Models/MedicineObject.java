package com.apps.hm.pharmaapp.Models;

import com.google.gson.annotations.SerializedName;

public class MedicineObject {
    @SerializedName("id") private int id = 0;
    @SerializedName("name") private String name = "";
    @SerializedName("image") private String image = "";
    @SerializedName("company") private String company = "";
    @SerializedName("count") private int count = 0;
    @SerializedName("user_id") private int user_id = 0;
    @SerializedName("created_at") private String created_at = "";
    @SerializedName("updated_at") private String updated_at = "";
    @SerializedName("ordered_count") private int ordered_count = 0;
    @SerializedName("added_to_cart") private boolean added_to_cart = false;
    @SerializedName("price") private float price = 0.0f;
    @SerializedName("sale_price") private float sale_price = 0.0f;
    @SerializedName("expire_date") private String expire_date = "";
    @SerializedName("total") private float total = 0.0f;
    @SerializedName("co_name") private String co_name = "";
    @SerializedName("shape") private String shape = "";
    @SerializedName("owner") private UserObject owner = new UserObject();
    @SerializedName("medicine") private ExtraMedicineObject medicine = new ExtraMedicineObject();

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public float getSale_price() {
        return sale_price;
    }

    public void setSale_price(float sale_price) {
        this.sale_price = sale_price;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public int getOrdered_count() {
        return ordered_count;
    }

    public void setOrdered_count(int ordered_count) {
        this.ordered_count = ordered_count;
    }

    public boolean isAdded_to_cart() {
        return added_to_cart;
    }

    public void setAdded_to_cart(boolean added_to_cart) {
        this.added_to_cart = added_to_cart;
    }

    public String getExpire_date() {
        return expire_date;
    }

    public void setExpire_date(String expire_date) {
        this.expire_date = expire_date;
    }

    public float getTotal() {
        return total;
    }

    public void setTotal(float total) {
        this.total = total;
    }

    public String getCo_name() {
        return co_name;
    }

    public void setCo_name(String co_name) {
        this.co_name = co_name;
    }

    public UserObject getOwner() {
        return owner;
    }

    public void setOwner(UserObject owner) {
        this.owner = owner;
    }

    public ExtraMedicineObject getMedicine() {
        return medicine;
    }

    public void setMedicine(ExtraMedicineObject medicine) {
        this.medicine = medicine;
    }

    public String getShape() {
        return shape;
    }

    public void setShape(String shape) {
        this.shape = shape;
    }
}
