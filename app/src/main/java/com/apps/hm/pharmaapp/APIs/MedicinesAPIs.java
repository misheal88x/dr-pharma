package com.apps.hm.pharmaapp.APIs;

import com.apps.hm.pharmaapp.Models.BaseResponse;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface MedicinesAPIs {


    @GET("search-med/{text}")
    Call<BaseResponse> search_products(
            @Header("Accept") String accept,
            @Path("text") String text
    );

    @FormUrlEncoded
    @POST("add-medicine-exchange")
    Call<BaseResponse> add_exchange_medicine(
            @Header("Accept") String accept,
            @Field("name") String name,
            @Field("price") String price,
            @Field("count") String count,
            @Field("expire_date") String expire_date,
            @Field("total") String total
    );

    @GET("delete-medicine-exchange/{id}")
    Call<BaseResponse> delete_exchange_medicine(
            @Header("Accept") String accept,
            @Path("id") String id
    );

    @GET("get-medicine-exchange")
    Call<BaseResponse> get_exchange_medicines(
            @Header("Accept") String accept,
            @Query("page") int page
    );

    @GET("my-medicines")
    Call<BaseResponse> get_my_medicines(
            @Header("Accept") String accept,
            @Query("page") int page
    );

    @GET("delete-medicine/{id}")
    Call<BaseResponse> delete_my_medicine(
            @Header("Accept") String accept,
            @Path("id") String id
    );

    @FormUrlEncoded
    @POST("add-medicine")
    Call<BaseResponse> add_my_medicine(
            @Header("Accept") String accept,
            @Field("med_id") int med_id,
            @Field("count") int count

    );

    @GET("search-med/{text}")
    Call<BaseResponse> search_in_all_medicines(
            @Header("Accept") String accept,
            @Path("text") String text,
            @Query("page") int page
    );
}
