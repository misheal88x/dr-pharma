package com.apps.hm.pharmaapp.Bases;

import android.app.Activity;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.media.AudioAttributes;
import android.media.MediaPlayer;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings;
import android.text.Html;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.apps.hm.pharmaapp.Adapters.CustomSpinnerAdapter;
import com.apps.hm.pharmaapp.Interfaces.ILoadImage;
import com.apps.hm.pharmaapp.Interfaces.IResponse;
import com.apps.hm.pharmaapp.Models.BaseResponse;
import com.apps.hm.pharmaapp.R;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.facebook.drawee.drawable.ProgressBarDrawable;
import com.facebook.drawee.view.SimpleDraweeView;
import com.valdesekamdem.library.mdtoast.MDToast;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;
import androidx.recyclerview.widget.RecyclerView;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Response;

public class BaseFunctions {
    public static boolean isOnline(Context context){
        try{
            ConnectivityManager conMgr =  (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
            if (conMgr != null){
                NetworkInfo netInfo = conMgr.getActiveNetworkInfo();
                if (netInfo == null){
                    return false;
                }else {
                    return true;
                }
            }else {
                return false;
            }
        }catch (Exception e){
            Toast.makeText(context, "Crashed because of isOnline function", Toast.LENGTH_SHORT).show();
            return false;
        }
    }
    public static void showNotification(Context context,
                                        PendingIntent pendingIntent,
                                        String message,
                                        NotificationCompat.Builder builder,
                                        NotificationManager notificationManager,
                                        String NOTIFICATION_CHANNEL_ID,
                                        int MY_NOTIFICATION_ID,
                                        String target_type){

        if (pendingIntent!=null){
            builder = new NotificationCompat.Builder(context)
                    .setContentTitle(context.getResources().getString(R.string.app_name))
                    .setContentText(message)
                    .setStyle(new NotificationCompat.BigTextStyle()
                            .bigText(message))
                    .setTicker("new notification")
                    .setWhen(System.currentTimeMillis())
                    .setContentIntent(pendingIntent)
                    .setDefaults(Notification.DEFAULT_SOUND)
                    .setAutoCancel(true)
                    .setSmallIcon(R.drawable.logo_outside)
                    .setVibrate(new long[]{500, 500, 500});
        }else {
            builder = new NotificationCompat.Builder(context)
                    .setContentTitle(context.getResources().getString(R.string.app_name))
                    .setContentText(message)
                    .setStyle(new NotificationCompat.BigTextStyle()
                            .bigText(message))
                    .setTicker("new notification")
                    .setWhen(System.currentTimeMillis())
                    .setDefaults(Notification.DEFAULT_SOUND)
                    .setAutoCancel(true)
                    .setSmallIcon(R.drawable.logo_outside)
                    .setVibrate(new long[]{500, 500, 500});
        }
        notificationManager =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        //notificationManager.notify(MY_NOTIFICATION_ID, myNotification);
        //Toast.makeText(getApplicationContext(), "", Toast.LENGTH_SHORT).show();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel notificationChannel = new NotificationChannel(NOTIFICATION_CHANNEL_ID, "NOTIFICATION_CHANNAEL", importance);
            notificationChannel.enableLights(true);
            notificationChannel.setLightColor(Color.BLUE);
            notificationChannel.enableVibration(true);
            notificationChannel.setVibrationPattern(new long[]{500, 500, 500});
            assert notificationManager != null;
            builder.setChannelId(NOTIFICATION_CHANNEL_ID);
            notificationManager.createNotificationChannel(notificationChannel);
        }
        assert notificationManager != null;
        notificationManager.notify(MY_NOTIFICATION_ID, builder.build());
    }
    public static String dateExtractor(String datetime){
        String myDate = "";
        for (int i = 0; i <datetime.length() ; i++) {
            if (datetime.charAt(i)!='T'){
                myDate+=datetime.charAt(i);
            }else {
                break;
            }
        }
        return myDate;
    }
    public static String timeExtractor(String datetime){
        String myTime = "";
        boolean isTime = false;
        for (int i = 0; i <datetime.length() ; i++) {
            if (datetime.charAt(i)!=' '&&isTime){
                myTime+=datetime.charAt(i);
            }else if (datetime.charAt(i)==' '){
                isTime = true;
            }
        }
        return myTime;
    }
    public static String formatSeconds(long totalSecs) {
        long hours = (int) ((totalSecs) / 3600);
        long minutes = (int) ((totalSecs % 3600) / 60);
        long seconds = (int) (totalSecs % 60);
        return String.format("%02d:%02d:%02d", hours, minutes, seconds);
    }
    public static String languageToNumberConverter(String in){
        String out = "";
        if (in.equals("en")){
            out = "1";
        }else {
            out = "2";
        }
        return out;
    }
    public static String numberToLanguageConverter(String in){
        String out = "";
        if (in.equals("1")){
            out = "en";
        }else {
            out = "ar";
        }
        return out;
    }

    public static String dateToStringConverter(Date input){
        SimpleDateFormat spf= new SimpleDateFormat("yyyy-MM-dd", Locale.US);
        String date = spf.format(input);
        return date;
    }
    public static String getDeviceId(Context context){
        String android_id = Settings.Secure.getString(context.getContentResolver(),
                Settings.Secure.ANDROID_ID);
        return android_id;
    }
    public static void setGlideImage(Context context, ImageView image, String url){
        try{
            Glide.with(context).load(url)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    //.override(600, 230)
                    //.apply(RequestOptions.bitmapTransform(new RoundedCorners(16)))
                    //.placeholder(R.drawable.placeholder)
                    .centerCrop().into(image);
        }catch (Exception e){}
    }
    public static void setGlideImageWithEvents(Context context, ImageView image, String url, final ILoadImage iLoadImage){
        try{
            Glide.with(context).load(url)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    //.placeholder(R.drawable.placeholder)
                    .centerCrop()
                    .error(R.drawable.grey_background)
                    .placeholder(R.drawable.grey_background)
                    .listener(new RequestListener<Drawable>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model,
                                                    Target<Drawable> target, boolean isFirstResource) {
                            iLoadImage.onFailed();
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target,
                                                       DataSource dataSource, boolean isFirstResource) {
                            iLoadImage.onLoaded();
                            return false;
                        }
                    })
                    .into(image);
        }catch (Exception e){}
    }
    public static void setGlideDrawableImage(Context context, ImageView image, int drawable){
        try{
            Glide.with(context).load(drawable)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    //.override(600, 230)
                    //.apply(RequestOptions.bitmapTransform(new RoundedCorners(16)))
                    //.placeholder(R.drawable.placeholder)
                    .centerCrop().into(image);
        }catch (Exception e){}
    }
    public static void setGlideFileImage(Context context, ImageView image, File file){
        try{
            Glide.with(context).load(file)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    //.override(600, 230)
                    //.apply(RequestOptions.bitmapTransform(new RoundedCorners(16)))
                    //.placeholder(R.drawable.placeholder)
                    .centerCrop().into(image);
        }catch (Exception e){}
    }


    public static void processResponse(Context context, Response<BaseResponse> response, IResponse onResponse){

        if (response.isSuccessful()){
            if (response.code() == 200){
                if (response.body()!=null){
                    if (response.body().getStatus() == 1){
                        onResponse.onResponse(response.body().getData());
                    }else {
                        onResponse.onResponse();
                        BaseFunctions.showErrorToast(context,response.body().getMessage());
                    }
                }else {
                    onResponse.onResponse();
                }
            }else {
                onResponse.onResponse();
            }
        }else {
            try {
                BaseFunctions.showErrorToast(context, response.body().getMessage());
            }catch (Exception e){
                BaseFunctions.showErrorToast(context, "يوجد خطأ بالخدمة, الرجاء إعادة المحاولة");
            }
            onResponse.onResponse();
        }
    }



    public static void setFrescoImage(SimpleDraweeView image, String url){
        try {
            Uri uri = Uri.parse(url);
            image.getHierarchy().setProgressBarImage(new ProgressBarDrawable());
            image.setImageURI(uri);
        }catch (Exception e){}
    }
    public static Date stringToDateConverter(String stringDate){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss",Locale.US);
        try {
            Date d = sdf.parse(stringDate.substring(0,19).replace("T"," "));
            return d;
        } catch (ParseException ex) {
            Log.i("jhjhujh", "stringToDateConverter: "+ex.getLocalizedMessage());
            return null;
        }
    }
    public static String dateToDayConverter(Date date){
        SimpleDateFormat sdf = new SimpleDateFormat("EEEE");
        String dayOfTheWeek = sdf.format(date);
        return dayOfTheWeek;
    }

    public static String todayDateString(){
        Date c = Calendar.getInstance().getTime();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String formattedDate = df.format(c);
        return formattedDate;
    }

    public static long deferenceBetweenTwoDates(Date d1,Date d2){
        return d1.getTime()-d2.getTime();
    }

    public static String processDate(Context context,String date){
        Date inputDate = stringToDateConverter(date);
        Date todayDate = stringToDateConverter(todayDateString());
        long i = deferenceBetweenTwoDates(todayDate,inputDate);
        long[] res = processMilliseconds(i);
        if (res[0] != 0){
            return String.valueOf(res[0])+" "+"يوم";
        }else if (res[1] != 0){
            return String.valueOf(res[1])+" "+"ساعة";
        }else if (res[2] != 0){
            return String.valueOf(res[2])+" "+"دقيقة";
        }else {
            return "لحظات";
        }
    }

    public static long[] processMilliseconds(long milli){
        long[] out = new long[]{0,0,0};

        long seconds = milli / 1000;
        long minutes = seconds / 60;
        long hours = minutes / 60;
        long days = hours / 24;

        long day = days;
        //long day = TimeUnit.MILLISECONDS.toDays(milli);

        //long hours = TimeUnit.MILLISECONDS.toHours(milli)
        //- TimeUnit.DAYS.toHours(TimeUnit.MILLISECONDS.toDays(milli));

        //long minutes = TimeUnit.MILLISECONDS.toMinutes(milli)
        //- TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(milli));
        out[0] = day;
        out[1] = hours%24;
        out[2] = minutes%60;
        return out;
    }

    public static String subString(String input,int start,int end){
        return input.substring(start,end);
    }

    public static String booleanToStringNumber(boolean in){
        String out = "";
        if (in){
            out = "1";
        }else {
            out = "0";
        }
        return out;
    }
    public static boolean stringNumberToBoolean(String in){
        boolean out = false;
        if (in.equals("1")){
            out = true;
        }else {
            out = false;
        }
        return out;
    }

    public static int getSpinnerPosition(AdapterView<?> parent, int i){
        long pos = parent.getItemIdAtPosition(i);
        int position = Integer.valueOf(String.valueOf(pos));
        return position;
    }

    public static void openBrowser(Context context,String url){
        Intent i = new Intent(Intent.ACTION_VIEW,
                Uri.parse(url));
        context.startActivity(i);
    }
    public static void openDialer(Context context,String phone_number){
        Intent intent = new Intent(Intent.ACTION_DIAL);
        intent.setData(Uri.parse("tel:"+phone_number));
        context.startActivity(intent);
    }
    public static void openEmail(Context context,String email){
        Intent intent = new Intent(Intent.ACTION_SENDTO);
        intent.setData(Uri.parse("mailto:")); // only email apps should handle this
        String[] addresses = new String[]{email};
        intent.putExtra(Intent.EXTRA_EMAIL, addresses);
        intent.putExtra(Intent.EXTRA_SUBJECT, "Sawwah");
        if (intent.resolveActivity(context.getPackageManager()) != null) {
            context.startActivity(intent);
        }
    }

    public static void openFacebook(Context context,String url){
        context.startActivity(newFacebookIntent(context.getPackageManager(),url));
    }

    public static Intent newFacebookIntent(PackageManager pm, String url) {
        Uri uri = Uri.parse(url);
        try {
            ApplicationInfo applicationInfo = pm.getApplicationInfo("com.facebook.katana", 0);
            if (applicationInfo.enabled) {
                // http://stackoverflow.com/a/24547437/1048340
                uri = Uri.parse("fb://facewebmodal/f?href=" + url);
            }
        } catch (PackageManager.NameNotFoundException ignored) {
        }
        return new Intent(Intent.ACTION_VIEW, uri);
    }

    public static void openTwitter(Context context,String url){
        Intent intent = null;
        try {
            context.getPackageManager().getPackageInfo("com.twitter.android", 0);
            intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(intent);
        } catch (PackageManager.NameNotFoundException e) {
            intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
            context.startActivity(intent);
        }
    }
    public static void sendMessageBySMS(Context context,String message,String errorMessage){
        try {
            Intent sendIntent = new Intent(Intent.ACTION_VIEW);
            sendIntent.setData(Uri.parse("sms:"));
            sendIntent.putExtra("sms_body", message);
            context.startActivity(sendIntent);
        }catch (Exception e){
            Toast.makeText(context, errorMessage, Toast.LENGTH_SHORT).show();
            return;
        }
    }

    public static void sendMessageByMessenger(Context context,String message,String errorMessage){
        try{
            Intent sendIntent = new Intent();
            sendIntent.setAction(Intent.ACTION_SEND);
            sendIntent.putExtra(Intent.EXTRA_TEXT, message);
            sendIntent.setType("text/plain");
            sendIntent.setPackage("com.facebook.orca");
            context.startActivity(sendIntent);
        }catch (Exception e){
            Toast.makeText(context, errorMessage, Toast.LENGTH_SHORT).show();
            return;
        }
    }

    public static void sendMessageByWhatsapp(Context context,String message,String errorMessage){
        try{
            PackageManager pm = context.getPackageManager();
            Intent waIntent = new Intent(Intent.ACTION_SEND);
            waIntent.setType("text/plain");
            String text = message;
            PackageInfo info=pm.getPackageInfo("com.whatsapp", PackageManager.GET_META_DATA);
            waIntent.setPackage("com.whatsapp");
            waIntent.putExtra(Intent.EXTRA_TEXT, text);
            context.startActivity(Intent.createChooser(waIntent, "Share with"));

        }catch (Exception e){
            Toast.makeText(context, errorMessage, Toast.LENGTH_SHORT).show();
            return;
        }
    }

    public static void runAnimation(RecyclerView recyclerView, int type, RecyclerView.Adapter adapter){
        Context context = recyclerView.getContext();
        LayoutAnimationController controller = null;
        if (type == 0){ // Fall down animation
            //controller = AnimationUtils.loadLayoutAnimation(context,R.anim.layout_fall_down);
            recyclerView.setAdapter(adapter);
            recyclerView.setLayoutAnimation(controller);
            recyclerView.getAdapter().notifyDataSetChanged();
            recyclerView.scheduleLayoutAnimation();
        }
    }

    public static void runVerticalAnimation(RecyclerView recyclerView, int type, RecyclerView.Adapter adapter){
        Context context = recyclerView.getContext();
        LayoutAnimationController controller = null;
        if (type == 0){ // Fall down animation
            //controller = AnimationUtils.loadLayoutAnimation(context,R.anim.layout_fall_down_vertical);
            recyclerView.setAdapter(adapter);
            recyclerView.setLayoutAnimation(controller);
            recyclerView.getAdapter().notifyDataSetChanged();
            recyclerView.scheduleLayoutAnimation();
        }
    }

    public static void runSlideAnimation(Activity activity){
        activity.overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
    }
    public static void runBackSlideAnimation(Activity activity){
        activity.overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }


    public static void init_spinner(Context context , Spinner spinner , CustomSpinnerAdapter adapter, List<String> list){
        adapter = new CustomSpinnerAdapter(context,R.layout.item_spinner,list);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
    }



    public static void playMusic(Context context,int music){
        MediaPlayer mPlayer = MediaPlayer.create(context, music);
        mPlayer.start();
    }
    public void stopPlayMusic(MediaPlayer mediaPlayer){
        mediaPlayer.stop();
    }

    public static MultipartBody.Part uploadFileImageConverter(String imageFieldName, String imagePath){
        File file = new File(imagePath);
        final RequestBody requestBody = RequestBody.create(MediaType.parse("multipart/form-data"),file);
        MultipartBody.Part body = null;
        try {
            body = MultipartBody.Part.createFormData(imageFieldName, URLEncoder.encode(file.getName(),"utf-8"),requestBody);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return body;
    }

    public static RequestBody uploadFileStringConverter(String input){
        return RequestBody.create(MultipartBody.FORM,input);
    }

    /*
    public  static void uploadStory(final Context context, String type, String content, String filePath) {
        try {
            String uploadId =
                    new MultipartUploadRequest(context, "http://taikme.com/taikme_api/public/api/v1/users/stories")
                            .addHeader("Accept","application/json")
                            .addHeader("Authorization","Bearer "+SharedPrefManager.getInstance(context).getAccessToken())
                            .addParameter("content",content)
                            .addParameter("type",type)
                            .addFileToUpload(filePath, "file")
                            .setNotificationConfig(new UploadNotificationConfig())
                            .setMaxRetries(2)
                            .startUpload();
            Log.i("test_upload", "uploadMultipart: "+uploadId);
        } catch (Exception exc) {
            Log.i("test_upload","uploadMultipart: "+ exc.getMessage());
        }
    }

    public static void uploadPost(final Context context, String topic_id, String post_type,String caption, String filePath) {
        try {
            String uploadId =
                    new MultipartUploadRequest(context, "http://taikme.com/taikme_api/public/api/v1/posts")
                            .addHeader("Accept","application/json")
                            .addHeader("Authorization","Bearer "+SharedPrefManager.getInstance(context).getAccessToken())
                            .addParameter("topic_id",topic_id)
                            .addParameter("post_type",post_type)
                            .addParameter("caption",caption)
                            .addFileToUpload(filePath, "file")
                            .setNotificationConfig(new UploadNotificationConfig())
                            .setMaxRetries(2)
                            .startUpload();
            Log.i("test_upload", "uploadMultipart: "+uploadId);
        } catch (Exception exc) {
            Log.i("test_upload","uploadMultipart: "+ exc.getMessage());
        }
    }
    **/


    public static void showInfoToast(Context context,String message){
        MDToast mdToast = MDToast.makeText(context, message, MDToast.LENGTH_SHORT, MDToast.TYPE_INFO);
        mdToast.show();
    }

    public static void showSuccessToast(Context context,String message){
        MDToast mdToast = MDToast.makeText(context, message, MDToast.LENGTH_SHORT, MDToast.TYPE_SUCCESS);
        mdToast.show();
    }

    public static void showWarningToast(Context context,String message){
        MDToast mdToast = MDToast.makeText(context, message, MDToast.LENGTH_SHORT, MDToast.TYPE_WARNING);
        mdToast.show();
    }

    public static void showErrorToast(Context context,String message){
        MDToast mdToast = MDToast.makeText(context, message, MDToast.LENGTH_SHORT, MDToast.TYPE_ERROR);
        mdToast.show();
    }

    public static void underlineText(TextView textView, String text){
        SpannableString content = new SpannableString(text);
        content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
        textView.setText(content);
    }

    public static void callAdmin(Context context){
        //String phone = SharedPrefManager.getInstance(context).getCall();
        //openDialer(context,phone);
    }

    public static void setWebView(WebView webView, String html){
        final String mimeType = "text/html";
        final String encoding = "UTF-8";
        Spanned spn= Html.fromHtml(html);
        webView.loadDataWithBaseURL("", "<html dir=\"rtl\" lang=\"\"><body>" +spn.toString()+"</body></html>", mimeType, encoding, "");
    }

    public static void setWebViewWithoutBody(WebView webView,String html){
        final String mimeType = "text/html";
        final String encoding = "UTF-8";
        Spanned spn= Html.fromHtml(html);
        webView.loadDataWithBaseURL("", spn.toString(), mimeType, encoding, "");
    }

    public static void disableSSLCertificateChecking() {
        TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager() {
            public X509Certificate[] getAcceptedIssuers() {
                return null;
            }

            @Override
            public void checkClientTrusted(X509Certificate[] arg0, String arg1) throws CertificateException {
                // Not implemented
            }

            @Override
            public void checkServerTrusted(X509Certificate[] arg0, String arg1) throws CertificateException {
                // Not implemented
            }
        } };

        try {
            SSLContext sc = SSLContext.getInstance("TLS");

            sc.init(null, trustAllCerts, new java.security.SecureRandom());

            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
            HttpsURLConnection.setDefaultHostnameVerifier(new HostnameVerifier() { @Override public boolean verify(String hostname, SSLSession session) { return true; } });
        } catch (KeyManagementException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
    }
}
