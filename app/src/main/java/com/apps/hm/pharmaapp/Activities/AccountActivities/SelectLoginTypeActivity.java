package com.apps.hm.pharmaapp.Activities.AccountActivities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.RelativeLayout;

import com.apps.hm.pharmaapp.Activities.HomesActivities.PharmaHomeActivity;
import com.apps.hm.pharmaapp.Activities.HomesActivities.StockOwnerHomeActivity;
import com.apps.hm.pharmaapp.Bases.BaseActivity;
import com.apps.hm.pharmaapp.Bases.BaseFunctions;
import com.apps.hm.pharmaapp.Bases.SharedPrefManager;
import com.apps.hm.pharmaapp.R;

public class SelectLoginTypeActivity extends BaseActivity {

    private RelativeLayout login,register;
    @Override
    public void set_layout() {
        setContentView(R.layout.activity_select_login_type);
    }

    @Override
    public void init_activity(Bundle savedInstanceState) {

    }

    @Override
    public void init_views() {
        login = findViewById(R.id.login);
        register = findViewById(R.id.register);
    }

    @Override
    public void init_events() {
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SelectLoginTypeActivity.this,LoginActivity.class);
                intent.putExtra("account_type",getIntent().getIntExtra("account_type",2));
                startActivity(intent);
            }
        });
        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SelectLoginTypeActivity.this,RegisterActivity.class);
                intent.putExtra("account_type",getIntent().getIntExtra("account_type",2));
                startActivity(intent);
            }
        });
    }

    @Override
    public void set_fragment_place() {

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        BaseFunctions.runBackSlideAnimation(this);
    }
}
