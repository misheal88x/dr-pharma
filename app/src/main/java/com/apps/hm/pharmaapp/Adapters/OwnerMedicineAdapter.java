package com.apps.hm.pharmaapp.Adapters;

import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.apps.hm.pharmaapp.APIs.MedicinesAPIs;
import com.apps.hm.pharmaapp.APIsClasses.MedicineAPIsClass;
import com.apps.hm.pharmaapp.Bases.BaseFunctions;
import com.apps.hm.pharmaapp.Bases.BaseRetrofit;
import com.apps.hm.pharmaapp.Bases.SharedPrefManager;
import com.apps.hm.pharmaapp.Interfaces.IFailure;
import com.apps.hm.pharmaapp.Interfaces.IResponse;
import com.apps.hm.pharmaapp.Models.MedicineObject;
import com.apps.hm.pharmaapp.Models.OwnerMedicineObject;
import com.apps.hm.pharmaapp.R;
import com.facebook.drawee.view.SimpleDraweeView;
import com.wang.avi.AVLoadingIndicatorView;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;
import de.hdodenhof.circleimageview.CircleImageView;

public class OwnerMedicineAdapter extends RecyclerView.Adapter<OwnerMedicineAdapter.ViewHolder> {
    private Context context;
    private List<OwnerMedicineObject> list;

    public OwnerMedicineAdapter(Context context,List<OwnerMedicineObject> list) {
        this.context = context;
        this.list = list;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView name,company,plus,qty,minus,add_to_cart,remove_from_cart,select,remove;
        private SimpleDraweeView image;
        private AVLoadingIndicatorView loading;

        public ViewHolder(View view) {
            super(view);
            name = view.findViewById(R.id.name);
            company = view.findViewById(R.id.company);
            plus = view.findViewById(R.id.plus);
            minus = view.findViewById(R.id.minus);
            qty = view.findViewById(R.id.qty);
            add_to_cart = view.findViewById(R.id.add_to_cart);
            remove_from_cart = view.findViewById(R.id.remove_from_cart);
            select = view.findViewById(R.id.select);
            remove = view.findViewById(R.id.remove);
            image = view.findViewById(R.id.image);
            loading = view.findViewById(R.id.loading);
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_category_product, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        final OwnerMedicineObject o = list.get(position);

        holder.name.setText(o.getMedicine().getName());
        holder.company.setText(o.getMedicine().getCo_name()!=null?o.getMedicine().getCo_name():"");
        holder.qty.setVisibility(View.GONE);
        BaseFunctions.setFrescoImage(holder.image, BaseRetrofit.IMAGES_BASE+o.getMedicine().getImage());
        holder.add_to_cart.setVisibility(View.GONE);
        holder.remove_from_cart.setVisibility(View.GONE);
        holder.select.setVisibility(View.GONE);
        holder.remove.setVisibility(View.VISIBLE);
        holder.plus.setVisibility(View.GONE);
        holder.minus.setVisibility(View.GONE);
        holder.qty.setVisibility(View.GONE);

        holder.remove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setMessage("هل أنت متأكد من حذف هذا الدواء ؟");
                builder.setPositiveButton("نعم", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        callRemoveAPI(holder,position,String.valueOf(o.getId()));
                    }
                });
                builder.setNegativeButton("لا", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                builder.show();
            }
        });

    }

    private void callRemoveAPI(final ViewHolder holder, final int position, String id){
        holder.loading.smoothToShow();
        holder.remove.setVisibility(View.GONE);
        MedicineAPIsClass.delete_my_medicine(
                context,
                id,
                new IResponse() {
                    @Override
                    public void onResponse() {
                        holder.loading.smoothToHide();
                        holder.remove.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onResponse(Object json) {
                        holder.loading.smoothToHide();
                        holder.remove.setVisibility(View.VISIBLE);
                        BaseFunctions.showSuccessToast(context,"تم الحذف بنجاح");
                        list.remove(position);
                        notifyDataSetChanged();
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        holder.loading.smoothToHide();
                        holder.remove.setVisibility(View.VISIBLE);
                        BaseFunctions.showErrorToast(context,context.getResources().getString(R.string.no_internet));
                    }
                });
    }
}
