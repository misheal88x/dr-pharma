package com.apps.hm.pharmaapp.Activities.AccountActivities;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentUris;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.apps.hm.pharmaapp.APIsClasses.UserAPIsClass;
import com.apps.hm.pharmaapp.Activities.HomesActivities.PharmaHomeActivity;
import com.apps.hm.pharmaapp.Activities.HomesActivities.StockOwnerHomeActivity;
import com.apps.hm.pharmaapp.Activities.MapsActivities.PickLocationActivity;
import com.apps.hm.pharmaapp.Bases.BaseActivity;
import com.apps.hm.pharmaapp.Bases.BaseFunctions;
import com.apps.hm.pharmaapp.Bases.SharedPrefManager;
import com.apps.hm.pharmaapp.Interfaces.IFailure;
import com.apps.hm.pharmaapp.Interfaces.IResponse;
import com.apps.hm.pharmaapp.Models.UserObject;
import com.apps.hm.pharmaapp.R;
import com.asksira.bsimagepicker.BSImagePicker;
import com.bumptech.glide.Glide;
import com.github.dhaval2404.imagepicker.ImagePicker;
import com.google.gson.Gson;
import com.soundcloud.android.crop.Crop;
import com.theartofdev.edmodo.cropper.CropImage;
import com.wang.avi.AVLoadingIndicatorView;

import java.io.File;
import java.io.IOException;

public class RegisterActivity extends BaseActivity implements BSImagePicker.OnSingleImageSelectedListener,
        BSImagePicker.ImageLoaderDelegate{
    private TextView register_pick_image,pick_location;
    private RelativeLayout profile_image_layout;
    private CircleImageView register_image;
    private static final String TEMP_PHOTO_FILE = "temporary_holder.jpg";
    private AVLoadingIndicatorView loading;

    private EditText full_name,username,mobile,address,password;

    private TextView register;

    private static final int PERMISSION_REQUEST_CODE = 11,PERMISSION_REQUEST_CODE2 = 12;

    private String selected_lat = "";
    private String selected_lng = "";
    private String imagePath = "";

    private int role_id = 0;

    @Override
    public void set_layout() {
        setContentView(R.layout.activity_register);
    }

    @Override
    public void init_activity(Bundle savedInstanceState) {
        role_id = getIntent().getIntExtra("account_type",2);
    }

    @Override
    public void init_views() {
        pick_location = findViewById(R.id.pick_location);
        profile_image_layout = findViewById(R.id.profile_image_layout);
        register_image = findViewById(R.id.register_image);
        register_pick_image = findViewById(R.id.register_pick_image);

        full_name = findViewById(R.id.full_name);
        username = findViewById(R.id.username);
        mobile = findViewById(R.id.mobile);
        address = findViewById(R.id.full_name);
        password = findViewById(R.id.password);

        loading = findViewById(R.id.loading);

        register = findViewById(R.id.register);
    }

    @Override
    public void init_events() {
        pick_location.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (BaseFunctions.isOnline(RegisterActivity.this)){
                    ActivityCompat.requestPermissions(RegisterActivity.this,new String[]
                            {
                                    Manifest.permission.ACCESS_FINE_LOCATION,
                                    Manifest.permission.ACCESS_COARSE_LOCATION
                            },PERMISSION_REQUEST_CODE);
                }else {
                    BaseFunctions.showErrorToast(RegisterActivity.this,"يجب عليك تشغيل الانترنت");
                }
            }
        });
        profile_image_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ActivityCompat.requestPermissions(RegisterActivity.this,new String[]
                        {
                                Manifest.permission.READ_EXTERNAL_STORAGE,
                                Manifest.permission.WRITE_EXTERNAL_STORAGE
                        },PERMISSION_REQUEST_CODE2);
            }
        });
        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (imagePath.equals("")){
                    BaseFunctions.showWarningToast(RegisterActivity.this,"يجب عليك تحديد الصورة الشخصية");
                    return;
                }
                if (full_name.getText().toString().equals("")){
                    BaseFunctions.showWarningToast(RegisterActivity.this,"يجب عليك كتابة الاسم الكامل");
                    return;
                }
                if (username.getText().toString().equals("")){
                    BaseFunctions.showWarningToast(RegisterActivity.this,"يجب عليك كتابة اسم المستخدم");
                    return;
                }
                if (mobile.getText().toString().equals("")){
                    BaseFunctions.showWarningToast(RegisterActivity.this,"يجب عليك كتابة رقم الجوال");
                    return;
                }
                if (address.getText().toString().equals("")){
                    BaseFunctions.showWarningToast(RegisterActivity.this,"يجب عليك كتابة العنوان");
                    return;
                }
                if (selected_lat.equals("")&&selected_lng.equals("")){
                    BaseFunctions.showWarningToast(RegisterActivity.this,"يجب عليك اختيار موقعك على الخريطة");
                    return;
                }
                if (password.getText().toString().equals("")){
                    BaseFunctions.showWarningToast(RegisterActivity.this,"يجب عليك كتابة كلمة المرور");
                    return;
                }
                callRegiserAPI();
            }
        });
    }

    @Override
    public void set_fragment_place() {

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        BaseFunctions.runBackSlideAnimation(this);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch(requestCode){
            case PERMISSION_REQUEST_CODE:{
                if (grantResults.length>0 && grantResults[0]== PackageManager.PERMISSION_GRANTED
                        && grantResults[1]== PackageManager.PERMISSION_GRANTED){
                    final LocationManager manager = (LocationManager) getSystemService( Context.LOCATION_SERVICE );
                    if ( !manager.isProviderEnabled( LocationManager.GPS_PROVIDER ) ) {
                        buildAlertMessageNoGps();
                    }else{
                        Intent intent = new Intent(RegisterActivity.this,PickLocationActivity.class);
                        startActivityForResult(intent,100);
                    }
                }else {
                    BaseFunctions.showErrorToast(RegisterActivity.this,"يجب عليك منح السماحيات للوصول للموقع");
                }
                break;
            }
            case PERMISSION_REQUEST_CODE2:{
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED
                        && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                    if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q){
                        ImagePicker.Companion.with(RegisterActivity.this)
                                .galleryOnly()
                                .crop(1f,1f)	    			//Crop image(Optional), Check Customization for more option
                                .compress(1024)			//Final image size will be less than 1 MB(Optional)
                                .start();
                    }else {
                        BSImagePicker singleSelectionPicker = new BSImagePicker.Builder("com.scit.tabibak.provider")
                                .hideCameraTile() //Default: show. Set this if you don't want user to take photo.
                                .hideGalleryTile() //Default: show. Set this if you don't want to further let user select from a gallery app. In such case, I suggest you to set maximum displaying images to Integer.MAX_VALUE.
                                .build();
                        singleSelectionPicker.show(getSupportFragmentManager(),"Picker");
                    }
                }else {
                    BaseFunctions.showErrorToast(RegisterActivity.this,"يجب عليك منح السماحيات أولا");
                }
            }break;
        }
    }

    private void buildAlertMessageNoGps(){
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("يجب عليك تشغيل خدمات الموقع")
                .setCancelable(false)
                .setPositiveButton("تشغيل", new DialogInterface.OnClickListener() {
                    public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                    }
                })
                .setNegativeButton("تجاهل", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        dialog.cancel();
                        BaseFunctions.showErrorToast(RegisterActivity.this,"يجب عليك تشغيل الموقع");
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode,resultCode,data);
        switch (requestCode){
            case 100:{
                if (resultCode == RESULT_OK){
                    selected_lat  = data.getStringExtra("lat");
                    selected_lng = data.getStringExtra("lng");
                    pick_location.setBackgroundResource(R.drawable.rounded_primary);
                    pick_location.setTextColor(getResources().getColor(R.color.white));
                    pick_location.setText("تم تحديد الموقع بنجاح");
                }
            }break;
            case CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE:{
                CropImage.ActivityResult result = CropImage.getActivityResult(data);
                if (resultCode == RESULT_OK) {
                    Uri resultUri = result.getUri();
                    imagePath = getPath(RegisterActivity.this,resultUri);
                    Bitmap myBitmap = BitmapFactory.decodeFile(imagePath);
                    register_image.setImageBitmap(myBitmap);
                    register_pick_image.setVisibility(View.GONE);
                } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                    Exception error = result.getError();
                }
            }break;
            case Crop.REQUEST_CROP:{
                handle_crop(resultCode,data);
            }break;
            default:{
                if (resultCode == Activity.RESULT_OK) {
                    imagePath = ImagePicker.Companion.getFilePath(data);
                    Bitmap myBitmap = BitmapFactory.decodeFile(imagePath);
                    register_image.setImageBitmap(myBitmap);
                    register_pick_image.setVisibility(View.GONE);
                }
            }
        }
    }

    @Override
    public void loadImage(Uri imageUri, ImageView ivImage) {
        Glide.with(RegisterActivity.this).load(imageUri).into(ivImage);
    }

    @Override
    public void onSingleImageSelected(Uri uri, String tag) {
        Uri source_uri = uri;
        Uri destination_uri = Uri.fromFile(getTempFile());
        //Crop.of(source_uri,destination_uri).withAspect(1,1).start(RegisterActivity.this);
        CropImage.activity(source_uri).setAspectRatio(1,1)
                .start(this);
    }

    private File getTempFile() {
        if (isSDCARDMounted()) {

            File f = new File(Environment.getExternalStorageDirectory(),TEMP_PHOTO_FILE);
            try {
                f.createNewFile();
            } catch (IOException e) {

            }
            return f;
        } else {
            return null;
        }
    }

    private boolean isSDCARDMounted(){
        String status = Environment.getExternalStorageState();
        if (status.equals(Environment.MEDIA_MOUNTED))
            return true;
        return false;
    }
    private void handle_crop(int code, final Intent data){
        if (code == RESULT_OK){
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    String filePath=  Environment.getExternalStorageDirectory()
                            + "/"+"temporary_holder.jpg";
                    if (new File(filePath).exists()){
                        imagePath = filePath;
                        Bitmap myBitmap = BitmapFactory.decodeFile(imagePath);
                        register_image.setImageBitmap(myBitmap);
                        register_pick_image.setVisibility(View.GONE);
                    }

                }
            });
        }
    }

    private void callRegiserAPI(){
        loading.smoothToShow();
        register.setVisibility(View.GONE);
        UserAPIsClass.register(
                RegisterActivity.this,
                full_name.getText().toString(),
                mobile.getText().toString(),
                password.getText().toString(),
                String.valueOf(role_id),
                username.getText().toString(),
                address.getText().toString(),
                selected_lat + "," + selected_lng,
                imagePath,
                new IResponse() {
                    @Override
                    public void onResponse() {
                        loading.smoothToHide();
                        register.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onResponse(Object json) {
                        loading.smoothToHide();
                        register.setVisibility(View.VISIBLE);
                        String j = new Gson().toJson(json);
                        UserObject success = new Gson().fromJson(j,UserObject.class);
                        SharedPrefManager.getInstance(RegisterActivity.this).setUser(success);
                        startActivity(new Intent(RegisterActivity.this,LoginActivity.class));
                        finish();

                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        loading.smoothToHide();
                        register.setVisibility(View.VISIBLE);
                        BaseFunctions.showErrorToast(RegisterActivity.this,getResources().getString(R.string.no_internet));
                    }
                }
        );
    }

    public static String getPath(final Context context, final Uri uri) {

        final boolean isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;

        // DocumentProvider
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            if (isKitKat && DocumentsContract.isDocumentUri(context, uri)) {
                // ExternalStorageProvider
                if (isExternalStorageDocument(uri)) {
                    final String docId = DocumentsContract.getDocumentId(uri);
                    final String[] split = docId.split(":");
                    final String type = split[0];

                    if ("primary".equalsIgnoreCase(type)) {
                        return Environment.getExternalStorageDirectory() + "/" + split[1];
                    }
                }
                // DownloadsProvider
                else if (isDownloadsDocument(uri)) {

                    final String id = DocumentsContract.getDocumentId(uri);
                    final Uri contentUri = ContentUris.withAppendedId(
                            Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));

                    return getDataColumn(context, contentUri, null, null);
                }
                // MediaProvider
                else if (isMediaDocument(uri)) {
                    final String docId = DocumentsContract.getDocumentId(uri);
                    final String[] split = docId.split(":");
                    final String type = split[0];

                    Uri contentUri = null;
                    if ("image".equals(type)) {
                        contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                    } else if ("video".equals(type)) {
                        contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                    } else if ("audio".equals(type)) {
                        contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                    }

                    final String selection = "_id=?";
                    final String[] selectionArgs = new String[]{
                            split[1]
                    };

                    return getDataColumn(context, contentUri, selection, selectionArgs);
                }
            }
            // MediaStore (and general)
            else if ("content".equalsIgnoreCase(uri.getScheme())) {
                return getDataColumn(context, uri, null, null);
            }
            // File
            else if ("file".equalsIgnoreCase(uri.getScheme())) {
                return uri.getPath();
            }
        }

        return null;
    }

    public static String getDataColumn(Context context, Uri uri, String selection,
                                       String[] selectionArgs) {

        Cursor cursor = null;
        final String column = "_data";
        final String[] projection = {
                column
        };

        try {
            cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs,
                    null);
            if (cursor != null && cursor.moveToFirst()) {
                final int column_index = cursor.getColumnIndexOrThrow(column);
                return cursor.getString(column_index);
            }
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return null;
    }
    public static boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }


    public static boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    public static boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }

    public static MultipartBody.Part getMultiPartBody(String key, String mMediaUrl) {
        if (mMediaUrl != null) {
            File file = new File(mMediaUrl);
            RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file);
            return MultipartBody.Part.createFormData(key, file.getName(), requestFile);
        } else {
            return MultipartBody.Part.createFormData(key, "");
        }
    }
}
