package com.apps.hm.pharmaapp.Fragments.PharmaFragments;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.apps.hm.pharmaapp.APIs.CategoriesAPIs;
import com.apps.hm.pharmaapp.APIs.SearchAPIs;
import com.apps.hm.pharmaapp.APIsClasses.CategoriesAPIsClass;
import com.apps.hm.pharmaapp.APIsClasses.SearchAPIsClass;
import com.apps.hm.pharmaapp.APIsClasses.UserAPIsClass;
import com.apps.hm.pharmaapp.Adapters.OwnerOffersAdapter;
import com.apps.hm.pharmaapp.Adapters.StocksAdapter;
import com.apps.hm.pharmaapp.Bases.App;
import com.apps.hm.pharmaapp.Bases.BaseFragment;
import com.apps.hm.pharmaapp.Bases.BaseFunctions;
import com.apps.hm.pharmaapp.Bases.SharedPrefManager;
import com.apps.hm.pharmaapp.Interfaces.IFailure;
import com.apps.hm.pharmaapp.Interfaces.IResponse;
import com.apps.hm.pharmaapp.Models.UserObject;
import com.apps.hm.pharmaapp.R;
import com.apps.hm.pharmaapp.Services.MyFirebaseMessageingService;
import com.facebook.shimmer.Shimmer;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class ShopFragment extends BaseFragment {

    private RecyclerView recycler;
    private List<UserObject> list;
    private StocksAdapter adapter;
    private LinearLayoutManager layoutManager;
    private EditText search;
    private LinearLayout no_data,no_internet,error_layout;
    private ShimmerFrameLayout shimmer;
    private int api_type = 0;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_shop,container,false);
    }

    @Override
    public void init_views() {
        recycler = base.findViewById(R.id.recycler);
        search = base.findViewById(R.id.search);
        no_data = base.findViewById(R.id.no_data_layout);
        no_internet = base.findViewById(R.id.no_internet_layout);
        error_layout = base.findViewById(R.id.error_layout);
        shimmer = base.findViewById(R.id.shimmer);
    }

    @Override
    public void init_events() {
        search.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    if (search.getText().toString().equals("")){
                        BaseFunctions.showErrorToast(getContext(),"يجب عليك كتابة اسم المستودع");
                        return false;
                    }else {
                        BaseFunctions.showWarningToast(getContext(),"جاري البحث..");
                        list.clear();
                        adapter = new StocksAdapter(base,list);
                        recycler.setAdapter(adapter);
                        callSearchAPI(search.getText().toString());
                    }
                    return true;
                }
                return false;
            }
        });

        search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() == 0){
                    list = new ArrayList<>();
                    adapter = new StocksAdapter(base,list);
                    recycler.setAdapter(adapter);
                    callGetStocksAPI();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        error_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (api_type == 1) {
                    callGetStocksAPI();
                }else if (api_type == 2){
                    callSearchAPI(search.getText().toString());
                }
            }
        });

        no_internet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (api_type == 1) {
                    callGetStocksAPI();
                }else if (api_type == 2){
                    callSearchAPI(search.getText().toString());
                }
            }
        });
    }

    @Override
    public void init_fragment(Bundle savedInstanceState) {
        init_recycler();
        callGetStocksAPI();
    }

    private void init_recycler(){
        list = new ArrayList<>();
        adapter = new StocksAdapter(base,list);
        layoutManager = new LinearLayoutManager(base);
        recycler.setLayoutManager(layoutManager);
        recycler.setAdapter(adapter);
    }

    private void callGetStocksAPI(){
        api_type = 1;
        no_internet.setVisibility(View.GONE);
        no_data.setVisibility(View.GONE);
        error_layout.setVisibility(View.GONE);
        search.setVisibility(View.GONE);
        shimmer.startShimmer();
        shimmer.setVisibility(View.VISIBLE);
        CategoriesAPIsClass.get_stocks(
                base,
                new IResponse() {
                    @Override
                    public void onResponse() {
                        error_layout.setVisibility(View.VISIBLE);
                        shimmer.stopShimmer();
                        shimmer.setVisibility(View.GONE);
                    }

                    @Override
                    public void onResponse(Object json) {
                        shimmer.stopShimmer();
                        shimmer.setVisibility(View.GONE);
                        String j = new Gson().toJson(json);
                        try {
                            UserObject[] success = new Gson().fromJson(j,UserObject[].class);
                            if (success.length>0){
                                search.setVisibility(View.VISIBLE);
                                for (UserObject o : success){
                                    boolean found = false;
                                    for (UserObject oo : list){
                                        if (oo.getId() == o.getId()){
                                            found = true;
                                            break;
                                        }
                                    }
                                    if (!found){
                                        list.add(o);
                                        adapter.notifyDataSetChanged();
                                    }
                                }
                            }else {
                                no_data.setVisibility(View.VISIBLE);
                            }
                            FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(base, new OnSuccessListener<InstanceIdResult>() {
                                @Override
                                public void onSuccess(InstanceIdResult instanceIdResult) {
                                    String token = instanceIdResult.getToken();
                                    if (App.is_token_updated == false) {
                                        Log.i("dfdfrf", "onResponse: "+token);
                                        if (SharedPrefManager.getInstance(base).getUser().getId() != 0) {
                                            App.is_token_updated = true;
                                            callUpdateTokenAPI(token);
                                        }
                                    }
                                }
                            });
                        }catch (Exception e){
                            callGetStocksAPI();
                        }
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        shimmer.stopShimmer();
                        shimmer.setVisibility(View.GONE);
                        no_internet.setVisibility(View.VISIBLE);
                    }
                }
        );
    }

    private void callSearchAPI(final String name){
        api_type = 2;
        no_internet.setVisibility(View.GONE);
        no_data.setVisibility(View.GONE);
        error_layout.setVisibility(View.GONE);
        search.setVisibility(View.GONE);
        shimmer.startShimmer();
        shimmer.setVisibility(View.VISIBLE);
        SearchAPIsClass.search_stocks(
                base,
                name,
                new IResponse() {
                    @Override
                    public void onResponse() {
                        error_layout.setVisibility(View.VISIBLE);
                        shimmer.stopShimmer();
                        shimmer.setVisibility(View.GONE);
                    }

                    @Override
                    public void onResponse(Object json) {
                        shimmer.stopShimmer();
                        shimmer.setVisibility(View.GONE);
                        String j = new Gson().toJson(json);
                        try {
                            UserObject[] success = new Gson().fromJson(j, UserObject[].class);
                            if (success.length > 0) {
                                search.setVisibility(View.VISIBLE);
                                for (UserObject o : success) {
                                    boolean found = false;
                                    for (UserObject oo : success){
                                        if (oo.getId() == o. getId()){
                                            found = true;
                                            break;
                                        }
                                    }
                                    if (!found){
                                        list.add(o);
                                        adapter.notifyDataSetChanged();
                                    }
                                }
                            } else {
                                no_data.setVisibility(View.VISIBLE);
                            }
                        }catch (Exception e){
                            callSearchAPI(name);
                        }
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        shimmer.stopShimmer();
                        shimmer.setVisibility(View.GONE);
                        no_internet.setVisibility(View.VISIBLE);
                    }
                }
        );
    }

    private void callUpdateTokenAPI(String token){
        App.is_token_updated = true;
        UserAPIsClass.update_fcm(base,
                token,
                new IResponse() {
                    @Override
                    public void onResponse() {
                        Log.i("firebase_token_update", "onResponse: "+"Failed");
                    }

                    @Override
                    public void onResponse(Object json) {
                        Log.i("firebase_token_update", "onResponse: "+"Success");
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        Log.i("firebase_token_update", "onResponse: "+"No Internet");
                    }
                });
    }
}
