package com.apps.hm.pharmaapp.Interfaces;

public interface IResponse {
    void onResponse();
    void onResponse(Object json);
}
