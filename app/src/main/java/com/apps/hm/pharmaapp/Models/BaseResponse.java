package com.apps.hm.pharmaapp.Models;

import com.google.gson.annotations.SerializedName;

public class BaseResponse {
    @SerializedName("status") private int status = 0;
    @SerializedName("message_id") private int message_id = 0;
    @SerializedName("message") private String message = "";
    @SerializedName("data") private Object data = null;

    public int getStatus() {
        return status;
    }

    public void setStatus(int id) {
        this.status = id;
    }

    public int getMessage_id() {
        return message_id;
    }

    public void setMessage_id(int message_id) {
        this.message_id = message_id;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }
}
