package com.apps.hm.pharmaapp.Services;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.util.Log;

import com.apps.hm.pharmaapp.APIsClasses.UserAPIsClass;
import com.apps.hm.pharmaapp.Activities.HomesActivities.PharmaHomeActivity;
import com.apps.hm.pharmaapp.Activities.HomesActivities.StockOwnerHomeActivity;
import com.apps.hm.pharmaapp.Activities.SplashActivity;
import com.apps.hm.pharmaapp.Bases.App;
import com.apps.hm.pharmaapp.Bases.BaseFunctions;
import com.apps.hm.pharmaapp.Bases.SharedPrefManager;
import com.apps.hm.pharmaapp.Interfaces.IFailure;
import com.apps.hm.pharmaapp.Interfaces.IResponse;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Map;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;

public class MyFirebaseMessageingService extends FirebaseMessagingService {
    final int MY_NOTIFICATION_ID = 1;
    final String NOTIFICATION_CHANNEL_ID = "10001";
    NotificationManager notificationManager;
    NotificationCompat.Builder builder;
    //final Context context = this;


    @Override
    public void onNewToken(@NonNull String s) {
        super.onNewToken(s);
        Log.e("newToken1",s);
        if (SharedPrefManager.getInstance(MyFirebaseMessageingService.this).getUser().getId()!=0){
            App.is_token_updated = true;
            callUpdateTokenAPI(s);
        }
    }



    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        try {
            Map<String, String> params = remoteMessage.getData();
            JSONObject jsonObject = new JSONObject(params);
            Log.i("JSON_OBJECTT", "the : " + jsonObject.toString());
            sendNotification(jsonObject);
        }catch (Exception e){
            Log.i("JSON_OBJECTT", "onMessageReceived: "+e.getMessage());
        }
    }
    private void sendNotification(JSONObject jsonObject) {

        String content = "";
        String target_type = "";
        String target_id = "";
        String target_title = "";
        String target_user = "";

        try {
            if (jsonObject.has("msg_title"))
                content = jsonObject.getString("msg_title");
            if (jsonObject.has("target_type"))
                target_type = jsonObject.getString("target_type");
            if (jsonObject.has("target_id"))
                target_id = jsonObject.getString("target_id");
            if (jsonObject.has("target_title"))
                target_title = jsonObject.getString("target_title");
            if (jsonObject.has("target_user"))
                target_user = jsonObject.getString("target_user");

        } catch (JSONException e) {
            e.printStackTrace();
        }
        if (SharedPrefManager.getInstance(this).getUser().getId()!=0) {
            if (target_user.equals(String.valueOf(SharedPrefManager.getInstance(this).getUser().getId()))) {
                switch (target_type) {
                    //Order accepted by admin
                    case "1": {
                        Intent intent = new Intent(getApplicationContext(), SplashActivity.class);
                        PendingIntent pendingIntent = PendingIntent.getActivity(
                                getApplicationContext(),
                                1,
                                intent,
                                PendingIntent.FLAG_UPDATE_CURRENT
                                        | PendingIntent.FLAG_ONE_SHOT);
                        BaseFunctions.showNotification(getApplicationContext(), pendingIntent, content, builder, notificationManager, NOTIFICATION_CHANNEL_ID, MY_NOTIFICATION_ID, target_type);

                    }
                    break;
                    //New Offer
                    case "2":{
                        Intent intent = new Intent(getApplicationContext(), SplashActivity.class);
                        PendingIntent pendingIntent = PendingIntent.getActivity(
                                getApplicationContext(),
                                1,
                                intent,
                                PendingIntent.FLAG_UPDATE_CURRENT
                                        | PendingIntent.FLAG_ONE_SHOT);
                        BaseFunctions.showNotification(getApplicationContext(), pendingIntent, content, builder, notificationManager, NOTIFICATION_CHANNEL_ID, MY_NOTIFICATION_ID, target_type);
                    }break;
                    //New News
                    case "3":{
                        if (SharedPrefManager.getInstance(this).getUser().getRole_id()==2) {
                            SharedPrefManager.getInstance(this).setNewsDot(true);
                            Intent intent = new Intent(getApplicationContext(), SplashActivity.class);
                            PendingIntent pendingIntent = PendingIntent.getActivity(
                                    getApplicationContext(),
                                    1,
                                    intent,
                                    PendingIntent.FLAG_UPDATE_CURRENT
                                            | PendingIntent.FLAG_ONE_SHOT);
                            BaseFunctions.showNotification(getApplicationContext(), pendingIntent, content, builder, notificationManager, NOTIFICATION_CHANNEL_ID, MY_NOTIFICATION_ID, target_type);
                        }
                        }break;
                    //Offer accepted
                    case "4":{
                        Intent intent = new Intent(getApplicationContext(), SplashActivity.class);
                        PendingIntent pendingIntent = PendingIntent.getActivity(
                                getApplicationContext(),
                                1,
                                intent,
                                PendingIntent.FLAG_UPDATE_CURRENT
                                        | PendingIntent.FLAG_ONE_SHOT);
                        BaseFunctions.showNotification(getApplicationContext(), pendingIntent, content, builder, notificationManager, NOTIFICATION_CHANNEL_ID, MY_NOTIFICATION_ID, target_type);
                    }break;
                    //General
                    case "5":{
                        Intent intent = new Intent(getApplicationContext(), SplashActivity.class);
                        PendingIntent pendingIntent = PendingIntent.getActivity(
                                getApplicationContext(),
                                1,
                                intent,
                                PendingIntent.FLAG_UPDATE_CURRENT
                                        | PendingIntent.FLAG_ONE_SHOT);
                        BaseFunctions.showNotification(getApplicationContext(), pendingIntent, content, builder, notificationManager, NOTIFICATION_CHANNEL_ID, MY_NOTIFICATION_ID, target_type);
                    }break;
                }
            }
        }
    }

    private void callUpdateTokenAPI(String token){
        App.is_token_updated = true;
        UserAPIsClass.update_fcm(MyFirebaseMessageingService.this,
                token,
                new IResponse() {
                    @Override
                    public void onResponse() {
                        Log.i("firebase_token_update", "onResponse: "+"Failed");
                    }

                    @Override
                    public void onResponse(Object json) {
                        Log.i("firebase_token_update", "onResponse: "+"Success");
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        Log.i("firebase_token_update", "onResponse: "+"No Internet");
                    }
                });
    }
}
