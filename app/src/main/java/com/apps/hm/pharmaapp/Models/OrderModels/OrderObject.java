package com.apps.hm.pharmaapp.Models.OrderModels;

import com.apps.hm.pharmaapp.Models.UserObject;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class OrderObject {
    @SerializedName("id") private int id = 0;
    @SerializedName("user_id") private int user_id = 0;
    @SerializedName("sender_id") private int sender_id = 0;
    @SerializedName("order_count") private int order_count = 0;
    @SerializedName("total_price") private float total_price = 0f;
    @SerializedName("status") private int status = 0;
    @SerializedName("confirmed") private int confirmed = 0;
    @SerializedName("created_at") private String created_at = "";
    @SerializedName("updated_at") private String updated_at = "";
    @SerializedName("sender") private UserObject sender = new UserObject();
    @SerializedName("owner") private UserObject owner = new UserObject();
    @SerializedName("products") private List<OrderProductObject> products = new ArrayList<>();

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public int getSender_id() {
        return sender_id;
    }

    public void setSender_id(int sender_id) {
        this.sender_id = sender_id;
    }

    public int getOrder_count() {
        return order_count;
    }

    public void setOrder_count(int order_count) {
        this.order_count = order_count;
    }

    public float getTotal_price() {
        return total_price;
    }

    public void setTotal_price(float total_price) {
        this.total_price = total_price;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getConfirmed() {
        return confirmed;
    }

    public void setConfirmed(int confirmed) {
        this.confirmed = confirmed;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public UserObject getSender() {
        return sender;
    }

    public void setSender(UserObject sender) {
        this.sender = sender;
    }

    public UserObject getOwner() {
        return owner;
    }

    public void setOwner(UserObject owner) {
        this.owner = owner;
    }

    public List<OrderProductObject> getProducts() {
        return products;
    }

    public void setProducts(List<OrderProductObject> products) {
        this.products = products;
    }
}
