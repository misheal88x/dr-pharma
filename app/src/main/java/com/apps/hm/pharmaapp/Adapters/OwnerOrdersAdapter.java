package com.apps.hm.pharmaapp.Adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.apps.hm.pharmaapp.Activities.OrdersActivities.OrderDetailsActivity;
import com.apps.hm.pharmaapp.Bases.BaseFunctions;
import com.apps.hm.pharmaapp.Interfaces.IMove;
import com.apps.hm.pharmaapp.Models.OrderModels.OrderObject;
import com.apps.hm.pharmaapp.R;
import com.google.gson.Gson;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import de.hdodenhof.circleimageview.CircleImageView;

public class OwnerOrdersAdapter extends RecyclerView.Adapter<OwnerOrdersAdapter.ViewHolder>{
    private Context context;
    private List<OrderObject> list;
    private IMove iMove;

    public OwnerOrdersAdapter(Context context,List<OrderObject> list,IMove iMove) {
        this.context = context;
        this.list = list;
        this.iMove = iMove;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private CircleImageView image;
        private TextView name,price,date;
        private RelativeLayout layout;

        public ViewHolder(View view) {
            super(view);
            image = view.findViewById(R.id.image);
            name = view.findViewById(R.id.name);
            price = view.findViewById(R.id.price);
            date = view.findViewById(R.id.date);
            layout = view.findViewById(R.id.layout);
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_owner_order, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {

        final OrderObject o = list.get(position);

        if (o.getSender()!=null){
            holder.name.setText(o.getSender().getName()!=null?o.getSender().getName():"");
            if (o.getSender().getAvatar()!=null){
                BaseFunctions.setGlideImage(context,holder.image,o.getSender().getAvatar());
            }
        }

        holder.date.setText(BaseFunctions.processDate(context,o.getCreated_at()));
        holder.price.setText(o.getTotal_price()+" ل.س");
        holder.layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                iMove.move(position);
            }
        });
    }
}
