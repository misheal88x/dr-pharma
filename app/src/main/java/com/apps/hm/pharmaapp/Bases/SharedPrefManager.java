package com.apps.hm.pharmaapp.Bases;

import android.content.Context;
import android.content.SharedPreferences;

import com.apps.hm.pharmaapp.Models.MedicineObject;
import com.apps.hm.pharmaapp.Models.UserObject;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

public class SharedPrefManager {
    private static SharedPrefManager mInstance;
    private static Context mCtx;

    private static final String SHARED_PREF_NAME = "Main";
    private static final String VIEW_LANGUAGE = "view_language";
    private static final String USER = "user";
    private static final String CART = "cart";
    private static final String REPO_ID = "repo_id";
    private static final String NEWS_DOT = "news_dot";

    private SharedPrefManager(Context context) {
        mCtx = context;
    }
    public static synchronized SharedPrefManager getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new SharedPrefManager(context);
        }
        return mInstance;
    }

    public void setViewLanguage(String lan){
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(VIEW_LANGUAGE,lan);
        editor.commit();
    }

    public String getViewLanguage() {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        String value = sharedPreferences.getString(VIEW_LANGUAGE, "ar");
        return value;
    }

    public void setUser(UserObject o){
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(USER,new Gson().toJson(o));
        editor.commit();
    }

    public UserObject getUser() {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        String value = sharedPreferences.getString(USER, "{}");
        UserObject o = null;
        if (value.equals("{}")){
            o = new UserObject();
        }else {
            o = new Gson().fromJson(value,UserObject.class);
        }

        return o;
    }

    public void setCart(List<MedicineObject> o){
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(CART,new Gson().toJson(o));
        editor.commit();
    }

    public List<MedicineObject> getCart() {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        String value = sharedPreferences.getString(CART, "[]");
        List<MedicineObject> list = new ArrayList<>();
        if (!value.equals("[]")) {
            MedicineObject[] arr = new Gson().fromJson(value,MedicineObject[].class);
            if (arr.length>0){
                for (MedicineObject m : arr){
                    list.add(m);
                }
            }
        }
        return list;
    }

    public void addToCart(MedicineObject mo){
        List<MedicineObject> list = getCart();
        list.add(mo);
        setCart(list);
    }

    public boolean removeFromCart(int id){
        boolean result = false;
        List<MedicineObject> list = getCart();
        if (list.size()>0){
            for (int i = 0; i < list.size(); i++) {
                if (list.get(i).getId() == id){
                    list.remove(i);
                    result = true;
                    break;
                }
            }
        }
        setCart(list);
        return result;
    }

    public void setRepoId(int repo_id){
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt(REPO_ID,repo_id);
        editor.commit();
    }

    public int getRepoId() {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        int value = sharedPreferences.getInt(REPO_ID, 0);
        return value;
    }

    public void setNewsDot(boolean b){
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(NEWS_DOT,b);
        editor.commit();
    }

    public boolean getNewsDot() {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        boolean value = sharedPreferences.getBoolean(NEWS_DOT, false);
        return value;
    }
}
