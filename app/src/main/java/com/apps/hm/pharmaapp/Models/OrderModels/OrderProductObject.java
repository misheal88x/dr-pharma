package com.apps.hm.pharmaapp.Models.OrderModels;

import com.google.gson.annotations.SerializedName;

public class OrderProductObject {
    @SerializedName("id") private int id = 0;
    @SerializedName("order_id") private int order_id = 0;
    @SerializedName("repository_medicine_id") private int repository_medicine_id = 0;
    @SerializedName("count") private int count = 0;
    @SerializedName("medicine") private OrderMedicineObject medicine = new OrderMedicineObject();

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getOrder_id() {
        return order_id;
    }

    public void setOrder_id(int order_id) {
        this.order_id = order_id;
    }

    public int getRepository_medicine_id() {
        return repository_medicine_id;
    }

    public void setRepository_medicine_id(int repository_medicine_id) {
        this.repository_medicine_id = repository_medicine_id;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public OrderMedicineObject getMedicine() {
        return medicine;
    }

    public void setMedicine(OrderMedicineObject medicine) {
        this.medicine = medicine;
    }
}
