package com.apps.hm.pharmaapp.Adapters;

import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.apps.hm.pharmaapp.APIs.MedicinesAPIs;
import com.apps.hm.pharmaapp.APIsClasses.MedicineAPIsClass;
import com.apps.hm.pharmaapp.Bases.BaseActivity;
import com.apps.hm.pharmaapp.Bases.BaseFunctions;
import com.apps.hm.pharmaapp.Bases.SharedPrefManager;
import com.apps.hm.pharmaapp.Interfaces.IFailure;
import com.apps.hm.pharmaapp.Interfaces.IMove;
import com.apps.hm.pharmaapp.Interfaces.IResponse;
import com.apps.hm.pharmaapp.Models.MedicineObject;
import com.apps.hm.pharmaapp.R;
import com.wang.avi.AVLoadingIndicatorView;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;

public class ExchangeMedicinesAdapter extends RecyclerView.Adapter<ExchangeMedicinesAdapter.ViewHolder>{
    private Context context;
    private List<MedicineObject> list;
    private IMove iMove;
    public ExchangeMedicinesAdapter(Context context,List<MedicineObject> list,IMove iMove) {
        this.context = context;
        this.list = list;
        this.iMove = iMove;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView id,name,qty,date,price,final_price,delete,pharmacian_name,pharmacian_phone;
        private AVLoadingIndicatorView loading;

        public ViewHolder(View view) {
            super(view);
            id = view.findViewById(R.id.id);
            name = view.findViewById(R.id.name);
            price = view.findViewById(R.id.price);
            date = view.findViewById(R.id.date);
            qty = view.findViewById(R.id.qty);
            final_price = view.findViewById(R.id.more);
            delete = view.findViewById(R.id.delete);
            pharmacian_name = view.findViewById(R.id.pharmacian_name);
            pharmacian_phone = view.findViewById(R.id.pharmacian_phone);
            loading = view.findViewById(R.id.loading);
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_exchange_medicine, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {

       final MedicineObject o = list.get(position);

        holder.name.setText(o.getName());
        holder.date.setText("تاريخ الانتهاء : "+o.getExpire_date());
        holder.id.setText("رقم الدواء : "+o.getId());
        holder.qty.setText(o.getCount()+" قطعة");
        holder.price.setText(o.getPrice()+" ل.س");
        holder.final_price.setText(o.getTotal()+" ل.س");
        if (o.getUser_id() == SharedPrefManager.getInstance(context).getUser().getId()){
            holder.delete.setVisibility(View.VISIBLE);
            holder.delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(context);
                    builder.setMessage("هل أنت متأكد من حذف هذا الدواء ؟");
                    builder.setPositiveButton("نعم", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            callDeleteAPI(holder,position,o.getId());
                        }
                    });
                    builder.setNegativeButton("لا", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    }).show();
                }
            });
        }else {
            holder.delete.setVisibility(View.GONE);
        }
        if (o.getOwner()!=null){
            holder.pharmacian_name.setText(o.getOwner().getName()!=null?o.getOwner().getName():"");
            holder.pharmacian_phone.setText(o.getOwner().getPhone()!=null?o.getOwner().getPhone():"");
        }
    }

    private void callDeleteAPI(final ViewHolder holder, final int postiion, int id){
        holder.delete.setVisibility(View.GONE);
        holder.loading.smoothToShow();
        MedicineAPIsClass.delete_exchange_medicine(
                context,
                String.valueOf(id),
                new IResponse() {
                    @Override
                    public void onResponse() {

                    }

                    @Override
                    public void onResponse(Object json) {
                        BaseFunctions.showSuccessToast(context,"تم حذف الدواء بنجاح");
                        list.remove(postiion);
                        notifyDataSetChanged();
                        if (list.size()==0){
                            iMove.move(postiion);
                        }
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        holder.delete.setVisibility(View.GONE);
                        holder.loading.smoothToShow();
                        BaseFunctions.showErrorToast(context,context.getResources().getString(R.string.no_internet));
                    }
                });
    }
}
