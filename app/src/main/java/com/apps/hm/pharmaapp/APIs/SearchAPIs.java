package com.apps.hm.pharmaapp.APIs;

import com.apps.hm.pharmaapp.Models.BaseResponse;
import com.google.gson.annotations.SerializedName;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface SearchAPIs {
    @GET("search-repo/{name}")
    Call<BaseResponse> search_stocks(
            @Header("Accept") String accept,
            @Path("name") String path);
    @GET("search-repo-med/{stock_id}/{name}")
    Call<BaseResponse> search_stock_products(
            @Header("Accept") String accept,
            @Path("stock_id") String stock_id,
            @Path("name") String path,
            @Query("page") int page
    );
}
