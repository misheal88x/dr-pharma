package com.apps.hm.pharmaapp.Fragments.OwnerFragments;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.apps.hm.pharmaapp.APIsClasses.MedicineAPIsClass;
import com.apps.hm.pharmaapp.Activities.ProductsActivities.SelelctMedicineActivity;
import com.apps.hm.pharmaapp.Bases.BaseFragment;
import com.apps.hm.pharmaapp.Bases.BaseFunctions;
import com.apps.hm.pharmaapp.Interfaces.IFailure;
import com.apps.hm.pharmaapp.Interfaces.IResponse;
import com.apps.hm.pharmaapp.R;
import com.wang.avi.AVLoadingIndicatorView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import static android.app.Activity.RESULT_OK;

public class MedicinesAddFragment extends BaseFragment {

    private TextView select,add;
    private int selected_id = 0;
    private AVLoadingIndicatorView loading;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_medicines_add,container,false);
    }

    @Override
    public void init_views() {
        select = base.findViewById(R.id.select);
        add = base.findViewById(R.id.add);
        loading = base.findViewById(R.id.loading);
    }

    @Override
    public void init_events() {
        select.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(base,SelelctMedicineActivity.class);
                startActivityForResult(intent,100);
            }
        });

        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (selected_id == 0){
                    BaseFunctions.showWarningToast(base,"يجب عليك اختيار الدواء أولا");
                    return;
                }
                callAddMedicineAPI();
            }
        });
    }

    @Override
    public void init_fragment(Bundle savedInstanceState) {

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == 100) {
            if (resultCode == RESULT_OK) {
                int id = data.getIntExtra("id", 0);
                select.setBackgroundResource(R.drawable.rounded_primary);
                select.setTextColor(getResources().getColor(R.color.white));
                select.setText("تم اختيار الدواء بنجاح");
                selected_id = id;
            }
        }
    }

    private void callAddMedicineAPI(){
        add.setVisibility(View.GONE);
        loading.smoothToShow();
        MedicineAPIsClass.add_my_medicine(
                base,
                selected_id,
                1,
                new IResponse() {
                    @Override
                    public void onResponse() {
                        add.setVisibility(View.VISIBLE);
                        loading.smoothToHide();
                    }

                    @Override
                    public void onResponse(Object json) {
                        add.setVisibility(View.VISIBLE);
                        loading.smoothToHide();
                        BaseFunctions.showSuccessToast(base,"تم الإضافة بنجاح");
                        select.setBackgroundResource(R.drawable.rounded_primary_border);
                        select.setTextColor(getResources().getColor(R.color.colorPrimary));
                        select.setText("اختر الدواء من القائمة");
                        selected_id = 0;
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        add.setVisibility(View.VISIBLE);
                        loading.smoothToHide();
                        BaseFunctions.showErrorToast(base,getResources().getString(R.string.no_internet));
                    }
                });
    }
}
