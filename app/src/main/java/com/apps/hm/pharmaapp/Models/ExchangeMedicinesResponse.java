package com.apps.hm.pharmaapp.Models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class ExchangeMedicinesResponse {
    @SerializedName("owner") private List<MedicineObject> owner = new ArrayList<>();
    @SerializedName("other") private MedicinesResponse other = new MedicinesResponse();

    public List<MedicineObject> getOwner() {
        return owner;
    }

    public void setOwner(List<MedicineObject> owner) {
        this.owner = owner;
    }

    public MedicinesResponse getOther() {
        return other;
    }

    public void setOther(MedicinesResponse other) {
        this.other = other;
    }
}
