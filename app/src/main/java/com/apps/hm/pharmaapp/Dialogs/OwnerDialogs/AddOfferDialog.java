package com.apps.hm.pharmaapp.Dialogs.OwnerDialogs;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.TextView;

import com.apps.hm.pharmaapp.APIsClasses.OffersAPIsClass;
import com.apps.hm.pharmaapp.Bases.BaseFunctions;
import com.apps.hm.pharmaapp.Interfaces.IFailure;
import com.apps.hm.pharmaapp.Interfaces.IMove;
import com.apps.hm.pharmaapp.Interfaces.IResponse;
import com.apps.hm.pharmaapp.R;
import com.wang.avi.AVLoadingIndicatorView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.LinearLayoutCompat;

public class AddOfferDialog extends AlertDialog {

    private Context context;
    private EditText text;
    private TextView add;
    private AVLoadingIndicatorView loading;

    private IMove iMove;
    public AddOfferDialog(@NonNull Context context,IMove iMove) {
        super(context);
        this.context = context;
        this.iMove = iMove;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_add_offer);

        getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        if (Build.VERSION.SDK_INT < 16) {
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN);
        } else {
            View decorView = getWindow().getDecorView();
            int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
            decorView.setSystemUiVisibility(uiOptions);
        }
        getWindow().setLayout(LinearLayoutCompat.LayoutParams.MATCH_PARENT,
                LinearLayoutCompat.LayoutParams.MATCH_PARENT);
        getWindow().getAttributes().windowAnimations = R.style.FadeAnimation;
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE|WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM);

        init_views();
        init_events();
        init_dialog();
    }

    private void init_views(){
        text = findViewById(R.id.text);
        add = findViewById(R.id.add);
        loading = findViewById(R.id.loading);
    }

    private void init_events(){
        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (text.getText().toString().equals("")){
                    BaseFunctions.showErrorToast(context,"يجب عليك كتابة نص العرض");
                    return;
                }
                callAddOfferAPI();
            }
        });
    }

    private void init_dialog(){}

    private void callAddOfferAPI(){
        loading.smoothToShow();
        add.setVisibility(View.GONE);
        OffersAPIsClass.add_offer(
                context,
                text.getText().toString(),
                new IResponse() {
                    @Override
                    public void onResponse() {
                        loading.smoothToHide();
                        add.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onResponse(Object json) {
                        loading.smoothToHide();
                        add.setVisibility(View.VISIBLE);
                        BaseFunctions.showSuccessToast(context,"تم إضافة العرض بنجاح");
                        iMove.move(0);
                        cancel();
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        loading.smoothToHide();
                        add.setVisibility(View.VISIBLE);
                        BaseFunctions.showErrorToast(context,context.getResources().getString(R.string.no_internet));
                    }
                });
    }
}
