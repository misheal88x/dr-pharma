package com.apps.hm.pharmaapp.Activities.ProductsActivities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.apps.hm.pharmaapp.APIsClasses.CategoriesAPIsClass;
import com.apps.hm.pharmaapp.APIsClasses.SearchAPIsClass;
import com.apps.hm.pharmaapp.Activities.OrdersActivities.CartActivity;
import com.apps.hm.pharmaapp.Adapters.CategoryProductsAdapter;
import com.apps.hm.pharmaapp.Adapters.OwnerOffersAdapter;
import com.apps.hm.pharmaapp.Adapters.StocksAdapter;
import com.apps.hm.pharmaapp.Bases.BaseActivity;
import com.apps.hm.pharmaapp.Bases.BaseFunctions;
import com.apps.hm.pharmaapp.Interfaces.IFailure;
import com.apps.hm.pharmaapp.Interfaces.IProduct;
import com.apps.hm.pharmaapp.Interfaces.IResponse;
import com.apps.hm.pharmaapp.Models.MedicineObject;
import com.apps.hm.pharmaapp.Models.MedicinesResponse;
import com.apps.hm.pharmaapp.Models.UserObject;
import com.apps.hm.pharmaapp.R;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.android.material.snackbar.Snackbar;
import com.google.gson.Gson;
import com.wang.avi.AVLoadingIndicatorView;

import java.util.ArrayList;
import java.util.List;

public class CategoryProductsActivity extends BaseActivity implements IProduct {

    private RelativeLayout toolbar;
    private TextView title;
    private ImageView back,extra;

    private EditText search;
    private RecyclerView recycler;
    private List<MedicineObject> list;
    private CategoryProductsAdapter adapter;
    private GridLayoutManager layoutManager;
    private NestedScrollView scrollView;
    private AVLoadingIndicatorView loading_more;

    private LinearLayout no_data,no_internet,error_layout;
    private ShimmerFrameLayout shimmer;

    private int current_page = 1;
    private boolean continue_paginate = true;
    private int per_page = 20;
    private RelativeLayout root;


    private int api_type = 0;

    private TextView total_price,order;


    @Override
    public void set_layout() {
        setContentView(R.layout.activity_category_products);
    }

    @Override
    public void init_activity(Bundle savedInstanceState) {
        title.setText(getIntent().getStringExtra("name"));
        extra.setImageResource(R.drawable.pharma_shop_off);
        extra.setVisibility(View.VISIBLE);
        init_recycler();
        callGetMedicinesAPI(current_page,0);
    }

    @Override
    public void init_views() {
        toolbar = findViewById(R.id.toolbar);
        title = toolbar.findViewById(R.id.title);
        back = toolbar.findViewById(R.id.back);
        extra = toolbar.findViewById(R.id.extra);
        root = findViewById(R.id.layout);

        search = findViewById(R.id.search);
        recycler = findViewById(R.id.recycler);

        scrollView = findViewById(R.id.scroll_view);

        no_data = findViewById(R.id.no_data_layout);
        no_internet = findViewById(R.id.no_internet_layout);
        error_layout = findViewById(R.id.error_layout);
        shimmer = findViewById(R.id.shimmer);

        total_price = findViewById(R.id.total_price);
        order = findViewById(R.id.order);

        loading_more = findViewById(R.id.loading_more);
    }

    @Override
    public void init_events() {
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                BaseFunctions.runBackSlideAnimation(CategoryProductsActivity.this);
            }
        });
        extra.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(CategoryProductsActivity.this, CartActivity.class));
            }
        });

        search.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    if (search.getText().toString().equals("")){
                        BaseFunctions.showErrorToast(CategoryProductsActivity.this,"يجب عليك كتابة اسم الدواء");
                        return false;
                    }else {
                        BaseFunctions.showWarningToast(CategoryProductsActivity.this,"جاري البحث..");
                        list.clear();
                        adapter = new CategoryProductsAdapter(CategoryProductsActivity.this,list,
                                getIntent().getIntExtra("id",0),0,CategoryProductsActivity.this);
                        recycler.setAdapter(adapter);
                        current_page = 1;
                        continue_paginate = true;
                        callSearchMedicinesAPI(current_page,0,search.getText().toString());
                    }
                    return true;
                }
                return false;
            }
        });

        search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() == 0){
                    list = new ArrayList<>();
                    adapter = new CategoryProductsAdapter(CategoryProductsActivity.this,list,
                            getIntent().getIntExtra("id",0),0,CategoryProductsActivity.this);
                    recycler.setAdapter(adapter);
                    current_page = 1;
                    continue_paginate = true;
                    callGetMedicinesAPI(current_page,0);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        error_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (api_type == 1) {
                    callGetMedicinesAPI(current_page,0);
                }else if (api_type == 2){
                    callSearchMedicinesAPI(current_page,0,search.getText().toString());
                }
            }
        });

        no_internet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (api_type == 1) {
                    callGetMedicinesAPI(current_page,0);
                }else if (api_type == 2){
                    callSearchMedicinesAPI(current_page,0,search.getText().toString());
                }
            }
        });

        scrollView.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
            @Override
            public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                if(v.getChildAt(v.getChildCount() - 1) != null) {
                    if ((scrollY >= (v.getChildAt(v.getChildCount() - 1).getMeasuredHeight() - v.getMeasuredHeight())) &&
                            scrollY > oldScrollY) {
                        if (list.size()>=per_page){
                            if (continue_paginate){
                                current_page++;
                                if (api_type == 1){
                                    callGetMedicinesAPI(current_page,1);
                                }else if (api_type == 2){
                                    callSearchMedicinesAPI(current_page,1,search.getText().toString());
                                }
                            }
                        }
                    }
                }
            }
        });
    }

    @Override
    public void set_fragment_place() {

    }

    private void init_recycler(){
        list = new ArrayList<>();
        adapter = new CategoryProductsAdapter(this,list,
                getIntent().getIntExtra("id",0),0,this);
        layoutManager = new GridLayoutManager(this,2);
        recycler.setLayoutManager(layoutManager);
        recycler.setAdapter(adapter);
        recycler.getItemAnimator().setChangeDuration(0);
    }

    @Override
    public void onBackPressed() {
        finish();
        BaseFunctions.runBackSlideAnimation(CategoryProductsActivity.this);
    }

    private void callGetMedicinesAPI(final int page,final int type){
        api_type = 1;
        no_internet.setVisibility(View.GONE);
        no_data.setVisibility(View.GONE);
        error_layout.setVisibility(View.GONE);
        search.setVisibility(View.GONE);
        if (type == 1){
            loading_more.smoothToShow();
        }else {
            shimmer.startShimmer();
            shimmer.setVisibility(View.VISIBLE);
        }
        CategoriesAPIsClass.get_stocks_products(
                CategoryProductsActivity.this,
                String.valueOf(getIntent().getIntExtra("id",0)),
                page,
                new IResponse() {
                    @Override
                    public void onResponse() {
                        error_happend(type);
                    }

                    @Override
                    public void onResponse(Object json) {
                        if (json!=null) {
                            String j = new Gson().toJson(json);
                            try {
                                MedicinesResponse success = new Gson().fromJson(j, MedicinesResponse.class);
                                if (success.getData() != null) {
                                    if (success.getData().size() > 0) {
                                        process_data(type);
                                        search.setVisibility(View.VISIBLE);
                                        for (MedicineObject o : success.getData()) {
                                            boolean found = false;
                                            for (MedicineObject oo : list){
                                                if (oo.getId() == o .getId()){
                                                    found = true;
                                                    break;
                                                }
                                            }
                                            if (!found){
                                                list.add(o);
                                                adapter.notifyDataSetChanged();
                                            }
                                        }
                                    } else {
                                        no_data(type);
                                    }
                                } else {
                                    error_happend(type);
                                }
                            }catch (Exception e){
                                callGetMedicinesAPI(page,type);
                            }
                        }else {
                            error_happend(type);
                        }
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        error_happend(type);
                        Snackbar.make(root, getResources().getString(R.string.no_internet), Snackbar.LENGTH_INDEFINITE)
                                .setAction("إعادة المحاولة", new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        callGetMedicinesAPI(page,type);
                                    }
                                }).setActionTextColor(getResources().getColor(R.color.white)).show();
                    }
                }
        );
    }

    private void callSearchMedicinesAPI(final int page,final int type,final String name){
        api_type = 2;
        no_internet.setVisibility(View.GONE);
        no_data.setVisibility(View.GONE);
        error_layout.setVisibility(View.GONE);
        if (type == 1){
            loading_more.smoothToShow();
        }else {
            shimmer.startShimmer();
        }
        SearchAPIsClass.search_stock_products(
                CategoryProductsActivity.this,
                String.valueOf(getIntent().getIntExtra("id",0)),
                name,
                page,
                new IResponse() {
                    @Override
                    public void onResponse() {
                        error_happend(type);
                    }

                    @Override
                    public void onResponse(Object json) {
                        if (json!=null) {
                            String j = new Gson().toJson(json);
                            try {
                                MedicinesResponse success = new Gson().fromJson(j, MedicinesResponse.class);
                                if (success.getData() != null) {
                                    if (success.getData().size() > 0) {
                                        process_data(type);
                                        search.setVisibility(View.VISIBLE);
                                        for (MedicineObject o : success.getData()) {
                                            list.add(o);
                                            adapter.notifyDataSetChanged();
                                        }
                                    } else {
                                        no_data(type);
                                    }
                                } else {
                                    error_happend(type);
                                }
                            }catch (Exception e){
                                callSearchMedicinesAPI(page,type,name);
                            }
                        }else {
                            error_happend(type);
                        }
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        error_happend(type);
                        Snackbar.make(root, getResources().getString(R.string.no_internet), Snackbar.LENGTH_INDEFINITE)
                                .setAction("إعادة المحاولة", new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        callSearchMedicinesAPI(page,type,name);
                                    }
                                }).setActionTextColor(getResources().getColor(R.color.white)).show();
                    }
                }
        );
    }

    private void error_happend(int type){
        if (type == 1){
            loading_more.smoothToHide();
        }else {
            shimmer.stopShimmer();
            shimmer.setVisibility(View.GONE);
            no_data.setVisibility(View.VISIBLE);
        }
    }
    private void process_data(int type){
        if (type == 0){
            no_data.setVisibility(View.GONE);
            shimmer.stopShimmer();
            shimmer.setVisibility(View.GONE);
        }else {
            loading_more.smoothToHide();
        }
    }
    private void no_data(int type){
        if (type == 0){
            no_data.setVisibility(View.VISIBLE);
            shimmer.stopShimmer();
            shimmer.setVisibility(View.GONE);
        }else {
            loading_more.smoothToHide();
            Snackbar.make(root,"لا يوجد المزيد",Snackbar.LENGTH_SHORT).show();
            continue_paginate = false;
        }
    }

    @Override
    public void onAddToCart(int position) {

    }

    @Override
    public void onRemoveFromCart(int position) {

    }

    @Override
    public void onRemove(int position) {

    }

    @Override
    public void onSelect(int position) {

    }
}
