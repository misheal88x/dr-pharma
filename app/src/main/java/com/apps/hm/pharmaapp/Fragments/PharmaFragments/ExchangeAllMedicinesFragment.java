package com.apps.hm.pharmaapp.Fragments.PharmaFragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.apps.hm.pharmaapp.APIsClasses.CategoriesAPIsClass;
import com.apps.hm.pharmaapp.APIsClasses.MedicineAPIsClass;
import com.apps.hm.pharmaapp.Adapters.ExchangeMedicinesAdapter;
import com.apps.hm.pharmaapp.Adapters.PharmaOffersAdapter;
import com.apps.hm.pharmaapp.Bases.App;
import com.apps.hm.pharmaapp.Bases.BaseFragment;
import com.apps.hm.pharmaapp.Interfaces.IFailure;
import com.apps.hm.pharmaapp.Interfaces.IMove;
import com.apps.hm.pharmaapp.Interfaces.IResponse;
import com.apps.hm.pharmaapp.Models.ExchangeMedicinesResponse;
import com.apps.hm.pharmaapp.Models.MedicineObject;
import com.apps.hm.pharmaapp.Models.MedicinesResponse;
import com.apps.hm.pharmaapp.Models.UserObject;
import com.apps.hm.pharmaapp.R;
import com.apps.hm.pharmaapp.Utils.EndlessRecyclerViewScrollListener;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.android.material.snackbar.Snackbar;
import com.google.gson.Gson;
import com.wang.avi.AVLoadingIndicatorView;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class ExchangeAllMedicinesFragment extends BaseFragment {

    private RecyclerView recycler;
    private List<MedicineObject> list;
    private ExchangeMedicinesAdapter adapter;
    private LinearLayoutManager layoutManager;

    private LinearLayout no_data,no_internet,error_layout;
    private ShimmerFrameLayout shimmer;

    private int current_page = 1;
    private boolean continue_paginate = true;
    private int per_page = 10;

    private AVLoadingIndicatorView loading_more;
    private RelativeLayout root;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_exchange_all_medicines,container,false);
    }

    @Override
    public void init_views() {
        recycler = base.findViewById(R.id.recycler);
        no_data = base.findViewById(R.id.no_data_layout);
        no_internet = base.findViewById(R.id.no_internet_layout);
        error_layout = base.findViewById(R.id.error_layout);
        shimmer = base.findViewById(R.id.shimmer);
        loading_more = base.findViewById(R.id.loading_more);
        root = base.findViewById(R.id.layout);
    }

    @Override
    public void init_events() {
        error_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callMedicinesAPI(current_page,0);
            }
        });

        no_internet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callMedicinesAPI(current_page,0);
            }
        });
    }

    @Override
    public void init_fragment(Bundle savedInstanceState) {
        init_recycler();
        callMedicinesAPI(current_page,0);
    }

    private void init_recycler(){
        list = new ArrayList<>();
        adapter = new ExchangeMedicinesAdapter(base, list, new IMove() {
            @Override
            public void move(int position) {
                no_data.setVisibility(View.VISIBLE);
            }
        });
        layoutManager = new LinearLayoutManager(base);
        recycler.setLayoutManager(layoutManager);
        recycler.setAdapter(adapter);
        recycler.addOnScrollListener(new EndlessRecyclerViewScrollListener(layoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                if (list.size()>=per_page){
                    if (continue_paginate){
                        current_page++;
                        callMedicinesAPI(current_page,1);
                    }
                }
            }
        });
    }

    private void callMedicinesAPI(final int page, final int type){
        if (type == 1){
            loading_more.smoothToShow();
        }else {
            shimmer.startShimmer();
            shimmer.setVisibility(View.VISIBLE);
        }
        MedicineAPIsClass.get_exchange_medicines(
                base,
                page,
                new IResponse() {
                    @Override
                    public void onResponse() {
                        error_happend(type);
                    }

                    @Override
                    public void onResponse(Object json) {
                        shimmer.stopShimmer();
                        shimmer.setVisibility(View.GONE);
                        String j = new Gson().toJson(json);
                        try {
                            ExchangeMedicinesResponse success = new Gson().fromJson(j, ExchangeMedicinesResponse.class);
                            if (type == 0){
                                if (success.getOwner()!=null){
                                    if (success.getOwner().size()>0){
                                        for (MedicineObject o : success.getOwner()){
                                            list.add(o);
                                            adapter.notifyDataSetChanged();
                                        }
                                    }
                                }
                            }
                            if (success.getOther().getData() != null) {
                                if (success.getOther().getData().size() > 0) {
                                    process_data(type);
                                    per_page = success.getOther().getPer_page();
                                    for (MedicineObject o : success.getOther().getData()) {
                                        list.add(o);
                                        adapter.notifyDataSetChanged();
                                    }
                                } else {

                                }
                            } else {
                                //error_happend(type);
                            }
                            if (list.size() == 0){
                                no_data(type);
                            }
                        }catch (Exception e){
                            callMedicinesAPI(page,type);
                        }
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        error_happend(type);
                        Snackbar.make(root, getResources().getString(R.string.no_internet), Snackbar.LENGTH_INDEFINITE)
                                .setAction("إعادة المحاولة", new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        callMedicinesAPI(page,type);
                                    }
                                }).setActionTextColor(getResources().getColor(R.color.white)).show();
                    }
                }
        );
    }
    private void error_happend(int type){
        if (type == 1){
            loading_more.smoothToHide();
        }else {
            shimmer.stopShimmer();
            shimmer.setVisibility(View.GONE);
            no_data.setVisibility(View.VISIBLE);
        }
    }
    private void process_data(int type){
        if (type == 0){
            no_data.setVisibility(View.GONE);
            shimmer.stopShimmer();
            shimmer.setVisibility(View.GONE);
        }else {
            loading_more.smoothToHide();
        }
    }
    private void no_data(int type){
        if (type == 0){
            no_data.setVisibility(View.VISIBLE);
            shimmer.stopShimmer();
            shimmer.setVisibility(View.GONE);
        }else {
            loading_more.smoothToHide();
            Snackbar.make(root,"لا يوجد المزيد",Snackbar.LENGTH_SHORT).show();
            continue_paginate = false;
        }
    }
}
