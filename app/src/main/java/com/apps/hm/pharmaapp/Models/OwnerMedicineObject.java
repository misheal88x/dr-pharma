package com.apps.hm.pharmaapp.Models;

import com.google.gson.annotations.SerializedName;

public class OwnerMedicineObject {
    @SerializedName("id") private int id = 0;
    @SerializedName("count") private int count = 0;
    @SerializedName("med_id") private int med_id = 0;
    @SerializedName("user_id") private int user_id = 0;
    @SerializedName("created_at") private String created_at = "";
    @SerializedName("updated_at") private String updated_at = "";
    @SerializedName("medicine") private MedicineObject medicine = new MedicineObject();

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public int getMed_id() {
        return med_id;
    }

    public void setMed_id(int med_id) {
        this.med_id = med_id;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public MedicineObject getMedicine() {
        return medicine;
    }

    public void setMedicine(MedicineObject medicine) {
        this.medicine = medicine;
    }
}
