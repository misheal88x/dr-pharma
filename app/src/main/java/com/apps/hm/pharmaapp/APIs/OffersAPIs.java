package com.apps.hm.pharmaapp.APIs;

import com.apps.hm.pharmaapp.Models.BaseResponse;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface OffersAPIs {

    @GET("offers")
    Call<BaseResponse> get_offers(
            @Header("Accept") String accept,
            @Query("page") int page
    );

    @FormUrlEncoded
    @POST("add-offer")
    Call<BaseResponse> add_offer(
            @Header("Accept") String accept,
            @Field("text") String text
    );

    @GET("my-offers")
    Call<BaseResponse> get_my_offers(
            @Header("Accept") String accept
    );

    @GET("delete-offer/{id}")
    Call<BaseResponse> delete_offer(
            @Header("Accept") String accept,
            @Path("id") String id
    );
}
