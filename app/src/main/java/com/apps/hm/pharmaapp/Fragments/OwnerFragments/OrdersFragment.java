package com.apps.hm.pharmaapp.Fragments.OwnerFragments;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.apps.hm.pharmaapp.APIsClasses.OrdersAPIsClass;
import com.apps.hm.pharmaapp.APIsClasses.UserAPIsClass;
import com.apps.hm.pharmaapp.Activities.OrdersActivities.OrderDetailsActivity;
import com.apps.hm.pharmaapp.Adapters.OwnerOrdersAdapter;
import com.apps.hm.pharmaapp.Bases.App;
import com.apps.hm.pharmaapp.Bases.BaseFragment;
import com.apps.hm.pharmaapp.Bases.SharedPrefManager;
import com.apps.hm.pharmaapp.Interfaces.IFailure;
import com.apps.hm.pharmaapp.Interfaces.IMove;
import com.apps.hm.pharmaapp.Interfaces.IResponse;
import com.apps.hm.pharmaapp.Models.OrderModels.OrderObject;
import com.apps.hm.pharmaapp.R;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import static android.app.Activity.RESULT_OK;

public class OrdersFragment extends BaseFragment implements IMove {

    int[] header_text_views = new int[]{R.id.txt1, R.id.txt2, R.id.txt3};
    int[] header_views = new int[]{R.id.view1, R.id.view2, R.id.view3};

    private LinearLayout new_orders,running_orders,finished_orders;
    private int selected_tab = 0;

    private RecyclerView recycler;
    private List<OrderObject> list;
    private OwnerOrdersAdapter adapter;
    private LinearLayoutManager layoutManager;
    private ShimmerFrameLayout shimmer;


    private LinearLayout no_data,no_internet,error_layout;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_orders,container,false);
    }

    @Override
    public void init_views() {
        new_orders = base.findViewById(R.id.new_orders);
        running_orders = base.findViewById(R.id.running_orders);
        finished_orders = base.findViewById(R.id.finished_orders);
        recycler = base.findViewById(R.id.orders_recycler);
        shimmer = base.findViewById(R.id.shimmer);
        no_data = base.findViewById(R.id.no_data_layout);
        no_internet = base.findViewById(R.id.no_internet_layout);
        error_layout = base.findViewById(R.id.error_layout);
    }

    @Override
    public void init_events() {
        new_orders.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!App.is_api_called) {
                    setActive(0);
                    list.clear();
                    adapter = new OwnerOrdersAdapter(base, list, OrdersFragment.this);
                    recycler.setAdapter(adapter);
                    callOrdersAPI(selected_tab);
                }
            }
        });
        running_orders.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!App.is_api_called) {
                    setActive(1);
                    list.clear();
                    adapter = new OwnerOrdersAdapter(base, list, OrdersFragment.this);
                    recycler.setAdapter(adapter);
                    callOrdersAPI(selected_tab);
                }
            }
        });
        finished_orders.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!App.is_api_called) {
                    setActive(2);
                    list.clear();
                    adapter = new OwnerOrdersAdapter(base, list, OrdersFragment.this);
                    recycler.setAdapter(adapter);
                    callOrdersAPI(selected_tab);
                }
            }
        });

        error_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callOrdersAPI(selected_tab);
            }
        });

        no_internet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callOrdersAPI(selected_tab);
            }
        });
    }

    @Override
    public void init_fragment(Bundle savedInstanceState) {
        setActive(0);
        init_recycler();
        callOrdersAPI(selected_tab);
    }

    void setActive(int index){
        selected_tab = index;
        for(int i=0;i<header_views.length;i++){
            ((TextView)base.findViewById(header_text_views[i])).setTextColor(getResources().getColor(R.color.black));
            ((View)base.findViewById(header_views[i])).setBackgroundColor(getResources().getColor(R.color.white));
        }
        ((TextView)base.findViewById(header_text_views[index])).setTextColor(getResources().getColor(R.color.colorPrimary));
        ((View)base.findViewById(header_views[index])).setBackgroundColor(getResources().getColor(R.color.colorPrimary));
    }

    private void init_recycler(){
        list = new ArrayList<>();
        adapter = new OwnerOrdersAdapter(base,list,OrdersFragment.this);
        layoutManager = new LinearLayoutManager(base);
        recycler.setLayoutManager(layoutManager);
        recycler.setAdapter(adapter);
    }

    private void callOrdersAPI(final int type){
        final int new_type = type+1;
        shimmer.showShimmer(true);
        shimmer.setVisibility(View.VISIBLE);
        error_layout.setVisibility(View.GONE);
        no_internet.setVisibility(View.GONE);
        no_data.setVisibility(View.GONE);
        OrdersAPIsClass.get_stock_orders(
                base,
                new IResponse() {
                    @Override
                    public void onResponse() {
                        shimmer.showShimmer(false);
                        shimmer.setVisibility(View.GONE);
                        error_layout.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onResponse(Object json) {
                        shimmer.showShimmer(false);
                        shimmer.setVisibility(View.GONE);
                        String j = new Gson().toJson(json);
                        try {
                            OrderObject[] success = new Gson().fromJson(j, OrderObject[].class);
                            if (success.length > 0) {
                                for (OrderObject o : success) {
                                    if (o.getStatus() == new_type) {
                                        list.add(o);
                                        adapter.notifyDataSetChanged();
                                    }
                                }
                                if (list.size() == 0) {
                                    no_data.setVisibility(View.VISIBLE);
                                }
                            } else {
                                no_data.setVisibility(View.VISIBLE);
                            }
                            FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(base, new OnSuccessListener<InstanceIdResult>() {
                                @Override
                                public void onSuccess(InstanceIdResult instanceIdResult) {
                                    String token = instanceIdResult.getToken();
                                    if (App.is_token_updated == false) {
                                        Log.i("dfdfrf", "onResponse: "+token);
                                        if (SharedPrefManager.getInstance(base).getUser().getId() != 0) {
                                            App.is_token_updated = true;
                                            callUpdateTokenAPI(token);
                                        }
                                    }
                                }
                            });
                        }catch (Exception e){
                            callOrdersAPI(type);
                        }
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        shimmer.showShimmer(false);
                        shimmer.setVisibility(View.GONE);
                        no_internet.setVisibility(View.VISIBLE);
                    }
                });
    }

    @Override
    public void move(int position) {
        Intent intent = new Intent(base, OrderDetailsActivity.class);
        intent.putExtra("order",new Gson().toJson(list.get(position)));
        startActivityForResult(intent,100);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == 100){
            if (resultCode == RESULT_OK){
                list.clear();
                adapter = new OwnerOrdersAdapter(base,list,this);
                recycler.setAdapter(adapter);
                callOrdersAPI(selected_tab);
            }
        }
    }

    private void callUpdateTokenAPI(String token){
        App.is_token_updated = true;
        UserAPIsClass.update_fcm(base,
                token,
                new IResponse() {
                    @Override
                    public void onResponse() {
                        Log.i("firebase_token_update", "onResponse: "+"Failed");
                    }

                    @Override
                    public void onResponse(Object json) {
                        Log.i("firebase_token_update", "onResponse: "+"Success");
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        Log.i("firebase_token_update", "onResponse: "+"No Internet");
                    }
                });
    }
}
