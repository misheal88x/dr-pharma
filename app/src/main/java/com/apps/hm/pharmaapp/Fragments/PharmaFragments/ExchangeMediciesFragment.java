package com.apps.hm.pharmaapp.Fragments.PharmaFragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.apps.hm.pharmaapp.Bases.BaseFragment;
import com.apps.hm.pharmaapp.R;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class ExchangeMediciesFragment extends BaseFragment {

    int[] header_text_views = new int[]{R.id.txt1, R.id.txt2};
    int[] header_views = new int[]{R.id.view1, R.id.view2};

    private LinearLayout medicines_list,add_medicine;
    private int selected_tab = 0;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_exchange_medicines,container,false);
    }

    @Override
    public void init_views() {
        medicines_list = base.findViewById(R.id.medicines_list);
        add_medicine = base.findViewById(R.id.add_medicine);
    }

    @Override
    public void init_events() {
        medicines_list.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (selected_tab!=0){
                    setActive(0);
                    open_fragment(new ExchangeAllMedicinesFragment());
                }
            }
        });
        add_medicine.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (selected_tab!=1){
                    setActive(1);
                    open_fragment(new ExchangeAddMedicineFragment());
                }
            }
        });
    }

    @Override
    public void init_fragment(Bundle savedInstanceState) {
        setActive(0);
        fragment_place = base.findViewById(R.id.container);
        open_fragment(new ExchangeAllMedicinesFragment());
    }

    void setActive(int index){
        selected_tab = index;
        for(int i=0;i<header_views.length;i++){
            ((TextView)base.findViewById(header_text_views[i])).setTextColor(getResources().getColor(R.color.black));
            ((View)base.findViewById(header_views[i])).setBackgroundColor(getResources().getColor(R.color.white));
        }
        ((TextView)base.findViewById(header_text_views[index])).setTextColor(getResources().getColor(R.color.colorPrimary));
        ((View)base.findViewById(header_views[index])).setBackgroundColor(getResources().getColor(R.color.colorPrimary));
    }
}
