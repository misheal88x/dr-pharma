package com.apps.hm.pharmaapp.Models;

import com.google.gson.annotations.SerializedName;

public class UserObject {
    @SerializedName("id") private int id = 0;
    @SerializedName("name") private String name = "";
    @SerializedName("user_name") private String user_name = "";
    @SerializedName("phone") private String phone = "";
    @SerializedName("address") private String address = "";
    @SerializedName("location") private String location = "";
    @SerializedName("role_id") private int role_id = 0;
    @SerializedName("api_token") private String api_token = "";
    @SerializedName("avatar") private String avatar = "";
    @SerializedName("contact_phone") private String contact_phone = "";
    @SerializedName("allow_med_price") private int allow_med_price = 0;
    @SerializedName("allow_med_exchange") private int allow_med_exchange = 0;
    @SerializedName("password") private String password = "";
    @SerializedName("active") private int active = 0;
    @SerializedName("updated_at") private String updated_at = "";
    @SerializedName("created_at") private String created_at = "";

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public int getRole_id() {
        return role_id;
    }

    public void setRole_id(int role_id) {
        this.role_id = role_id;
    }

    public String getApi_token() {
        return api_token;
    }

    public void setApi_token(String api_token) {
        this.api_token = api_token;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getContact_phone() {
        return contact_phone;
    }

    public void setContact_phone(String contact_phone) {
        this.contact_phone = contact_phone;
    }

    public int getAllow_med_price() {
        return allow_med_price;
    }

    public void setAllow_med_price(int allow_med_price) {
        this.allow_med_price = allow_med_price;
    }

    public int getAllow_med_exchange() {
        return allow_med_exchange;
    }

    public void setAllow_med_exchange(int allow_med_exchange) {
        this.allow_med_exchange = allow_med_exchange;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getActive() {
        return active;
    }

    public void setActive(int active) {
        this.active = active;
    }
}
