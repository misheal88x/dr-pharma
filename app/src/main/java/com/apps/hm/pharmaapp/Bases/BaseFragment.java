package com.apps.hm.pharmaapp.Bases;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

public abstract class BaseFragment extends Fragment {
    protected ViewGroup fragment_place;
    protected BaseActivity base;
    protected BaseFragment previous;
    public FragmentManager fragmentManager;


    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
        void onOpenFragment(String tag, String title);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        base = (BaseActivity) activity;
    }



    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        init(savedInstanceState);
    }

    public void init(Bundle savedInstanceState){
        fragmentManager = getChildFragmentManager();
        init_views();
        init_events();
        init_fragment(savedInstanceState);
    }

    public abstract void init_views();

    public abstract void init_events();

    public abstract void init_fragment(Bundle savedInstanceState);

    public void set_previous(BaseFragment fragment){
        previous = fragment;
    }


    public BaseFragment get_previous_fragment() {
        return previous;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    public void open_fragment(final BaseFragment fragment) {
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                //if (current_fragment != fragment) {
                //try {
                FragmentTransaction transaction = fragmentManager.beginTransaction();
                transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
                transaction.replace(fragment_place.getId(), fragment);
                transaction.addToBackStack("Home");
                transaction.commit();
                //if (current_fragment!=null){
                //    fragment.set_previous(current_fragment);
                // }
                // current_fragment = fragment;
                //}catch (Exception e){
                //e.printStackTrace();
                // }
                //}
            }
        };
        base.runOnUiThread(runnable);
    }
}
