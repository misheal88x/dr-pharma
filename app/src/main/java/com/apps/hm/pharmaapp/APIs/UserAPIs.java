package com.apps.hm.pharmaapp.APIs;

import com.apps.hm.pharmaapp.Models.BaseResponse;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

public interface UserAPIs {

    @Multipart
    @POST("register")
    Call<BaseResponse> register(
            @Header("Accept") String accept,
            @Part("name") RequestBody name,
            @Part("phone") RequestBody phone,
            @Part("password") RequestBody password,
            @Part("role_id") RequestBody role_id,
            @Part("user_name") RequestBody user_name,
            @Part("address") RequestBody address,
            @Part("location") RequestBody location,
            @Part MultipartBody.Part image
    );

    @FormUrlEncoded
    @POST("login")
    Call<BaseResponse> login(
            @Header("Accept") String accept,
            @Field("user_name") String user_name,
            @Field("password") String password
    );

    @FormUrlEncoded
    @POST("update-fcm")
    Call<BaseResponse> update_fcm(
            @Header("Accept") String accept,
            @Field("fcm_token") String token
    );
}
