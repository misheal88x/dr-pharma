package com.apps.hm.pharmaapp.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.apps.hm.pharmaapp.Bases.BaseFunctions;
import com.apps.hm.pharmaapp.Models.OrderModels.OrderProductObject;
import com.apps.hm.pharmaapp.R;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import de.hdodenhof.circleimageview.CircleImageView;

public class OrderProductsAdapter extends RecyclerView.Adapter<OrderProductsAdapter.ViewHolder>{
    private Context context;
    private List<OrderProductObject> list;

    public OrderProductsAdapter(Context context,List<OrderProductObject> list) {
        this.context = context;
        this.list = list;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private CircleImageView image;
        private TextView name,qty;
        private View divider;

        public ViewHolder(View view) {
            super(view);
            image = view.findViewById(R.id.image);
            name = view.findViewById(R.id.name);
            qty = view.findViewById(R.id.qty);
            divider = view.findViewById(R.id.divider);
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_order_product, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {

        OrderProductObject o = list.get(position);

        if (o.getMedicine()!=null){
            if (o.getMedicine().getName()!=null){
                holder.name.setText(o.getMedicine().getName());
            }
            if (o.getMedicine().getMedicine()!=null){
                BaseFunctions.setGlideImage(context,holder.image,o.getMedicine().getMedicine().getImage());
            }
            holder.qty.setText("العدد المطلوب : "+o.getCount()+" قطعة");
        }


        if (position == list.size()-1){
            holder.divider.setVisibility(View.GONE);
        }else {
            holder.divider.setVisibility(View.VISIBLE);
        }
    }
}
