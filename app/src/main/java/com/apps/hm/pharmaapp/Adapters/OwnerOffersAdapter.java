package com.apps.hm.pharmaapp.Adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.apps.hm.pharmaapp.APIsClasses.OffersAPIsClass;
import com.apps.hm.pharmaapp.Activities.OrdersActivities.OrderDetailsActivity;
import com.apps.hm.pharmaapp.Bases.BaseFunctions;
import com.apps.hm.pharmaapp.Interfaces.IFailure;
import com.apps.hm.pharmaapp.Interfaces.IResponse;
import com.apps.hm.pharmaapp.Models.OwnerOfferObject;
import com.apps.hm.pharmaapp.R;
import com.wang.avi.AVLoadingIndicatorView;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.PopupMenu;
import androidx.recyclerview.widget.RecyclerView;
import de.hdodenhof.circleimageview.CircleImageView;

public class OwnerOffersAdapter extends RecyclerView.Adapter<OwnerOffersAdapter.ViewHolder>{
    private Context context;
    private List<OwnerOfferObject> list;

    public OwnerOffersAdapter(Context context,List<OwnerOfferObject> list) {
        this.context = context;
        this.list = list;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView text,date;
        private ImageView more;
        private AVLoadingIndicatorView loading;

        public ViewHolder(View view) {
            super(view);
            text = view.findViewById(R.id.text);
            date = view.findViewById(R.id.date);
            more = view.findViewById(R.id.more);
            loading = view.findViewById(R.id.loading);
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_owner_offer, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {

        final OwnerOfferObject o = list.get(position);
        holder.text.setText(o.getText());
        holder.date.setText(BaseFunctions.processDate(context,o.getCreated_at()));
        holder.more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PopupMenu popup = new PopupMenu(context, holder.more);
                popup.inflate(R.menu.owner_offer_options);
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.delete:
                                callDeleteAPI(holder,position,String.valueOf(o.getId()));
                                return true;
                        }
                        return false;
                    }
                });
                popup.show();
            }
        });
    }

    private void callDeleteAPI(final ViewHolder holder, final int position,String id){
        holder.more.setVisibility(View.GONE);
        holder.loading.smoothToShow();
        OffersAPIsClass.delete_offer(context, id, new IResponse() {
            @Override
            public void onResponse() {
                holder.more.setVisibility(View.VISIBLE);
                holder.loading.smoothToHide();
            }

            @Override
            public void onResponse(Object json) {
                holder.more.setVisibility(View.VISIBLE);
                holder.loading.smoothToHide();
                BaseFunctions.showSuccessToast(context,"تم الحذف بنجاح");
                list.remove(position);
                notifyDataSetChanged();
            }
        }, new IFailure() {
            @Override
            public void onFailure() {
                holder.more.setVisibility(View.VISIBLE);
                holder.loading.smoothToHide();
                BaseFunctions.showErrorToast(context,context.getResources().getString(R.string.no_internet));
            }
        });
    }
}
