package com.apps.hm.pharmaapp.Activities.OrdersActivities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.apps.hm.pharmaapp.APIsClasses.OrdersAPIsClass;
import com.apps.hm.pharmaapp.Adapters.CategoryProductsAdapter;
import com.apps.hm.pharmaapp.Adapters.OrderProductsAdapter;
import com.apps.hm.pharmaapp.Bases.BaseActivity;
import com.apps.hm.pharmaapp.Bases.BaseFunctions;
import com.apps.hm.pharmaapp.Bases.SharedPrefManager;
import com.apps.hm.pharmaapp.Fragments.OwnerFragments.OrdersFragment;
import com.apps.hm.pharmaapp.Interfaces.IFailure;
import com.apps.hm.pharmaapp.Interfaces.IProduct;
import com.apps.hm.pharmaapp.Interfaces.IResponse;
import com.apps.hm.pharmaapp.Models.MedicineObject;
import com.apps.hm.pharmaapp.R;
import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class CartActivity extends BaseActivity implements IProduct {
    private RelativeLayout toolbar;
    private TextView title;
    private ImageView back;

    private RecyclerView recycler;
    private List<MedicineObject> list;
    private CategoryProductsAdapter adapter;
    private GridLayoutManager layoutManager;

    private LinearLayout no_data;
    private LinearLayout result_layout;
    private TextView order,total_price;
    private RelativeLayout loading;

    private String total_price_string = "";

    @Override
    public void set_layout() {
        setContentView(R.layout.activity_cart);
    }

    @Override
    public void init_activity(Bundle savedInstanceState) {
        title.setText("السلة");
        init_recycler();
        getData();
    }

    @Override
    public void init_views() {
        toolbar = findViewById(R.id.toolbar);
        title = toolbar.findViewById(R.id.title);
        back = toolbar.findViewById(R.id.back);
        recycler = findViewById(R.id.recycler);
        no_data = findViewById(R.id.no_data_layout);
        result_layout = findViewById(R.id.result_layout);
        order = findViewById(R.id.order);
        total_price = findViewById(R.id.total_price);
        loading = findViewById(R.id.loading);
    }

    @Override
    public void init_events() {
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                BaseFunctions.runBackSlideAnimation(CartActivity.this);
            }
        });

        order.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (list.size() == 0){
                    BaseFunctions.showErrorToast(CartActivity.this,"لا يوجد أدوية في السلة");
                    return;
                }
                callAddOrderAPI();
            }
        });
    }

    @Override
    public void set_fragment_place() {

    }

    @Override
    public void onBackPressed() {
        finish();
        BaseFunctions.runBackSlideAnimation(CartActivity.this);
    }

    private void init_recycler(){
        list = new ArrayList<>();
        adapter = new CategoryProductsAdapter(CartActivity.this,list,0,1,this);
        layoutManager = new GridLayoutManager(CartActivity.this,2);
        recycler.setLayoutManager(layoutManager);
        recycler.setAdapter(adapter);
        recycler.getItemAnimator().setChangeDuration(0);
    }

    @Override
    public void onAddToCart(int position) {

    }

    @Override
    public void onRemoveFromCart(int position) {

    }

    @Override
    public void onRemove(int position) {

    }

    @Override
    public void onSelect(int position) {

    }

    private void getData(){
        float total_price_value= 0.0f;
        if (SharedPrefManager.getInstance(CartActivity.this).getCart().size()>0){
            for (MedicineObject m : SharedPrefManager.getInstance(CartActivity.this).getCart()){
                float p_price = m.getMedicine().getPrice()*m.getOrdered_count();
                total_price_value = total_price_value+p_price;
                list.add(m);
                adapter.notifyDataSetChanged();
            }
            total_price.setText("سعر الطلبية : "+total_price_value+" ل.س");
            total_price_string = String.valueOf(total_price_value);
        }else {
            no_data.setVisibility(View.VISIBLE);
            result_layout.setVisibility(View.GONE);
        }
    }


    private void callAddOrderAPI(){
        final List<OrderProduct> list_products = new ArrayList<>();
        int order_count = 0;
        for (MedicineObject o : SharedPrefManager.getInstance(CartActivity.this).getCart()){
            order_count = order_count + o.getOrdered_count();
            OrderProduct p = new OrderProduct();
            p.setMed_id(o.getId());
            p.setCount(o.getOrdered_count());
            list_products.add(p);
        }

        loading.setVisibility(View.VISIBLE);
        order.setVisibility(View.GONE);
        OrdersAPIsClass.add_order(
                CartActivity.this,
                SharedPrefManager.getInstance(CartActivity.this).getRepoId(),
                order_count,
                total_price_string,
                new Gson().toJson(list_products),
                new IResponse() {
                    @Override
                    public void onResponse() {
                        loading.setVisibility(View.GONE);
                        order.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onResponse(Object json) {
                        loading.setVisibility(View.GONE);
                        order.setVisibility(View.VISIBLE);
                        BaseFunctions.showSuccessToast(CartActivity.this,"تم إضافة الطلب بنجاح");
                        SharedPrefManager.getInstance(CartActivity.this).setCart(new ArrayList<MedicineObject>());
                        SharedPrefManager.getInstance(CartActivity.this).setRepoId(0);
                        list.clear();
                        adapter = new CategoryProductsAdapter(CartActivity.this,list,0,1,CartActivity.this);
                        recycler.setAdapter(adapter);
                        no_data.setVisibility(View.VISIBLE);
                        result_layout.setVisibility(View.GONE);
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        loading.setVisibility(View.GONE);
                        order.setVisibility(View.VISIBLE);
                        BaseFunctions.showErrorToast(CartActivity.this,getResources().getString(R.string.no_internet));
                    }
                });
    }

    class OrderProduct {
        @SerializedName("med_id") private int med_id = 0;
        @SerializedName("count") private int count = 0;

        public int getMed_id() {
            return med_id;
        }

        public void setMed_id(int med_id) {
            this.med_id = med_id;
        }

        public int getCount() {
            return count;
        }

        public void setCount(int count) {
            this.count = count;
        }
    }
}
