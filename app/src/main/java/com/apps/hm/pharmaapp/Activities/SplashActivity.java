package com.apps.hm.pharmaapp.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.View;

import com.apps.hm.pharmaapp.APIsClasses.UserAPIsClass;
import com.apps.hm.pharmaapp.Activities.AccountActivities.LoginActivity;
import com.apps.hm.pharmaapp.Activities.AccountActivities.SelectAccountTypeActivity;
import com.apps.hm.pharmaapp.Activities.HomesActivities.PharmaHomeActivity;
import com.apps.hm.pharmaapp.Activities.HomesActivities.StockOwnerHomeActivity;
import com.apps.hm.pharmaapp.Bases.BaseActivity;
import com.apps.hm.pharmaapp.Bases.BaseFunctions;
import com.apps.hm.pharmaapp.Bases.SharedPrefManager;
import com.apps.hm.pharmaapp.Interfaces.IFailure;
import com.apps.hm.pharmaapp.Interfaces.IResponse;
import com.apps.hm.pharmaapp.Models.UserObject;
import com.apps.hm.pharmaapp.R;
import com.google.gson.Gson;

public class SplashActivity extends BaseActivity {


    @Override
    public void set_layout() {
        setContentView(R.layout.activity_splash);
    }

    @Override
    public void init_activity(Bundle savedInstanceState) {
        //No user
        if (SharedPrefManager.getInstance(SplashActivity.this).getUser().getId() == 0){
            CountDownTimer timer = new CountDownTimer(2000,1000) {
                @Override
                public void onTick(long millisUntilFinished) {

                }

                @Override
                public void onFinish() {
                    startActivity(new Intent(SplashActivity.this, SelectAccountTypeActivity.class));
                    finish();
                }
            }.start();
        }
        //User
        else {
            if (SharedPrefManager.getInstance(SplashActivity.this).getUser().getRole_id() == 2) {
                callLoginAPI();
            }else {
                CountDownTimer timer = new CountDownTimer(2000,1000) {
                    @Override
                    public void onTick(long millisUntilFinished) {

                    }

                    @Override
                    public void onFinish() {
                        startActivity(new Intent(SplashActivity.this, SelectAccountTypeActivity.class));
                        finish();
                    }
                }.start();
            }
        }
    }

    @Override
    public void init_views() {

    }

    @Override
    public void init_events() {

    }

    @Override
    public void set_fragment_place() {

    }

    private void callLoginAPI(){
        final String password = SharedPrefManager.getInstance(SplashActivity.this).getUser().getPassword();
        UserAPIsClass.login(
                SplashActivity.this,
                SharedPrefManager.getInstance(SplashActivity.this).getUser().getUser_name(),
                SharedPrefManager.getInstance(SplashActivity.this).getUser().getPassword(),
                new IResponse() {
                    @Override
                    public void onResponse() {

                    }

                    @Override
                    public void onResponse(Object json) {
                        String j = new Gson().toJson(json);
                        UserObject success = new Gson().fromJson(j,UserObject.class);
                        success.setPassword(password);
                        SharedPrefManager.getInstance(SplashActivity.this).setUser(success);
                        startActivity(new Intent(SplashActivity.this, SelectAccountTypeActivity.class));
                        finish();
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        BaseFunctions.showErrorToast(SplashActivity.this,getResources().getString(R.string.no_internet));
                    }
                }
        );
    }
}
