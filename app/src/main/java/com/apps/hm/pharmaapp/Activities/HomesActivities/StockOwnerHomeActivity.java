package com.apps.hm.pharmaapp.Activities.HomesActivities;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.apps.hm.pharmaapp.Activities.AccountActivities.SelectAccountTypeActivity;
import com.apps.hm.pharmaapp.Activities.AccountActivities.SelectLoginTypeActivity;
import com.apps.hm.pharmaapp.Bases.App;
import com.apps.hm.pharmaapp.Bases.BaseActivity;
import com.apps.hm.pharmaapp.Bases.BaseFunctions;
import com.apps.hm.pharmaapp.Bases.SharedPrefManager;
import com.apps.hm.pharmaapp.Dialogs.OwnerDialogs.AddOfferDialog;
import com.apps.hm.pharmaapp.Fragments.OwnerFragments.MedicinesFragment;
import com.apps.hm.pharmaapp.Fragments.OwnerFragments.OffersFragment;
import com.apps.hm.pharmaapp.Fragments.OwnerFragments.OrdersFragment;
import com.apps.hm.pharmaapp.Interfaces.IMove;
import com.apps.hm.pharmaapp.Models.UserObject;
import com.apps.hm.pharmaapp.R;

public class StockOwnerHomeActivity extends BaseActivity {
    private RelativeLayout toolbar;
    private TextView title;
    private ImageView back;
    private ImageView extra;
    private LinearLayout offers_layout,orders_layout,medicines_layout;
    private ImageView offer_icon,orders_icon,medicines_icon;
    private TextView offers_txt,orders_txt,medicines_txt;
    private boolean is_back_pressed = false;

    @Override
    public void set_layout() {
        setContentView(R.layout.activity_stock_owner_home);
    }

    @Override
    public void init_activity(Bundle savedInstanceState) {
        click_orders();
        back.setVisibility(View.GONE);
    }

    @Override
    public void init_views() {
        toolbar = findViewById(R.id.toolbar);
        title = toolbar.findViewById(R.id.title);
        back = toolbar.findViewById(R.id.back);
        extra = toolbar.findViewById(R.id.extra);

        offers_layout = findViewById(R.id.offers_layout);
        orders_layout = findViewById(R.id.orders_layout);
        medicines_layout = findViewById(R.id.medicines_layout);
        offer_icon = findViewById(R.id.offers_icon);
        orders_icon = findViewById(R.id.orders_icon);
        medicines_icon = findViewById(R.id.medicines_icon);
        offers_txt = findViewById(R.id.offers_txt);
        orders_txt = findViewById(R.id.orders_txt);
        medicines_txt = findViewById(R.id.medicines_txt);
    }

    @Override
    public void init_events() {
        orders_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!App.is_api_called) {
                    click_orders();
                }
            }
        });

        offers_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!App.is_api_called) {
                    click_offers();
                }
            }
        });

        medicines_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!App.is_api_called) {
                    click_medicines();
                }
            }
        });

    }

    @Override
    public void set_fragment_place() {
        fragment_place = findViewById(R.id.container);
    }

    @Override
    public void onBackPressed() {
        Fragment f = fragmentManager.findFragmentById(R.id.container);
        if (f != null){
            if (f instanceof OrdersFragment){
                if (is_back_pressed){
                    Intent intent = new Intent(Intent.ACTION_MAIN);
                    intent.addCategory(Intent.CATEGORY_HOME);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    finish();
                   BaseFunctions.runBackSlideAnimation(this);
                }else {
                    is_back_pressed = true;
                    BaseFunctions.showInfoToast(StockOwnerHomeActivity.this,"اضغط زر العودة مرة اخرى للخروج من التطبيق");
                }
            }else if (f instanceof OffersFragment || f instanceof MedicinesFragment){
                click_orders();
            }
        }
    }

    private void click_orders(){
        Fragment f = fragmentManager.findFragmentById(R.id.container);
        if (!(f instanceof OrdersFragment)) {
            orders_icon.setImageResource(R.drawable.orders_on);
            offer_icon.setImageResource(R.drawable.offers_off);
            medicines_icon.setImageResource(R.drawable.owner_medicine_off);
            title.setText("الطلبيات");
            extra.setVisibility(View.VISIBLE);
            extra.setImageResource(R.drawable.logout_red);
            extra.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(StockOwnerHomeActivity.this);
                    builder.setMessage("هل أنت متأكد من تسجيل الخروج ؟");
                    builder.setPositiveButton("نعم", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            SharedPrefManager.getInstance(StockOwnerHomeActivity.this).setUser(new UserObject());
                            startActivity(new Intent(StockOwnerHomeActivity.this, SelectAccountTypeActivity.class));
                            finish();
                        }
                    });
                    builder.setNegativeButton("لا", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    });
                    builder.show();
                }
            });

            orders_txt.setTextColor(getResources().getColor(R.color.colorPrimary));
            offers_txt.setTextColor(getResources().getColor(R.color.black));
            medicines_txt.setTextColor(getResources().getColor(R.color.black));

            open_fragment(new OrdersFragment());
        }
    }
    private void click_offers(){
        Fragment f = fragmentManager.findFragmentById(R.id.container);
        if (!(f instanceof OffersFragment)) {
            orders_icon.setImageResource(R.drawable.orders_off);
            offer_icon.setImageResource(R.drawable.offers_on);
            medicines_icon.setImageResource(R.drawable.owner_medicine_off);
            title.setText("العروض");
            extra.setVisibility(View.VISIBLE);
            extra.setImageResource(R.drawable.ic_add);
            extra.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    AddOfferDialog dialog = new AddOfferDialog(StockOwnerHomeActivity.this, new IMove() {
                        @Override
                        public void move(int position) {
                            Fragment f = fragmentManager.findFragmentById(R.id.container);
                            if (f instanceof OffersFragment){
                                ((OffersFragment)f).callOffersAPI();
                            }
                        }
                    });
                    dialog.show();
                }
            });

            orders_txt.setTextColor(getResources().getColor(R.color.black));
            offers_txt.setTextColor(getResources().getColor(R.color.colorPrimary));
            medicines_txt.setTextColor(getResources().getColor(R.color.black));

            open_fragment(new OffersFragment());
        }
    }

    private void click_medicines(){
        Fragment f = fragmentManager.findFragmentById(R.id.container);
        if (!(f instanceof MedicinesFragment)) {
            orders_icon.setImageResource(R.drawable.orders_off);
            offer_icon.setImageResource(R.drawable.offers_off);
            medicines_icon.setImageResource(R.drawable.owner_medicine_on);
            title.setText("أدويتي");
            extra.setVisibility(View.GONE);

            orders_txt.setTextColor(getResources().getColor(R.color.black));
            offers_txt.setTextColor(getResources().getColor(R.color.black));
            medicines_txt.setTextColor(getResources().getColor(R.color.colorPrimary));

            open_fragment(new MedicinesFragment());
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }
}
