package com.apps.hm.pharmaapp.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.apps.hm.pharmaapp.Bases.BaseFunctions;
import com.apps.hm.pharmaapp.Models.OfferObject;
import com.apps.hm.pharmaapp.R;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import de.hdodenhof.circleimageview.CircleImageView;

public class PharmaOffersAdapter  extends RecyclerView.Adapter<PharmaOffersAdapter.ViewHolder>{
    private Context context;
    private List<OfferObject> list;
    private String[] names = new String[]{"مستودع دياموند","مستودع الهيثم"};
    private String[] texts = new String[]{"العرض الأول العرض الأول العرض الأول العرض الأول العرض الأول العرض الأول العرض الأول العرض الأول العرض الأول العرض الأول العرض الأول",
            "العرض الثاني العرض الثاني العرض الثاني العرض الثاني العرض الثاني"};
    public PharmaOffersAdapter(Context context,List<OfferObject> list) {
        this.context = context;
        this.list = list;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView name,text,date;
        private CircleImageView image;

        public ViewHolder(View view) {
            super(view);
            name = view.findViewById(R.id.name);
            image = view.findViewById(R.id.image);
            text = view.findViewById(R.id.text);
            date = view.findViewById(R.id.date);
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_pharma_offer, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        OfferObject o = list.get(position);

        holder.name.setText(o.getOwner().getName());
        holder.text.setText(o.getText());
        BaseFunctions.setGlideImage(context,holder.image,o.getOwner().getAvatar());
        holder.date.setText("منذ "+BaseFunctions.processDate(context,o.getCreated_at()));
    }
}
